# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
-keepattributes SourceFile, LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-keepclassmembers, allowobfuscation class * {
  @com.google.gson.annotations.SerializedName <fields>;
}
-keep, allowobfuscation @interface com.google.gson.annotations.SerializedName

-keepclassmembers, allowobfuscation class * {
  @com.mcnmr.utilities.internal_plugin.StringIntent <fields>;
}
-keepclassmembers, allowobfuscation class * {
  @com.mcnmr.utilities.internal_plugin.BooleanIntent <fields>;
}
-keepclassmembers, allowobfuscation class * {
  @com.mcnmr.utilities.internal_plugin.IntIntent <fields>;
}
-keepclassmembers, allowobfuscation class * {
  @com.mcnmr.utilities.internal_plugin.SerializableIntent <fields>;
}
-keep, allowobfuscation @interface com.mcnmr.utilities.internal_plugin.StringIntent
-keep, allowobfuscation @interface com.mcnmr.utilities.internal_plugin.BooleanIntent
-keep, allowobfuscation @interface com.mcnmr.utilities.internal_plugin.IntIntent
-keep, allowobfuscation @interface com.mcnmr.utilities.internal_plugin.SerializableIntent
