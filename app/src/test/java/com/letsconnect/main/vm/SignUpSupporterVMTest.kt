package com.letsconnect.main.vm

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.letsconnect.ancestors.BaseResponse
import com.mcnmr.utilities.extension.gt
import com.mcnmr.utilities.extension.isMultipliedBy
import com.mcnmr.utilities.extension.isNotEmptyOrNotNull
import com.mcnmr.utilities.extension.isNotNull
import com.mcnmr.utilities.extension.notNullAndTrue
import com.mcnmr.utilities.extension.nullOrFalse
import com.mcnmr.utilities.internal_plugin.combineLiveData
import com.letsconnect.model.Role
import com.letsconnect.repository.network.parameter.SignUpSupporterParams
import com.letsconnect.repository.network.request.RemoteRepository
import com.letsconnect.repository.network.response.RegistrationDataResponse
import com.letsconnect.retrofit.RetrofitBuilder
import com.letsconnect.utils.safeApiCall
import com.letsconnect.wrapper.ResultWrapper
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import io.mockk.*
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import org.mockito.ArgumentMatchers.any
import java.io.File
import java.net.HttpURLConnection

class SignUpSupporterVMTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val folderRule = TemporaryFolder()

    private val mockWebserver = MockWebServer()
    private lateinit var repo: RemoteRepository

    private val readStoragePermissionPhotoEvent = SingleEventWrapper<Any?>()
    private val readStoragePermissionPhotoResult = SingleEventWrapper<Boolean>()
    private val readStoragePermissionCopiesCertEvent = SingleEventWrapper<Any?>()
    private val readStoragePermissionCopiesCertResult = SingleEventWrapper<Boolean>()
    private val gotoStep1Event = SingleEventWrapper<Any?>()
    private val gotoStep2Event = SingleEventWrapper<Any?>()
    private val gotoStep3Event = SingleEventWrapper<Any?>()
    private val gotoStep4Event = SingleEventWrapper<Any?>()
    private val gotoStep5Event = SingleEventWrapper<Any?>()
    private val gotoLoginPageEvent = SingleEventWrapper<Any?>()
    private val registerResponse = SingleEventWrapper<BaseResponse>()

    private val roleData = MutableLiveData<Role>()
    private val photoData = MutableLiveData<File>()
    private val introductionVideoData = MutableLiveData<String>()
    private val fullNameData = MutableLiveData<String>()

    private val skypeData = MutableLiveData<String>()
    private val genderData = MutableLiveData<String>()
    private val locationData = MutableLiveData<RegistrationDataResponse.Location>()
    private val workWithAdultsData = MutableLiveData<Boolean>()
    private val workWithAdolescentsData = MutableLiveData<Boolean>()
    private val workWithCouplesData = MutableLiveData<Boolean>()
    private val isLiveChatData = MutableLiveData<Boolean>()
    private val isVoiceCallData = MutableLiveData<Boolean>()
    private val isVideoCallData = MutableLiveData<Boolean>()

    private val aboutMeData = MutableLiveData<String>()
    private val licensingData = MutableLiveData<String>()
    private val certificationData = MutableLiveData<String>()

    private val listSpecializationData = MutableLiveData<MutableList<RegistrationDataResponse.Specialize>>()

    private val speakFluentData = MutableLiveData<String>()
    private val charge20Data = MutableLiveData<String>()
    private val charge50Data = MutableLiveData<String>()
    private val emailData = MutableLiveData<String>()
    private val passwordData = MutableLiveData<String>()
    private val repeatPasswordData = MutableLiveData<String>()

    private val step1Validation = combineLiveData(roleData, photoData, fullNameData, introductionVideoData)
    { a, b, c, d -> a.isNotNull() && b.isNotNull() && c.isNotEmptyOrNotNull() && d.isNotEmptyOrNotNull()}
    private val step2Validation = combineLiveData(skypeData, genderData, locationData)
    { a, b, c -> a.isNotEmptyOrNotNull() && b.isNotEmptyOrNotNull() && c.isNotNull()}
    private val step3Validation = combineLiveData(aboutMeData){ a -> a.isNotEmptyOrNotNull() }
    private val step4Validation = combineLiveData(listSpecializationData){ a -> a != null && a.isNotEmpty() }
    private val step5Validation = combineLiveData(speakFluentData, charge20Data, charge50Data, emailData, passwordData, repeatPasswordData)
    { a, b, c, d, e, f -> a.isNotEmptyOrNotNull() && b.isNotEmptyOrNotNull() &&
            (b?.toInt().isMultipliedBy(5) && b?.toInt().gt(0)) &&
            (c?.toInt().isMultipliedBy(5) && c?.toInt().gt(0)) &&
            c.isNotEmptyOrNotNull() && d.isNotEmptyOrNotNull() && e.isNotEmptyOrNotNull() &&
            f.isNotEmptyOrNotNull() && e.equals(f)}

    @Before
    fun start(){
        mockWebserver.start()
        repo = RetrofitBuilder.getClient(mockWebserver)
    }

    @After
    fun teardown(){
        mockWebserver.shutdown()
    }

    @Test
    fun `test step1 validation`(){
        step1Validation.observeForever(spyk())

        assert(step1Validation.value.nullOrFalse())
        roleData.value = mockk()
        assert(step1Validation.value.nullOrFalse())
        photoData.value = folderRule.newFile("profile.png")
        assert(step1Validation.value.nullOrFalse())
        fullNameData.value = "any"
        assert(step1Validation.value.nullOrFalse())
        introductionVideoData.value = "any"
        assert(step1Validation.value.notNullAndTrue())
    }

    @Test
    fun `test step2 validation`(){
        step2Validation.observeForever(spyk())

        assert(step2Validation.value.nullOrFalse())
        skypeData.value = "any"
        assert(step2Validation.value.nullOrFalse())
        genderData.value = "any"
        assert(step2Validation.value.nullOrFalse())
        locationData.value = mockk()
        assert(step2Validation.value.notNullAndTrue())
    }

    @Test
    fun `test step3 validation`(){
        step3Validation.observeForever(spyk())

        assert(step3Validation.value.nullOrFalse())
        aboutMeData.value = "any"
        assert(step3Validation.value.notNullAndTrue())
    }

    @Test
    fun `test step4 validation`(){
        step4Validation.observeForever(spyk())

        assert(step4Validation.value.nullOrFalse())
        listSpecializationData.value = mutableListOf()
        assert(step4Validation.value.nullOrFalse())
        listSpecializationData.value = mutableListOf(mockk())
        assert(step4Validation.value.notNullAndTrue())
    }

    @Test
    fun `test step5 validation`(){
        step5Validation.observeForever(spyk())

        assert(step5Validation.value.nullOrFalse())
        speakFluentData.value = "any"
        assert(step5Validation.value.nullOrFalse())
        charge20Data.value = "0"
        assert(step5Validation.value.nullOrFalse())
        charge50Data.value = "0"
        assert(step5Validation.value.nullOrFalse())
        emailData.value = "any"
        assert(step5Validation.value.nullOrFalse())
        passwordData.value = "any"
        assert(step5Validation.value.nullOrFalse())
        repeatPasswordData.value = "-any"
        assert(step5Validation.value.nullOrFalse())
        repeatPasswordData.value = "any"
        assert(step5Validation.value.nullOrFalse())
        charge20Data.value = "5"
        assert(step5Validation.value.nullOrFalse())
        charge50Data.value = "5"
        assert(step5Validation.value.notNullAndTrue())
    }

    @Test
    fun `test single event`(){
        val readStoragePermissionPhotoObs = mockk<Observer<Any?>>()
        val readStoragePermissionPhotoResultObs = mockk<Observer<Boolean>>()
        val readStoragePermissionCopiesCertObs = mockk<Observer<Any?>>()
        val readStoragePermissionCopiesCertResultObs = mockk<Observer<Boolean>>()
        val gotoStep1Obs = mockk<Observer<Any?>>()
        val gotoStep2Obs = mockk<Observer<Any?>>()
        val gotoStep3Obs = mockk<Observer<Any?>>()
        val gotoStep4Obs = mockk<Observer<Any?>>()
        val gotoStep5Obs = mockk<Observer<Any?>>()
        val gotoLoginPageObs = mockk<Observer<Any?>>()
        val registerObs = mockk<Observer<BaseResponse>>()

        every { readStoragePermissionPhotoObs.onChanged(any()) } just Runs
        every { readStoragePermissionPhotoResultObs.onChanged(any()) } just Runs
        every { readStoragePermissionCopiesCertObs.onChanged(any()) } just Runs
        every { readStoragePermissionCopiesCertResultObs.onChanged(any()) } just Runs
        every { gotoStep1Obs.onChanged(any()) } just Runs
        every { gotoStep2Obs.onChanged(any()) } just Runs
        every { gotoStep3Obs.onChanged(any()) } just Runs
        every { gotoStep4Obs.onChanged(any()) } just Runs
        every { gotoStep5Obs.onChanged(any()) } just Runs
        every { gotoLoginPageObs.onChanged(any()) } just Runs
        every { registerObs.onChanged(any()) } just Runs

        readStoragePermissionPhotoEvent.observeForever(readStoragePermissionPhotoObs)
        readStoragePermissionPhotoResult.observeForever(readStoragePermissionPhotoResultObs)
        readStoragePermissionCopiesCertEvent.observeForever(readStoragePermissionCopiesCertObs)
        readStoragePermissionCopiesCertResult.observeForever(readStoragePermissionCopiesCertResultObs)
        gotoStep1Event.observeForever(gotoStep1Obs)
        gotoStep2Event.observeForever(gotoStep2Obs)
        gotoStep3Event.observeForever(gotoStep3Obs)
        gotoStep4Event.observeForever(gotoStep4Obs)
        gotoStep5Event.observeForever(gotoStep5Obs)
        gotoLoginPageEvent.observeForever(gotoLoginPageObs)
        registerResponse.observeForever(registerObs)

        readStoragePermissionPhotoEvent.trigger()
        readStoragePermissionPhotoResult.value = true
        readStoragePermissionCopiesCertEvent.trigger()
        readStoragePermissionCopiesCertResult.value = true
        gotoStep1Event.trigger()
        gotoStep2Event.trigger()
        gotoStep3Event.trigger()
        gotoStep4Event.trigger()
        gotoStep5Event.trigger()
        gotoLoginPageEvent.trigger()
        registerResponse.value = any()

        verify { readStoragePermissionPhotoObs.onChanged(any()) }
        verify { readStoragePermissionPhotoResultObs.onChanged(any()) }
        verify { readStoragePermissionCopiesCertObs.onChanged(any()) }
        verify { readStoragePermissionCopiesCertResultObs.onChanged(any()) }
        verify { gotoStep1Obs.onChanged(any()) }
        verify { gotoStep2Obs.onChanged(any()) }
        verify { gotoStep3Obs.onChanged(any()) }
        verify { gotoStep4Obs.onChanged(any()) }
        verify { gotoStep5Obs.onChanged(any()) }
        verify { gotoLoginPageObs.onChanged(any()) }
        verify { registerObs.onChanged(any()) }
    }

    @Test
    fun `test register string parameter`(){
        roleData.value = Role.Counselor()
        introductionVideoData.value = "any"
        fullNameData.value = "any"
        skypeData.value = "any"
        genderData.value = "any"
        locationData.value = RegistrationDataResponse.Location("ID", "Indonesia")
        workWithAdultsData.value = true
        workWithAdolescentsData.value = true
        workWithCouplesData.value = true
        isLiveChatData.value = true
        isVoiceCallData.value = true
        isVideoCallData.value = true
        aboutMeData.value = "any"
        licensingData.value = "any"
        certificationData.value = "any"
        listSpecializationData.value = mutableListOf(RegistrationDataResponse.Specialize("1", "Any"))
        speakFluentData.value = "any"
        charge20Data.value = "any number multiplied by 5"
        charge50Data.value = "any number multiplied by 5"
        passwordData.value = "any"

        val params = SignUpSupporterParams(
            roleData.value?.id, introductionVideoData.value, fullNameData.value, skypeData.value,
            genderData.value, locationData.value?.locationCode, aboutMeData.value, licensingData.value,
            certificationData.value, passwordData.value, listSpecializationData.value, isLiveChatData.value,
            isVoiceCallData.value, isVideoCallData.value, speakFluentData.value, charge20Data.value,
            charge50Data.value, emailData.value, workWithAdultsData.value, workWithAdolescentsData.value,
            workWithCouplesData.value
        ).asMap()

        println(params)
    }

    @Test
    fun `test success register supporter`(){
        val profileImage = folderRule.newFile("profile_image.png")

        runBlocking {
            val jsonResponse = javaClass.getResource("/register_ok.json")?.readText()
            assert(jsonResponse != null)

            mockWebserver.enqueue(
                MockResponse()
                    .setResponseCode(HttpURLConnection.HTTP_OK)
                    .setBody(jsonResponse!!))

            val file = MultipartBody.Part.createFormData("profile_image",
                "${System.currentTimeMillis()}.png",
                profileImage.asRequestBody("image/png".toMediaTypeOrNull()))

            val response = safeApiCall(coroutineContext){ repo.registerAsSupporter(mutableMapOf(), file) }
            assert(response is ResultWrapper.Success)
        }
    }

    @Test
    fun `test success register supporter with certification files`() {
        val profileImage = folderRule.newFile("profile_image.png")
        val certificationImage = folderRule.newFile("certification_image.png")

        runBlocking {
            val jsonResponse = javaClass.getResource("/register_ok.json")?.readText()
            assert(jsonResponse != null)

            mockWebserver.enqueue(
                MockResponse()
                    .setResponseCode(HttpURLConnection.HTTP_OK)
                    .setBody(jsonResponse!!))

            val file = MultipartBody.Part.createFormData("profile_image",
                "${System.currentTimeMillis()}.png",
                profileImage.asRequestBody("image/png".toMediaTypeOrNull()))
            val certFile = MultipartBody.Part.createFormData("copies_certification",
                "${System.currentTimeMillis()}.png", certificationImage.asRequestBody("image/png".toMediaTypeOrNull()))

            val response = safeApiCall(coroutineContext){ repo.registerAsSupporter(mutableMapOf(), file, certFile) }
            assert(response is ResultWrapper.Success)
        }
    }

    @Test
    fun `test fail register supporter`() {
        val profileImage = folderRule.newFile("profile_image.png")

        runBlocking {
            val jsonResponse = javaClass.getResource("/register_fail.json")?.readText()
            assert(jsonResponse != null)

            mockWebserver.enqueue(
                MockResponse()
                    .setResponseCode(HttpURLConnection.HTTP_OK)
                    .setBody(jsonResponse!!))

            val file = MultipartBody.Part.createFormData("profile_image",
                "${System.currentTimeMillis()}.png",
                profileImage.asRequestBody("image/png".toMediaTypeOrNull()))

            val response = safeApiCall(coroutineContext){ repo.registerAsSupporter(mutableMapOf(), file) }
            assert(response is ResultWrapper.ResponseError)
        }
    }
}