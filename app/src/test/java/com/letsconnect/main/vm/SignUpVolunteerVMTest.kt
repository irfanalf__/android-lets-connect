package com.letsconnect.main.vm

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.letsconnect.R
import com.letsconnect.ancestors.BaseResponse
import com.mcnmr.utilities.internal_plugin.combineLiveData
import com.letsconnect.model.Role
import com.letsconnect.repository.network.request.RemoteRepository
import com.letsconnect.repository.network.response.RegistrationDataResponse
import com.letsconnect.retrofit.RetrofitBuilder
import com.letsconnect.utils.safeApiCall
import com.letsconnect.wrapper.ResultWrapper
import com.mcnmr.utilities.extension.*
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import io.mockk.*
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import org.mockito.ArgumentMatchers.any
import org.mockito.ArgumentMatchers.anyString
import java.io.File
import java.net.HttpURLConnection
import java.util.regex.Pattern

class SignUpVolunteerVMTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val folderRule = TemporaryFolder()

    private val mockWebserver = MockWebServer()
    private lateinit var repo: RemoteRepository

    private val readStoragePermissionEvent = SingleEventWrapper<Any?>()
    private val readStoragePermissionResult = SingleEventWrapper<Boolean>()
    private val gotoStep1Event = SingleEventWrapper<Any?>()
    private val gotoStep2Event = SingleEventWrapper<Any?>()
    private val gotoStep3Event = SingleEventWrapper<Any?>()
    private val gotoLoginPageEvent = SingleEventWrapper<Any?>()
    private val registerResponse = SingleEventWrapper<BaseResponse>()

    private val roleData = MutableLiveData<Role>()
    private val photoData = MutableLiveData<File>()
    private val screenNameData = MutableLiveData<String>()
    private val screenNameValidation = combineLiveData(screenNameData){
        if(!it.isNotEmptyOrNotNull()){
            false
        }else (it ?: " ").split(" ").size == 1
    }
    private val photoValidation = combineLiveData(photoData){ it.isNotNull() }

    private val genderData = MutableLiveData<String>()
    private val locationData = MutableLiveData<RegistrationDataResponse.Location>()
    private val aboutMeData = MutableLiveData<String>()
    private val aboutMeValidation = combineLiveData(aboutMeData){ it.isNotEmptyOrNotNull() }

    private val emailData = MutableLiveData<String>()
    private val passwordData = MutableLiveData<String>()
    private val repeatPasswordData = MutableLiveData<String>()
    private val termsOfAgreementData = MutableLiveData<Boolean>()
    private val above13YearsData = MutableLiveData<Boolean>()
    private val emailValidation = combineLiveData(emailData){
        if(!it.isNotEmptyOrNotNull()){
            false
        } else isEmailAddress(it)
    }
    private val passwordValidation = combineLiveData(passwordData){
        if(!it.isNotEmptyOrNotNull()){
            false
        }else !(!it.isAlphanumeric() || (it?.length ?: 0) < 8)
    }
    private val repeatPasswordValidation = combineLiveData(passwordData, repeatPasswordData){ a, b -> a.equals(b) }

    private val step1Validation = combineLiveData(roleData, photoValidation, screenNameValidation)
    { a, b, c -> a.isNotNull() && b.notNullAndTrue() && c.notNullAndTrue() }
    private val step2Validation = combineLiveData(genderData, locationData, aboutMeValidation)
    { a, b, c -> a.isNotNull() && b.isNotNull() && c.notNullAndTrue()}
    private val step3Validation = combineLiveData(emailValidation, passwordValidation, repeatPasswordValidation, termsOfAgreementData, above13YearsData)
    { a, b, c, d, e -> a.notNullAndTrue() && b.notNullAndTrue() && c.notNullAndTrue()
            && d.notNullAndTrue() && e.notNullAndTrue()}

    private fun isEmailAddress(s: String?): Boolean{
        if(s == null){
            return false
        }

        return Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
        ).matcher(s).matches()
    }

    @Before
    fun start(){
        mockWebserver.start()
        repo = RetrofitBuilder.getClient(mockWebserver)
    }

    @After
    fun teardown(){
        mockWebserver.shutdown()
    }

    @Test
    fun `test success register volunteer`(){
        val profileImage = folderRule.newFile("profile_image.png")

        runBlocking {
            val jsonResponse = javaClass.getResource("/register_ok.json")?.readText()
            assert(jsonResponse != null)

            mockWebserver.enqueue(MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(jsonResponse!!))

            val file = MultipartBody.Part.createFormData("profile_image",
                "${System.currentTimeMillis()}.png",
                profileImage.asRequestBody("image/png".toMediaTypeOrNull()))

            val response = safeApiCall(coroutineContext){ repo.registerAsVolunteer(mutableMapOf(), file) }
            assert(response is ResultWrapper.Success)
        }
    }

    @Test
    fun `test fail register volunteer`(){
        val profileImage = folderRule.newFile("profile_image.png")

        runBlocking {
            val jsonResponse = javaClass.getResource("/register_fail.json")?.readText()
            assert(jsonResponse != null)

            mockWebserver.enqueue(MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(jsonResponse!!))

            val file = MultipartBody.Part.createFormData("profile_image",
                "${System.currentTimeMillis()}.png",
                profileImage.asRequestBody("image/png".toMediaTypeOrNull()))

            val response = safeApiCall(coroutineContext){ repo.registerAsVolunteer(mutableMapOf(), file) }
            assert(response is ResultWrapper.ResponseError)
        }
    }

    @Test
    fun `test single event`(){
        val readStoragePermissionObs = mockk<Observer<Any?>>()
        val readStoragePermissionResultObs = mockk<Observer<Boolean>>()
        val gotoStep1Obs = mockk<Observer<Any?>>()
        val gotoStep2Obs = mockk<Observer<Any?>>()
        val gotoStep3Obs = mockk<Observer<Any?>>()
        val gotoLoginPageObs = mockk<Observer<Any?>>()
        val registerResponseObs = mockk<Observer<BaseResponse>>()

        every { readStoragePermissionObs.onChanged(any()) } just Runs
        every { readStoragePermissionResultObs.onChanged(any()) } just Runs
        every { gotoStep1Obs.onChanged(any()) } just Runs
        every { gotoStep2Obs.onChanged(any()) } just Runs
        every { gotoStep3Obs.onChanged(any()) } just Runs
        every { gotoLoginPageObs.onChanged(any()) } just Runs
        every { registerResponseObs.onChanged(any()) } just Runs

        readStoragePermissionEvent.observeForever(readStoragePermissionObs)
        readStoragePermissionResult.observeForever(readStoragePermissionResultObs)
        gotoStep1Event.observeForever(gotoStep1Obs)
        gotoStep2Event.observeForever(gotoStep2Obs)
        gotoStep3Event.observeForever(gotoStep3Obs)
        gotoLoginPageEvent.observeForever(gotoLoginPageObs)
        registerResponse.observeForever(registerResponseObs)

        readStoragePermissionEvent.trigger()
        readStoragePermissionResult.trigger()
        gotoStep1Event.trigger()
        gotoStep2Event.trigger()
        gotoStep3Event.trigger()
        gotoLoginPageEvent.trigger()
        registerResponse.value = any()

        verify(exactly = 1) { readStoragePermissionObs.onChanged(any()) }
        verify(exactly = 1) { readStoragePermissionResultObs.onChanged(any()) }
        verify(exactly = 1) { gotoStep1Obs.onChanged(any()) }
        verify(exactly = 1) { gotoStep2Obs.onChanged(any()) }
        verify(exactly = 1) { gotoStep3Obs.onChanged(any()) }
        verify(exactly = 1) { gotoLoginPageObs.onChanged(any()) }
        verify(exactly = 1) { registerResponseObs.onChanged(any()) }
    }

    @Test
    fun `test step1 validation`(){
        val validationObs = mockk<Observer<Boolean>>()
        every { validationObs.onChanged(any()) } just Runs

        screenNameValidation.observeForever(spyk())
        step1Validation.observeForever(validationObs)

        assert(step1Validation.value.nullOrFalse())
        roleData.value = mockk()

        assert(step1Validation.value.nullOrFalse())
        assert(photoValidation.value.nullOrFalse())
        photoData.value = folderRule.newFile("profile_image.png")
        assert(photoValidation.value.notNullAndTrue())

        assert(step1Validation.value.nullOrFalse())
        assert(screenNameValidation.value.nullOrFalse())
        screenNameData.value = "any any10"
        assert(screenNameValidation.value.nullOrFalse())
        screenNameData.value = "any10"
        assert(screenNameValidation.value.notNullAndTrue())
        assert(step1Validation.value.notNullAndTrue())

        verify { validationObs.onChanged(any()) }
    }

    @Test
    fun `test step2 validation`(){
        val validationObs = mockk<Observer<Boolean>>()
        every { validationObs.onChanged(any()) } just Runs

        aboutMeValidation.observeForever(spyk())
        step2Validation.observeForever(validationObs)

        assert(step2Validation.value.nullOrFalse())
        genderData.value = anyString()

        assert(step2Validation.value.nullOrFalse())
        locationData.value = mockk()

        assert(step2Validation.value.nullOrFalse())
        assert(aboutMeValidation.value.nullOrFalse())
        aboutMeData.value = "any"
        assert(aboutMeValidation.value.notNullAndTrue())
        assert(step2Validation.value.notNullAndTrue())

        verify { validationObs.onChanged(any()) }
    }

    @Test
    fun `test step3 validation`(){
        val validationObs = mockk<Observer<Boolean>>()
        every { validationObs.onChanged(any()) } just Runs

        emailValidation.observeForever(spyk())
        passwordValidation.observeForever(spyk())
        repeatPasswordValidation.observeForever(spyk())
        step3Validation.observeForever(validationObs)

        assert(step3Validation.value.nullOrFalse())
        assert(emailValidation.value.nullOrFalse())
        emailData.value = "email"
        assert(emailValidation.value.nullOrFalse())
        emailData.value = "email@gmail"
        assert(emailValidation.value.nullOrFalse())
        emailData.value = "email@gmail.com"
        assert(emailValidation.value.notNullAndTrue())

        assert(step3Validation.value.nullOrFalse())
        assert(passwordValidation.value.nullOrFalse())
        passwordData.value = "pass"
        assert(passwordValidation.value.nullOrFalse())
        passwordData.value = "pass1234"
        assert(passwordValidation.value.notNullAndTrue())

        assert(step3Validation.value.nullOrFalse())
        assert(repeatPasswordValidation.value.nullOrFalse())
        repeatPasswordData.value = "pass1111"
        assert(repeatPasswordValidation.value.nullOrFalse())
        repeatPasswordData.value = "pass1234"
        assert(repeatPasswordValidation.value.notNullAndTrue())

        assert(step3Validation.value.nullOrFalse())
        termsOfAgreementData.value = true

        assert(step3Validation.value.nullOrFalse())
        above13YearsData.value = true
        assert(step3Validation.value.notNullAndTrue())

        verify { validationObs.onChanged(any()) }
    }
}