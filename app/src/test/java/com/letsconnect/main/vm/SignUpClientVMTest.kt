package com.letsconnect.main.vm

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.letsconnect.ancestors.BaseResponse
import com.mcnmr.utilities.extension.isNotEmptyOrNotNull
import com.mcnmr.utilities.extension.notNullAndTrue
import com.mcnmr.utilities.extension.nullOrFalse
import com.mcnmr.utilities.internal_plugin.combineLiveData
import com.letsconnect.repository.network.request.RemoteRepository
import com.letsconnect.retrofit.RetrofitBuilder
import com.letsconnect.utils.safeApiCall
import com.letsconnect.wrapper.ResultWrapper
import com.mcnmr.utilities.extension.isAlphanumeric
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import io.mockk.*
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.any
import java.net.HttpURLConnection
import java.util.regex.Pattern

class SignUpClientVMTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val mockWebserver = MockWebServer()
    private lateinit var repo: RemoteRepository

    private val screenNameData = MutableLiveData<String>()
    private val emailData = MutableLiveData<String>()
    private val passwordData = MutableLiveData<String>()
    private val repeatPasswordData = MutableLiveData<String>()
    private val termConditionsData = MutableLiveData<Boolean>()
    private val over13YearsData = MutableLiveData<Boolean>()

    private val gotoLoginPageEvent = SingleEventWrapper<Any?>()
    private val registerResponse = SingleEventWrapper<BaseResponse>()

    private val screenNameValidation = combineLiveData(screenNameData){
        if(!it.isNotEmptyOrNotNull()){
            false
        }else (it ?: " ").split(" ").size == 1
    }
    private val emailValidation = combineLiveData(emailData){
        if(!it.isNotEmptyOrNotNull() ){
            false
        }else isEmailAddress(it)
    }
    private val passwordValidation = combineLiveData(passwordData){
        if(!it.isNotEmptyOrNotNull()){
            false
        }else !(!it.isAlphanumeric() || (it?.length ?: 0) < 8)
    }
    private  val repeatPasswordValidation = combineLiveData(passwordData, repeatPasswordData){ a, b -> a.equals(b) }

    private val validation = combineLiveData(screenNameValidation, emailValidation,
        passwordValidation, repeatPasswordValidation, termConditionsData, over13YearsData)
    { a, b, c, d, e, f -> a.notNullAndTrue() && b.notNullAndTrue() && c.notNullAndTrue() &&
            d.notNullAndTrue() && e.notNullAndTrue() && f.notNullAndTrue() }

    private fun isEmailAddress(s: String?): Boolean{
        if(s == null){
            return false
        }

        return Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
        ).matcher(s).matches()
    }

    @Before
    fun start(){
        mockWebserver.start()
        repo = RetrofitBuilder.getClient(mockWebserver)
    }

    @After
    fun teardown(){
        mockWebserver.shutdown()
    }

    @Test
    fun `test success register client`() = runBlocking {
        val jsonResponse = javaClass.getResource("/register_ok.json")?.readText()
        assert(jsonResponse != null)

        mockWebserver.enqueue(MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(jsonResponse!!))

        val response = safeApiCall(coroutineContext){ repo.registerAsClient(mockk()) }
        assert(response is ResultWrapper.Success)
    }

    @Test
    fun `test fail register client`() = runBlocking {
        val jsonResponse = javaClass.getResource("/register_fail.json")?.readText()
        assert(jsonResponse != null)

        mockWebserver.enqueue(MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(jsonResponse!!))

        val response = safeApiCall(coroutineContext){ repo.registerAsClient(mockk()) }
        assert(response is ResultWrapper.ResponseError)
    }

    @Test
    fun `test input validation`(){
        screenNameValidation.observeForever(spyk())
        emailValidation.observeForever(spyk())
        passwordValidation.observeForever(spyk())
        repeatPasswordValidation.observeForever(spyk())
        validation.observeForever(spyk())

        assert(validation.value.nullOrFalse())
        assert(screenNameValidation.value.nullOrFalse())
        screenNameData.value = "any any10"
        assert(screenNameValidation.value.nullOrFalse())
        screenNameData.value = "any10"
        assert(screenNameValidation.value.notNullAndTrue())

        assert(validation.value.nullOrFalse())
        assert(emailValidation.value.nullOrFalse())
        emailData.value = "email"
        assert(emailValidation.value.nullOrFalse())
        emailData.value = "email@gmail"
        assert(emailValidation.value.nullOrFalse())
        emailData.value = "email@gmail.com"
        assert(emailValidation.value.notNullAndTrue())

        assert(validation.value.nullOrFalse())
        assert(passwordValidation.value.nullOrFalse())
        passwordData.value = "pass"
        assert(passwordValidation.value.nullOrFalse())
        passwordData.value = "pass1234"
        assert(passwordValidation.value.notNullAndTrue())

        assert(validation.value.nullOrFalse())
        assert(repeatPasswordValidation.value.nullOrFalse())
        repeatPasswordData.value = "pass1111"
        assert(repeatPasswordValidation.value.nullOrFalse())
        repeatPasswordData.value = "pass1234"
        assert(repeatPasswordValidation.value.notNullAndTrue())

        assert(validation.value.nullOrFalse())
        assert(termConditionsData.value.nullOrFalse())
        termConditionsData.value = true
        assert(termConditionsData.value.notNullAndTrue())

        assert(validation.value.nullOrFalse())
        assert(over13YearsData.value.nullOrFalse())
        over13YearsData.value = true
        assert(over13YearsData.value.notNullAndTrue())

        assert(validation.value.notNullAndTrue())
    }

    @Test
    fun `test single event`(){
        val gotoLoginPageObs = mockk<Observer<Any?>>()
        every { gotoLoginPageObs.onChanged(any()) } just Runs

        val registerResponseObs = mockk<Observer<BaseResponse>>()
        every { registerResponseObs.onChanged(any()) } just Runs

        gotoLoginPageEvent.observeForever(gotoLoginPageObs)
        registerResponse.observeForever(registerResponseObs)

        gotoLoginPageEvent.trigger()
        registerResponse.value = any()

        verify(exactly = 1){ gotoLoginPageEvent.trigger() }
        verify(exactly = 1){ registerResponse.trigger() }
    }
}