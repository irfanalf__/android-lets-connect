package com.letsconnect.main.vm

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.letsconnect.repository.network.request.RemoteRepository
import com.letsconnect.retrofit.RetrofitBuilder
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule

class DetailPersonVMTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val mockWebserver = MockWebServer()
    private lateinit var repo: RemoteRepository

    @Before
    fun start() {
        mockWebserver.start()
        repo = RetrofitBuilder.getClient(mockWebserver)
    }

    @After
    fun teardown() {
        mockWebserver.shutdown()
    }
}