package com.letsconnect.main.vm

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.letsconnect.repository.network.request.RemoteRepository
import com.letsconnect.repository.network.response.RegistrationDataResponse
import com.letsconnect.retrofit.RetrofitBuilder
import com.letsconnect.utils.safeApiCall
import com.letsconnect.wrapper.ResultWrapper
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import io.mockk.*
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import java.net.HttpURLConnection

class OnboardingVMTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val mockWebserver = MockWebServer()
    private lateinit var repo: RemoteRepository

    private val registrationDataResponse = SingleEventWrapper<RegistrationDataResponse>()

    @Before
    fun start() {
        mockWebserver.start()
        repo = RetrofitBuilder.getClient(mockWebserver)
    }

    @After
    fun teardown() {
        mockWebserver.shutdown()
    }

    @Test
    fun `test success get registration data`() = runBlocking{
        val jsonResponse = javaClass.getResource("/registration_data_ok.json")?.readText()
        assert(jsonResponse != null)

        mockWebserver.enqueue(
            MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(jsonResponse!!))

        val response = safeApiCall(coroutineContext){ repo.registrationData() }
        assert(response is ResultWrapper.Success)
    }

    @Test
    fun `test single event`(){
        val registrationDataObs = mockk<Observer<RegistrationDataResponse>>()

        every { registrationDataObs.onChanged(any()) } just Runs

        registrationDataResponse.observeForever(registrationDataObs)

        registrationDataResponse.value = ArgumentMatchers.any()

        verify { registrationDataObs.onChanged(any()) }
    }
}