package com.letsconnect.main.vm

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.letsconnect.model.Role
import com.letsconnect.repository.network.request.RemoteRepository
import com.letsconnect.repository.network.response.UserResponse
import com.letsconnect.repository.network.response.RegistrationDataResponse
import com.letsconnect.retrofit.RetrofitBuilder
import com.letsconnect.utils.safeApiCall
import com.letsconnect.wrapper.ResultWrapper
import com.mcnmr.utilities.extension.isNotEmptyOrNotNull
import com.mcnmr.utilities.extension.notNullAndTrue
import com.mcnmr.utilities.extension.nullOrFalse
import com.mcnmr.utilities.internal_plugin.combineLiveData
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import io.mockk.*
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.any
import java.net.HttpURLConnection

class LoginVMTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val mockWebserver = MockWebServer()
    private lateinit var repo: RemoteRepository

    private val registrationDataResponse = SingleEventWrapper<RegistrationDataResponse>()
    private val loginResponse = SingleEventWrapper<UserResponse>()

    private val emailData = MutableLiveData<String>()
    private val passwordData = MutableLiveData<String>()
    private val validation = combineLiveData(emailData, passwordData)
    { a, b -> a.isNotEmptyOrNotNull() && b.isNotEmptyOrNotNull() }

    @Before
    fun start() {
        mockWebserver.start()
        repo = RetrofitBuilder.getClient(mockWebserver)
    }

    @After
    fun teardown() {
        mockWebserver.shutdown()
    }

    @Test
    fun `test validation`() {
        validation.observeForever(spyk())

        emailData.value = "any"
        assert(validation.value.nullOrFalse())
        passwordData.value = "any"
        assert(validation.value.notNullAndTrue())
    }

    @Test
    fun `test success login as client`() = runBlocking{
        val jsonResponse = javaClass.getResource("/login_client_ok.json")?.readText()
        assert(jsonResponse != null)

        mockWebserver.enqueue(
            MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(jsonResponse!!))

        val response = safeApiCall(coroutineContext){ repo.login(mockk()) }
        assert(response is ResultWrapper.Success)
        assertEquals((response as ResultWrapper.Success).value.user.roleId, Role.ROLE_CLIENT)
    }

    @Test
    fun `test success login as volunteer`() = runBlocking{
        val jsonResponse = javaClass.getResource("/login_volunteer_ok.json")?.readText()
        assert(jsonResponse != null)

        mockWebserver.enqueue(
            MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(jsonResponse!!))

        val response = safeApiCall(coroutineContext){ repo.login(mockk()) }
        assert(response is ResultWrapper.Success)
        assertEquals((response as ResultWrapper.Success).value.user.roleId, Role.ROLE_VOLUNTEER)
    }

    @Test
    fun `test success login as professional`() = runBlocking{
        val jsonResponse = javaClass.getResource("/login_professional_ok.json")?.readText()
        assert(jsonResponse != null)

        mockWebserver.enqueue(
            MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(jsonResponse!!))

        val response = safeApiCall(coroutineContext){ repo.login(mockk()) }
        assert(response is ResultWrapper.Success)
        assertEquals((response as ResultWrapper.Success).value.user.roleId, Role.ROLE_PROFESSIONAL)
    }

    @Test
    fun `test success login as supporter`() = runBlocking{
        val jsonResponse = javaClass.getResource("/login_supporter_ok.json")?.readText()
        assert(jsonResponse != null)

        mockWebserver.enqueue(
            MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(jsonResponse!!))

        val response = safeApiCall(coroutineContext){ repo.login(mockk()) }
        assert(response is ResultWrapper.Success)
        assertEquals((response as ResultWrapper.Success).value.user.roleId, Role.ROLE_SUPPORTER)
    }

    @Test
    fun `test fail login`() = runBlocking{
        val jsonResponse = javaClass.getResource("/login_fail.json")?.readText()
        assert(jsonResponse != null)

        mockWebserver.enqueue(
            MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(jsonResponse!!))

        val response = safeApiCall(coroutineContext){ repo.login(mockk()) }
        assert(response is ResultWrapper.ResponseError)
    }

    @Test
    fun `test success get registration data`() = runBlocking{
        val jsonResponse = javaClass.getResource("/registration_data_ok.json")?.readText()
        assert(jsonResponse != null)

        mockWebserver.enqueue(
            MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(jsonResponse!!))

        val response = safeApiCall(coroutineContext){ repo.registrationData() }
        assert(response is ResultWrapper.Success)
    }

    @Test
    fun `test single event`(){
        val registrationDataObs = mockk<Observer<RegistrationDataResponse>>()
        val loginObs = mockk<Observer<UserResponse>>()

        every { registrationDataObs.onChanged(any()) } just Runs
        every { loginObs.onChanged(any()) } just Runs

        registrationDataResponse.observeForever(registrationDataObs)
        loginResponse.observeForever(loginObs)

        registrationDataResponse.value = any()
        loginResponse.value = any()

        verify { registrationDataObs.onChanged(any()) }
        verify { loginObs.onChanged(any()) }
    }
}