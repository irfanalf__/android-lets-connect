package com.letsconnect.main

import com.google.gson.Gson
import com.letsconnect.repository.socket.response.SomeoneOfflineResponse
import com.letsconnect.repository.socket.response.SomeoneOnlineResponse
import com.letsconnect.repository.socket.response.SomeoneTypingResponse
import org.awaitility.Awaitility.await
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit

class SocketRepositoryTest {

    private lateinit var gson: Gson
    private lateinit var cacheOnlineUser: HashMap<String, Double>
    private lateinit var cacheTypingUser: HashMap<String, Long>

    @Before
    fun setup(){
        gson = Gson()
        cacheOnlineUser = HashMap()
        cacheTypingUser = HashMap()
    }

    @Test
    fun `test user typing`(){
        val jsonSomeoneTyping = javaClass.getResource("/someone_typing_ok.json")?.readText()
        assert(jsonSomeoneTyping != null)

        val someoneTyping = gson.fromJson(jsonSomeoneTyping, SomeoneTypingResponse::class.java)

        cacheTypingUser[someoneTyping.fromId] = System.currentTimeMillis()
        assert(cacheTypingUser.containsKey(someoneTyping.fromId))
        await().pollDelay(5, TimeUnit.SECONDS).until{ true }
        assert(System.currentTimeMillis().minus(cacheTypingUser[someoneTyping.fromId]!!) > 5000)
    }
}