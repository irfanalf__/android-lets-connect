package com.letsconnect.main

import android.content.SharedPreferences
import io.mockk.*
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString

class AppPreferenceTest {
    companion object {
        const val IS_FIRST_TIME = "IS_FIRST_TIME"
        const val USER = "USER"
    }

    private lateinit var preferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor

    @Before
    fun setup(){
        preferences = mockk()
        editor = mockk()
    }

    @Test
    fun `test save and get first time status`(){
        every { editor.putBoolean(IS_FIRST_TIME, true).apply() } just Runs
        every { preferences.getBoolean(IS_FIRST_TIME, false) } returns true

        editor.putBoolean(IS_FIRST_TIME, true).apply()
        val isFirstTime = preferences.getBoolean(IS_FIRST_TIME, false)

        verify { editor.putBoolean(IS_FIRST_TIME, true).apply() }
        verify { preferences.getBoolean(IS_FIRST_TIME, false) }
        assert(isFirstTime)
    }

    @Test
    fun `test save and get user`(){
        every { editor.putString(USER, anyString()).apply() } just Runs
        every { preferences.getString(USER, anyString()) } returns "JSON User"

        editor.putString(USER, anyString()).apply()
        val user = preferences.getString(USER, anyString())

        verify { editor.putString(USER, anyString()).apply() }
        verify { preferences.getString(USER, anyString()) }
        assert(user == "JSON User")
    }
}