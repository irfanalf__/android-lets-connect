package com.letsconnect.utils

import com.letsconnect.ancestors.BaseResponse
import com.letsconnect.repository.network.response.ErrorResponseException
import com.letsconnect.wrapper.ResultWrapper
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

suspend fun <T: BaseResponse> safeApiCall(context: CoroutineContext,
                                  apiCall: suspend () -> T): ResultWrapper<T> {
    return withContext(context){
        try{
            val result = apiCall.invoke()
            if(!result.status()) {
                throw ErrorResponseException(message = result.message())
            }else {
                ResultWrapper.Success(result)
            }
        }catch (e: Exception){
            e.printStackTrace()
            when(e){
                is IOException -> ResultWrapper.NetworkError(0, e)
                is HttpException -> {
                    val code = e.code()
                    val errorResponse = e.message()
                    ResultWrapper.HttpError(0, code = code, error = errorResponse)
                }
                is ErrorResponseException -> ResultWrapper.ResponseError(0, e)
                else -> ResultWrapper.UnknownError(0, e.message ?: "No message")
            }
        }
    }
}