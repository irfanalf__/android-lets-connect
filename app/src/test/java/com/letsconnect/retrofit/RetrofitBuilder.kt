package com.letsconnect.retrofit

import com.google.gson.Gson
import com.letsconnect.repository.network.request.RemoteRepository
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.mockwebserver.MockWebServer
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitBuilder {
    companion object{
        fun getClient(mockWebServer: MockWebServer): RemoteRepository = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .client(OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(HttpLoggingInterceptor().apply{ level = HttpLoggingInterceptor.Level.BODY })
                .build())
            .baseUrl(mockWebServer.url("/"))
            .build()
            .create(RemoteRepository::class.java)
    }
}