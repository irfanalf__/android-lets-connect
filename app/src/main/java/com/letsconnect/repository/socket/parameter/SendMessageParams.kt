package com.letsconnect.repository.socket.parameter

import com.google.gson.annotations.SerializedName

data class SendMessageParams(@SerializedName("conversation_id")
                             val conversationId: String?,
                             @SerializedName("target_id")
                             val targetId: Int?,
                             @SerializedName("conversation")
                             val conversation: String?,
                             @SerializedName("reply_to")
                             val replyTo: String?,
                             @SerializedName("send_at")
                             val sendAt: Double?)