package com.letsconnect.repository.socket.parameter

import com.google.gson.annotations.SerializedName

data class AskLatestMessageStatusParams(@SerializedName("data")
                                        val data: List<Message>) {

    data class Message(@SerializedName("conversation_id") val conversationId: String)
}