package com.letsconnect.repository.socket.response

import com.google.gson.annotations.SerializedName
import com.letsconnect.ancestors.BaseResponse
import java.io.Serializable

class SomeoneOfflineResponse(
  @SerializedName("status")
  val status: Boolean,
  @SerializedName("message")
  val message: String,
  @SerializedName("last_online")
  val lastOnline: Double,
  @SerializedName("id_user")
  val idUser: String
): BaseResponse, Serializable{
  override fun status(): Boolean = status
  override fun message(): String = message
}