package com.letsconnect.repository.socket.parameter

import com.google.gson.annotations.SerializedName

data class TypingParams(@SerializedName("target_id") val targetId: Int)