package com.letsconnect.repository.socket.event

sealed class Emitter(val action: String) {
    class GoToBackground: Emitter("i'm going to background")
    class GoToForeground: Emitter("i'm going to foreground")
    class ReceivingMessage: Emitter("i'm receiving message")
    class ReadMessage: Emitter("i'm reading message")
    class AskForDelayedMessage: Emitter("give me my delayed message")
    class AskForUpdatedMessageReport: Emitter("give me my latest report message")
    class Typing: Emitter("i'm typing")
    class SendMessage: Emitter("i'm sending message")
}