package com.letsconnect.repository.socket.response

import com.google.gson.annotations.SerializedName
import com.letsconnect.ancestors.BaseResponse
import java.io.Serializable

data class TargetReadYourMessageResponse(
  @SerializedName("status")
  val status: Boolean,
  @SerializedName("message")
  val message: String,
  @SerializedName("receiver_id")
  val receiverId: Int,
  @SerializedName("read_at")
  val readAt: Double): BaseResponse, Serializable {

  override fun status(): Boolean = status
  override fun message(): String = message

}