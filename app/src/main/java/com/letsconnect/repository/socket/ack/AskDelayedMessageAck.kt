package com.letsconnect.repository.socket.ack

import com.google.gson.annotations.SerializedName
import com.letsconnect.ancestors.BaseResponse
import java.io.Serializable

data class AskDelayedMessageAck(
  @SerializedName("status")
  val status: Boolean,
  @SerializedName("message")
  val message: String,
  @SerializedName("data")
  val data: List<AskDelayedMessageData>
): BaseResponse, Serializable {

  override fun status(): Boolean = status
  override fun message(): String = message

  data class AskDelayedMessageData(
    @SerializedName("conversation_id")
    val idConversation: String,
    @SerializedName("conversation")
    val conversation: String,
    @SerializedName("reply_to")
    val replyTo: String?,
    @SerializedName("receiver_id")
    val receiverId: String,
    @SerializedName("received_by_target_at")
    val receivedByTargetAt: Double,
    @SerializedName("sender")
    val sender: Sender
  ): Serializable {

    data class Sender(
      @SerializedName("id")
      val id: Int,
      @SerializedName("email")
      val email: String,
      @SerializedName("fullname")
      val fullname: String,
      @SerializedName("image")
      val image: String,
      @SerializedName("username")
      val username: String
    ): Serializable

  }

}

