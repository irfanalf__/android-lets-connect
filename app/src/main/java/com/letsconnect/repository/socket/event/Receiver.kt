package com.letsconnect.repository.socket.event

import io.socket.client.Socket

sealed class Receiver(val action: String) {
    class Connect: Receiver(Socket.EVENT_CONNECT)
    class SomeoneOnline: Receiver("someone online")
    class SomeoneOffline: Receiver("someone offline")
    class SomeoneTyping: Receiver("someone typing")
    class SomeoneSendMessage: Receiver("someone send you message")
    class TargetReceivingYourMessage: Receiver("target receiving your message")
    class TargetReceivingYourDelayedMessage: Receiver("target receiving your delayed message")
    class TargetReadYourMessage: Receiver("target read your message")
    class Disconnect: Receiver(Socket.EVENT_DISCONNECT)
    class Exception: Receiver("default exception")
}