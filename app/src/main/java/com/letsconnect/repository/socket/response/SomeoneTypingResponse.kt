package com.letsconnect.repository.socket.response

import com.google.gson.annotations.SerializedName
import com.letsconnect.ancestors.BaseResponse
import java.io.Serializable

data class SomeoneTypingResponse(
  @SerializedName("status")
  val status: Boolean,
  @SerializedName("message")
  val message: String,
  @SerializedName("from_id")
  val fromId: String): BaseResponse, Serializable {

  override fun status(): Boolean = status
  override fun message(): String = message

}