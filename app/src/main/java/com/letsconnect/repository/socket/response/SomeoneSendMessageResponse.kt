package com.letsconnect.repository.socket.response

import com.google.gson.annotations.SerializedName
import com.letsconnect.ancestors.BaseResponse
import java.io.Serializable

data class SomeoneSendMessageResponse(
  @SerializedName("status")
  val status: Boolean,
  @SerializedName("message")
  val message: String,
  @SerializedName("conversation_id")
  val conversationId: String,
  @SerializedName("receiver_id")
  val receiverId: String,
  @SerializedName("conversation")
  val conversation: String,
  @SerializedName("reply_to")
  val replyTo: String?,
  @SerializedName("received_by_server_at")
  val receivedByServerAt: Double,
  @SerializedName("sender")
  val sender: Sender): BaseResponse, Serializable {

  override fun status(): Boolean = status
  override fun message(): String = message

  data class Sender(
    @SerializedName("id")
    val id: Int,
    @SerializedName("email")
    val email: String,
    @SerializedName("fullname")
    val fullname: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("username")
    val username: String
  ): Serializable

}