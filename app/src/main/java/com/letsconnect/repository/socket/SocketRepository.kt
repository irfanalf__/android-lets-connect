package com.letsconnect.repository.socket

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.letsconnect.BuildConfig
import com.letsconnect.ancestors.BaseResponse
import com.letsconnect.extension.NonNullLiveData
import com.letsconnect.model.Flavor
import com.letsconnect.repository.socket.ack.AskDelayedMessageAck
import com.letsconnect.repository.socket.ack.AskLatestMessageStatusAck
import com.letsconnect.repository.socket.ack.SendMessageAck
import com.letsconnect.repository.socket.event.Emitter
import com.letsconnect.repository.socket.event.Receiver
import com.letsconnect.repository.socket.parameter.*
import com.letsconnect.repository.socket.response.*
import com.mcnmr.utilities.extension.doIfTrue
import com.mcnmr.utilities.extension.isNull
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import io.socket.client.Ack
import io.socket.client.IO
import io.socket.client.Socket
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.HashSet

class SocketRepository(private val url: String) {
    companion object{
        const val HEADER_X_LETS_CONNECT_ID_USER = "x-lets-connect-id-user"
        const val HEADER_X_LETS_CONNECT_PLATFORM = "x-lets-connect-platform"

        const val PLATFORM = "android"
    }

    private var socket: Socket? = null
    private val gson = Gson()

    private var chatWith: Int? = null

    private var isFirstConnect = true

    val onFirstConnect = SingleEventWrapper<Void>()
    val onConnect = SingleEventWrapper<Void>()
    val onDisconnect = SingleEventWrapper<Any?>()
    val onException = SingleEventWrapper<BaseResponse>()
    val onSomeoneOnline = NonNullLiveData<HashSet<String>>(HashSet())
    val onSomeoneOffline = NonNullLiveData<HashMap<String, Double>>(HashMap())
    val onSomeoneTyping = NonNullLiveData<HashMap<String, Long>>(HashMap())
    val onSomeoneSendMessage = SingleEventWrapper<SomeoneSendMessageResponse>()
    val onTargetReceivingYourMessage = SingleEventWrapper<TargetReceivingYourMessageResponse>()
    val onTargetReadYourMessage = SingleEventWrapper<TargetReadYourMessageResponse>()

    fun connect(idUser: Int){
        socket.isNull().doIfTrue {
            val httpClient = OkHttpClient.Builder()
                .addNetworkInterceptor(object : Interceptor{
                    override fun intercept(chain: Interceptor.Chain): Response =
                        chain.proceed(chain.request().newBuilder()
                            .addHeader(HEADER_X_LETS_CONNECT_ID_USER, idUser.toString())
                            .addHeader(HEADER_X_LETS_CONNECT_PLATFORM, PLATFORM)
                            .build()
                        )
                }).build()

            val options = IO.Options().apply {
                webSocketFactory = httpClient
                callFactory = httpClient
            }

            socket = IO.socket(url, options)
            initReceiver()
        }

        socket?.connect()
    }

    fun disconnect(){
        socket?.disconnect()
        socket = null
    }

    private fun initReceiver(){
        socket?.on(Receiver.Connect().action){
            if(isFirstConnect){
                isFirstConnect = false
                onFirstConnect.postTrigger()
            }

            onConnect.postTrigger()
        }
        socket?.on(Receiver.SomeoneOnline().action){
            val response = gson.fromJson(it[0].toString(), SomeoneOnlineResponse::class.java)
            onSomeoneOnline.postValue(onSomeoneOnline.value.apply { add(response.idUser) })
            onSomeoneOffline.postValue(onSomeoneOffline.value.apply { remove(response.idUser) })
        }
        socket?.on(Receiver.SomeoneOffline().action){
            val response = gson.fromJson(it[0].toString(), SomeoneOfflineResponse::class.java)
            onSomeoneOnline.postValue(onSomeoneOnline.value.apply { remove(response.idUser) })
            onSomeoneOffline.postValue(onSomeoneOffline.value.apply {
                this[response.idUser] = response.lastOnline })
        }
        socket?.on(Receiver.SomeoneTyping().action){
            val response = gson.fromJson(it[0].toString(), SomeoneTypingResponse::class.java)
            onSomeoneTyping.postValue(onSomeoneTyping.value.apply {
                this[response.fromId] = System.currentTimeMillis()
            })
        }
        socket?.on(Receiver.SomeoneSendMessage().action){
            Log.e("Someone send message", it[0].toString())
            val response = gson.fromJson(it[0].toString(), SomeoneSendMessageResponse::class.java)
            onSomeoneSendMessage.postValue(response)
            onSomeoneTyping.postValue(onSomeoneTyping.value.apply { remove(response.sender.id.toString()) })
        }
        socket?.on(Receiver.TargetReceivingYourMessage().action){
            val response = gson.fromJson(it[0].toString(), TargetReceivingYourMessageResponse::class.java)
            onTargetReceivingYourMessage.postValue(response)
        }
        socket?.on(Receiver.TargetReceivingYourDelayedMessage().action){
            val response = gson.fromJson(it[0].toString(), TargetReceivingYourMessageResponse::class.java)
            onTargetReceivingYourMessage.postValue(response)
        }
        socket?.on(Receiver.TargetReadYourMessage().action){
            val response = gson.fromJson(it[0].toString(), TargetReadYourMessageResponse::class.java)
            onTargetReadYourMessage.postValue(response)
        }
        socket?.on(Receiver.Disconnect().action){
            onDisconnect.postTrigger()
        }
        socket?.on(Receiver.Exception().action){
            Log.e("Exception", "Called with response ${Arrays.toString(it)}")
            onException.postTrigger()
        }
    }

    fun joinPrivateChatRoom(id: Int){
        chatWith = id
        imReadMessage(ImReadMessageParams(id))
    }
    fun exitPrivateChatRoom(){ chatWith = null }
    fun isActiveInRoom(id: Int): Boolean = chatWith == id

    //Emitten
    fun typing(params: TypingParams){
        socket?.emit(Emitter.Typing().action, gson.toJson(params))
    }

    fun imReceivingMessage(params: ImReceivingMessageParams){
        socket?.emit(Emitter.ReceivingMessage().action, gson.toJson(params))
    }

    fun imReadMessage(params: ImReadMessageParams){
        socket?.emit(Emitter.ReadMessage().action, gson.toJson(params))
    }

    fun sendMessage(params: SendMessageParams, callback: (SendMessageAck) -> Unit){
        socket?.emit(Emitter.SendMessage().action, gson.toJson(params), object : Ack{
            override fun call(vararg args: Any?) {
                val ack = gson.fromJson(args[0].toString(), SendMessageAck::class.java)
                callback.invoke(ack)
            }
        })
    }

    fun askLatestMessageStatus(params: AskLatestMessageStatusParams, callback: (AskLatestMessageStatusAck) -> Unit){
        socket?.emit(Emitter.AskForUpdatedMessageReport().action, gson.toJson(params), object : Ack {
            override fun call(vararg args: Any?) {
                val ack = gson.fromJson(args[0].toString(), AskLatestMessageStatusAck::class.java)
                callback.invoke(ack)
            }
        })
    }

    fun askForDelayedMessage(callback: (AskDelayedMessageAck) -> Unit){
        socket?.emit(Emitter.AskForDelayedMessage().action, "", object : Ack {
            override fun call(vararg args: Any?) {
                val ack = gson.fromJson(args[0].toString(), AskDelayedMessageAck::class.java)
                callback.invoke(ack)
            }
        })
    }

    fun gotoBackground(){
        socket?.emit(Emitter.GoToBackground().action)
    }

    fun gotoForeground(){
        socket?.emit(Emitter.GoToForeground().action)
    }
}