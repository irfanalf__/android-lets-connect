package com.letsconnect.repository.socket.parameter

import com.google.gson.annotations.SerializedName

data class ImReadMessageParams(@SerializedName("sender_id") val senderId: Int)