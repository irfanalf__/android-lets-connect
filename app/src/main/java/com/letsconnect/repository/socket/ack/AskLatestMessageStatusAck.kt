package com.letsconnect.repository.socket.ack

import com.google.gson.annotations.SerializedName

data class AskLatestMessageStatusAck(@SerializedName("data")
                                     val data: List<Message>) {

    data class Message(@SerializedName("conversation_id")
                       val conversationId: String,
                       @SerializedName("status")
                       val status: Int,
                       @SerializedName("sent_at")
                       val sentAt: Double,
                       @SerializedName("read_at")
                       val readAt: Double)

}