package com.letsconnect.repository.socket.parameter

import com.google.gson.annotations.SerializedName

data class ImReceivingMessageParams(@SerializedName("conversation_id")
                                    val conversationId: String,
                                    @SerializedName("sender_id")
                                    val senderId: Int)