package com.letsconnect.repository.socket.ack

import com.google.gson.annotations.SerializedName
import com.letsconnect.ancestors.BaseResponse

data class SendMessageAck(
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("message")
    val message: String,
    @SerializedName("conversation_id")
    val conversationId: String,
    @SerializedName("received_by_server_at")
    val receivedByServerAt: Long
) : BaseResponse {
    override fun status(): Boolean = status
    override fun message(): String = message
}