package com.letsconnect.repository.socket.response

import com.google.gson.annotations.SerializedName
import com.letsconnect.ancestors.BaseResponse

data class TargetReceivingYourMessageResponse(
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("message")
    val message: String,
    @SerializedName("conversation_id")
    val conversationId: String,
    @SerializedName("sender_id")
    val senderId: String,
    @SerializedName("sent_at")
    val sentAt: Long
) : BaseResponse {

    override fun status(): Boolean = status
    override fun message(): String = message

}