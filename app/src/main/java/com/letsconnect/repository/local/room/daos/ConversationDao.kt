package com.letsconnect.repository.local.room.daos

import androidx.room.*
import com.letsconnect.repository.local.room.LocalDatabase
import com.letsconnect.repository.local.room.entities.Conversation

@Dao
interface ConversationDao {

    @Query("SELECT * FROM ${LocalDatabase.CONVERSATION_TABLE} " +
            "WHERE ${Conversation.COLUMN_CONVERSATION_STATUS} = ${Conversation.STATUS_SEND}")
    suspend fun getMyUndeliveredMessage(): List<Conversation>

    @Query("SELECT * FROM ${LocalDatabase.CONVERSATION_TABLE} " +
            "WHERE (${Conversation.COLUMN_CONVERSATION_STATUS} = ${Conversation.STATUS_RECEIVED_BY_SERVER} OR ${Conversation.COLUMN_CONVERSATION_STATUS} = ${Conversation.STATUS_SENT}) " +
            "AND ${Conversation.COLUMN_WHO_IS_SENDER} = ${Conversation.ME}")
    suspend fun getMyUnreadUnsentMessage(): List<Conversation>

    @Query("UPDATE ${LocalDatabase.CONVERSATION_TABLE} " +
            "SET ${Conversation.COLUMN_CONVERSATION_STATUS} = ${Conversation.STATUS_READ}, ${Conversation.COLUMN_READ_AT} = :readAt " +
            "WHERE ${Conversation.COLUMN_INTERLOCUTOR_ID} = :id AND ${Conversation.COLUMN_WHO_IS_SENDER} = ${Conversation.THEM}")
    suspend fun readMessage(readAt: Double, id: Int)

    @Query("UPDATE ${LocalDatabase.CONVERSATION_TABLE} " +
            "SET ${Conversation.COLUMN_CONVERSATION_STATUS} = ${Conversation.STATUS_RECEIVED_BY_SERVER}, ${Conversation.COLUMN_RECEIVED_BY_SERVER_AT} = :receivedByServerAt " +
            "WHERE ${Conversation.COLUMN_CONVERSATION_ID} = :id")
    suspend fun serverReceivingMyMessage(receivedByServerAt: Double, id: String)

    @Query("UPDATE ${LocalDatabase.CONVERSATION_TABLE} " +
            "SET ${Conversation.COLUMN_CONVERSATION_STATUS} = ${Conversation.STATUS_SENT}, ${Conversation.COLUMN_SENT_AT} = :sentAt " +
            "WHERE ${Conversation.COLUMN_CONVERSATION_ID} = :id")
    suspend fun targetReceivingMyMessage(sentAt: Double, id: String)

    @Query("UPDATE ${LocalDatabase.CONVERSATION_TABLE} " +
            "SET ${Conversation.COLUMN_CONVERSATION_STATUS} = ${Conversation.STATUS_READ}, ${Conversation.COLUMN_READ_AT} = :readAt " +
            "WHERE ${Conversation.COLUMN_INTERLOCUTOR_ID} = :id AND ${Conversation.COLUMN_WHO_IS_SENDER} = ${Conversation.ME} AND ${Conversation.COLUMN_READ_AT} IS NULL")
    suspend fun targetReadMyMessage(readAt: Double, id: Int)

    @Query("UPDATE ${LocalDatabase.CONVERSATION_TABLE} " +
            "SET ${Conversation.COLUMN_CONVERSATION_STATUS} = ${Conversation.STATUS_SENT}, ${Conversation.COLUMN_SENT_AT} = :sentAt " +
            "WHERE ${Conversation.COLUMN_CONVERSATION_ID} = :id")
    suspend fun updateStatusMessageToSent(sentAt: Double, id: String)

    @Query("UPDATE ${LocalDatabase.CONVERSATION_TABLE} " +
            "SET ${Conversation.COLUMN_CONVERSATION_STATUS} = ${Conversation.STATUS_READ}, ${Conversation.COLUMN_READ_AT} = :readAt " +
            "WHERE ${Conversation.COLUMN_CONVERSATION_ID} = :id")
    suspend fun updateStatusMessageToRead(readAt: Double, id: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertConversation(conversation: Conversation): Long

    @Query("DELETE FROM ${LocalDatabase.CONVERSATION_TABLE}")
    suspend fun clearConversation()
}