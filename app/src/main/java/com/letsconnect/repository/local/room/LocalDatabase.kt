package com.letsconnect.repository.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.letsconnect.repository.local.room.daos.ConversationDao
import com.letsconnect.repository.local.room.daos.InterlocutorDao
import com.letsconnect.repository.local.room.daos.UniversalDao
import com.letsconnect.repository.local.room.entities.Conversation
import com.letsconnect.repository.local.room.entities.Interlocutor

@Database(entities = [Interlocutor::class, Conversation::class],
    version = LocalDatabase.VERSION,
    exportSchema = false)
abstract class LocalDatabase : RoomDatabase() {
    companion object {
        const val INTERLOCUTOR_TABLE = "interlocutor"
        const val CONVERSATION_TABLE = "conversation"

        const val VERSION = 2
    }

    abstract val interlocutorDao: InterlocutorDao
    abstract val conversationDao: ConversationDao
    abstract val universalDao: UniversalDao
}