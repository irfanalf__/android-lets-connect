package com.letsconnect.repository.local.room.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.letsconnect.repository.local.LocalRepository
import com.letsconnect.repository.local.room.LocalDatabase
import com.letsconnect.repository.local.room.custom_row.CompleteConversation
import com.letsconnect.repository.local.room.custom_row.InterlocutorMessagesRow
import com.letsconnect.repository.local.room.entities.Conversation
import com.letsconnect.repository.local.room.entities.Interlocutor

@Dao
interface UniversalDao {

    @Query("SELECT c1.*, c2.${Conversation.COLUMN_CONVERSATION} AS ${CompleteConversation.COLUMN_REPLY_MESSAGE} " +
            "FROM ${LocalDatabase.CONVERSATION_TABLE} c1 " +
            "LEFT JOIN ${LocalDatabase.CONVERSATION_TABLE} c2 " +
            "ON c1.${Conversation.COLUMN_REPLY_TO} = c2.${Conversation.COLUMN_CONVERSATION_ID} " +
            "WHERE c1.${Conversation.COLUMN_INTERLOCUTOR_ID} = :id " +
            "ORDER BY c1.${Conversation.COLUMN_SEND_AT} ASC")
    fun watchConversationWithThisInterlocutor(id: Int): LiveData<List<CompleteConversation>>

    @Query("SELECT i.*, c.*, " +
            "(SELECT COUNT(ci.${Conversation.COLUMN_CONVERSATION_ID}) FROM ${LocalDatabase.CONVERSATION_TABLE} ci " +
            "WHERE ci.${Conversation.COLUMN_INTERLOCUTOR_ID} = i.${Interlocutor.COLUMN_ID} AND ci.${Conversation.COLUMN_CONVERSATION_STATUS} != 3 " +
            "AND ci.${Conversation.COLUMN_WHO_IS_SENDER} = -2) AS ${InterlocutorMessagesRow.COLUMN_UNREAD_COUNT} " +
            "FROM ${LocalDatabase.INTERLOCUTOR_TABLE} i " +
            "INNER JOIN (SELECT ${Conversation.COLUMN_INTERLOCUTOR_ID}, MAX(${Conversation.COLUMN_SEND_AT}) as MaxDate " +
            "FROM ${LocalDatabase.CONVERSATION_TABLE} GROUP BY ${Conversation.COLUMN_INTERLOCUTOR_ID}) MaxDates " +
            "ON i.${Interlocutor.COLUMN_ID} = MaxDates.${Conversation.COLUMN_INTERLOCUTOR_ID} " +
            "INNER JOIN ${LocalDatabase.CONVERSATION_TABLE} c " +
            "ON MaxDates.${Conversation.COLUMN_INTERLOCUTOR_ID} = c.${Conversation.COLUMN_INTERLOCUTOR_ID} " +
            "AND MaxDates.MaxDate = c.${Conversation.COLUMN_SEND_AT} " +
            "GROUP BY i.${Interlocutor.COLUMN_ID} " +
            "ORDER BY c.${Conversation.COLUMN_SEND_AT} DESC")
    fun watchInterlocutorMessages(): LiveData<List<InterlocutorMessagesRow>>

}