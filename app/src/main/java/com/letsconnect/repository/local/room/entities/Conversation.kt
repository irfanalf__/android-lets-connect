package com.letsconnect.repository.local.room.entities

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.letsconnect.repository.local.room.LocalDatabase

@Entity(tableName = LocalDatabase.CONVERSATION_TABLE)
class Conversation {
    companion object{
        const val COLUMN_CONVERSATION_ID = "conversation_id"
        const val COLUMN_INTERLOCUTOR_ID = "interlocutor_id"
        const val COLUMN_CONVERSATION = "conversation"
        const val COLUMN_CONVERSATION_STATUS = "conversation_status"
        const val COLUMN_WHO_IS_SENDER = "who_is_sender"
        const val COLUMN_SEND_AT = "send_at"
        const val COLUMN_REPLY_TO = "reply_to"
        const val COLUMN_RECEIVED_BY_SERVER_AT = "received_by_server_at"
        const val COLUMN_SENT_AT = "sent_at"
        const val COLUMN_READ_AT = "read_at"

        const val STATUS_SEND = 0
        const val STATUS_RECEIVED_BY_SERVER = 1
        const val STATUS_SENT = 2
        const val STATUS_READ = 3

        const val ME = -1
        const val THEM = -2
    }

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = COLUMN_CONVERSATION_ID)
    var conversationId: String = ""

    @ColumnInfo(name = COLUMN_INTERLOCUTOR_ID)
    var interlocutorId: Int? = null

    @ColumnInfo(name = COLUMN_CONVERSATION)
    var conversation: String? = null

    @ColumnInfo(name = COLUMN_CONVERSATION_STATUS)
    var conversationStatus: Int? = null

    @ColumnInfo(name = COLUMN_WHO_IS_SENDER)
    var whoIsSender: Int? = null

    @ColumnInfo(name = COLUMN_SEND_AT)
    var sendAt: Double? = null

    @ColumnInfo(name = COLUMN_REPLY_TO)
    var replyTo: String? = null

    @ColumnInfo(name = COLUMN_RECEIVED_BY_SERVER_AT)
    var receivedByServerAt: Double? = null

    @ColumnInfo(name = COLUMN_SENT_AT)
    var sentAt: Double? = null

    @ColumnInfo(name = COLUMN_READ_AT)
    var readAt: Double? = null

}