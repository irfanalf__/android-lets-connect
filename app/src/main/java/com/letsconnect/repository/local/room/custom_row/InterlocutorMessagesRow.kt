package com.letsconnect.repository.local.room.custom_row

import androidx.room.ColumnInfo
import com.letsconnect.repository.local.room.entities.Conversation
import com.letsconnect.repository.local.room.entities.Interlocutor

class InterlocutorMessagesRow {
    companion object {
        const val COLUMN_UNREAD_COUNT = "unread_count"
    }

    @ColumnInfo(name = Interlocutor.COLUMN_ID)
    var id: Int = -1

    @ColumnInfo(name = Interlocutor.COLUMN_EMAIL)
    var email: String? = null

    @ColumnInfo(name = Interlocutor.COLUMN_FULLNAME)
    var fullname: String? = null

    @ColumnInfo(name = Interlocutor.COLUMN_IMAGE)
    var image: String? = null

    @ColumnInfo(name = Interlocutor.COLUMN_USERNAME)
    var username: String? = null

    @ColumnInfo(name = Conversation.COLUMN_CONVERSATION_ID)
    var conversationId: String = ""

    @ColumnInfo(name = Conversation.COLUMN_INTERLOCUTOR_ID)
    var interlocutorId: Int = -1

    @ColumnInfo(name = Conversation.COLUMN_CONVERSATION)
    var conversation: String? = null

    @ColumnInfo(name = Conversation.COLUMN_CONVERSATION_STATUS)
    var conversationStatus: Int? = null

    @ColumnInfo(name = Conversation.COLUMN_WHO_IS_SENDER)
    var whoIsSender: Int? = null

    @ColumnInfo(name = Conversation.COLUMN_SEND_AT)
    var sendAt: Double? = null

    @ColumnInfo(name = Conversation.COLUMN_REPLY_TO)
    var replyTo: String? = null

    @ColumnInfo(name = Conversation.COLUMN_RECEIVED_BY_SERVER_AT)
    var receivedByServerAt: Double? = null

    @ColumnInfo(name = Conversation.COLUMN_SENT_AT)
    var sentAt: Double? = null

    @ColumnInfo(name = Conversation.COLUMN_READ_AT)
    var readAt: Double? = null

    @ColumnInfo(name = COLUMN_UNREAD_COUNT)
    var unreadCount: Int = 0

    fun isFromThemAndRead(): Boolean = whoIsSender == Conversation.THEM &&
            conversationStatus == Conversation.STATUS_READ
}