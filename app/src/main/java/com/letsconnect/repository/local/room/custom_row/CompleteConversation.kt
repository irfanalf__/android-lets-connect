package com.letsconnect.repository.local.room.custom_row

import androidx.room.ColumnInfo
import com.letsconnect.repository.local.room.entities.Conversation

class CompleteConversation {
    companion object{
        const val COLUMN_REPLY_MESSAGE = "reply_message"
    }

    @ColumnInfo(name = Conversation.COLUMN_CONVERSATION_ID)
    var conversationId: String = ""

    @ColumnInfo(name = Conversation.COLUMN_INTERLOCUTOR_ID)
    var interlocutorId: Int? = null

    @ColumnInfo(name = Conversation.COLUMN_CONVERSATION)
    var conversation: String? = null

    @ColumnInfo(name = Conversation.COLUMN_CONVERSATION_STATUS)
    var conversationStatus: Int? = null

    @ColumnInfo(name = Conversation.COLUMN_WHO_IS_SENDER)
    var whoIsSender: Int? = null

    @ColumnInfo(name = Conversation.COLUMN_REPLY_TO)
    var replyTo: String? = null

    @ColumnInfo(name = COLUMN_REPLY_MESSAGE)
    var replyMessage: String? = null

    @ColumnInfo(name = Conversation.COLUMN_SEND_AT)
    var sendAt: Double? = null

    @ColumnInfo(name = Conversation.COLUMN_RECEIVED_BY_SERVER_AT)
    var receivedByServerAt: Double? = null

    @ColumnInfo(name = Conversation.COLUMN_SENT_AT)
    var sentAt: Double? = null

    @ColumnInfo(name = Conversation.COLUMN_READ_AT)
    var readAt: Double? = null
}