package com.letsconnect.repository.local.room.daos

import androidx.room.*
import com.letsconnect.repository.local.room.LocalDatabase
import com.letsconnect.repository.local.room.entities.Interlocutor

@Dao
interface InterlocutorDao {

    @Query("SELECT EXISTS(SELECT * FROM ${LocalDatabase.INTERLOCUTOR_TABLE} WHERE ${Interlocutor.COLUMN_ID} = :id)")
    suspend fun isInterlocutorExists(id: Int): Boolean

    @Insert
    suspend fun insertInterlocutor(interlocutor: Interlocutor): Long

    @Update
    suspend fun updateInterlocutor(interlocutor: Interlocutor)

    @Query("DELETE FROM ${LocalDatabase.INTERLOCUTOR_TABLE}")
    suspend fun clearInterlocutor()
}