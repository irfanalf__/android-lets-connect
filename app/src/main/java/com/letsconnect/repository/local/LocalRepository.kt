package com.letsconnect.repository.local

import androidx.lifecycle.LiveData
import com.letsconnect.repository.local.room.custom_row.CompleteConversation
import com.letsconnect.repository.socket.ack.AskDelayedMessageAck
import com.letsconnect.repository.socket.ack.AskLatestMessageStatusAck
import com.letsconnect.repository.socket.ack.SendMessageAck
import com.letsconnect.repository.socket.response.SomeoneSendMessageResponse
import com.letsconnect.repository.socket.response.TargetReadYourMessageResponse
import com.letsconnect.repository.socket.response.TargetReceivingYourMessageResponse
import com.letsconnect.repository.local.room.custom_row.InterlocutorMessagesRow
import com.letsconnect.repository.local.room.daos.ConversationDao
import com.letsconnect.repository.local.room.daos.InterlocutorDao
import com.letsconnect.repository.local.room.daos.UniversalDao
import com.letsconnect.repository.local.room.entities.Conversation
import com.letsconnect.repository.local.room.entities.Interlocutor
import com.letsconnect.repository.network.response.UserResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class LocalRepository(private val interlocutorDao: InterlocutorDao,
                      private val conversationDao: ConversationDao,
                      private val universalDao: UniversalDao) {

    fun watchConversation(id: Int): LiveData<List<CompleteConversation>> = universalDao
        .watchConversationWithThisInterlocutor(id)

    fun watchInterlocutorMessages(): LiveData<List<InterlocutorMessagesRow>> = universalDao
        .watchInterlocutorMessages()

    suspend fun insertHistoryChat(data: UserResponse){
        withContext(Dispatchers.IO){
            data.historyChat.forEach { person ->
                val interlocutor = Interlocutor().apply {
                    id = person.id
                    email = person.email
                    fullname = person.fullname
                    image = person.image
                    username = person.username
                }

                if(interlocutorDao.isInterlocutorExists(person.id)){
                    interlocutorDao.updateInterlocutor(interlocutor)
                }else {
                    interlocutorDao.insertInterlocutor(interlocutor)
                }

                person.conversations.forEach { c ->
                    val conversation = Conversation().apply {
                        conversationId = c.idConversation
                        interlocutorId = person.id
                        conversation = c.conversation
                        replyTo = c.replyTo
                        conversationStatus = c.status
                        whoIsSender = if(c.chatSender == person.id) Conversation.THEM else Conversation.ME
                        sendAt = c.sendAt
                        receivedByServerAt = c.receivedByServerAt
                        sentAt = c.sendAt
                        readAt = c.readAt
                        replyTo = c.replyTo
                    }

                    conversationDao.insertConversation(conversation)
                }
            }
        }
    }

    suspend fun receivingMessage(data: SomeoneSendMessageResponse){
        withContext(Dispatchers.IO){
            val interlocutor = Interlocutor().apply {
                id = data.sender.id
                email = data.sender.email
                fullname = data.sender.fullname
                image = data.sender.image
                username = data.sender.username
            }

            if(interlocutorDao.isInterlocutorExists(data.sender.id)){
                interlocutorDao.updateInterlocutor(interlocutor)
            }else {
                interlocutorDao.insertInterlocutor(interlocutor)
            }

            val conversation = Conversation().apply {
                conversationId = data.conversationId
                interlocutorId = data.sender.id
                conversation = data.conversation
                replyTo = data.replyTo
                conversationStatus = Conversation.STATUS_SENT
                whoIsSender = Conversation.THEM
                sendAt = data.receivedByServerAt
                receivedByServerAt = data.receivedByServerAt
                sentAt = data.receivedByServerAt
                readAt = null
            }

            conversationDao.insertConversation(conversation)
        }
    }

    suspend fun iReadMessageOfThisInterlocutor(id: Int){
        withContext(Dispatchers.IO){
            conversationDao.readMessage(System.currentTimeMillis().toDouble(), id)
        }
    }

    suspend fun sendMessage(interlocutor: Interlocutor, conversation: Conversation){
        withContext(Dispatchers.IO){
            if(interlocutorDao.isInterlocutorExists(interlocutor.id)){
                interlocutorDao.updateInterlocutor(interlocutor)
            }else {
                interlocutorDao.insertInterlocutor(interlocutor)
            }
            conversationDao.insertConversation(conversation)
        }
    }

    suspend fun serverReceivingMessage(ack: SendMessageAck){
        withContext(Dispatchers.IO){
            conversationDao.serverReceivingMyMessage(ack.receivedByServerAt.toDouble(), ack.conversationId)
        }
    }

    suspend fun targetReceivingMyMessage(data: TargetReceivingYourMessageResponse){
        withContext(Dispatchers.IO){
            conversationDao.targetReceivingMyMessage(data.sentAt.toDouble(), data.conversationId)
        }
    }

    suspend fun targetReadMyMessage(data: TargetReadYourMessageResponse){
        withContext(Dispatchers.IO){
            conversationDao.targetReadMyMessage(data.readAt, data.receiverId)
        }
    }

    suspend fun getMyUndeliveredMessage(): List<Conversation> =
        withContext(Dispatchers.IO){
            conversationDao.getMyUndeliveredMessage()
        }

    suspend fun getMyUnreadUnsentMessage(): List<Conversation> =
        withContext(Dispatchers.IO){
            conversationDao.getMyUnreadUnsentMessage()
        }

    suspend fun updateMessageWithLatestStatus(data: AskLatestMessageStatusAck){
        withContext(Dispatchers.IO){
            data.data.forEach {
                when(it.status){
                    Conversation.STATUS_SENT -> conversationDao.updateStatusMessageToSent(it.sentAt, it.conversationId)
                    Conversation.STATUS_READ -> conversationDao.updateStatusMessageToRead(it.readAt, it.conversationId)
                }
            }
        }
    }

    suspend fun receiveDelayedMessage(data: AskDelayedMessageAck){
        withContext(Dispatchers.IO){
            data.data.forEach {
                val interlocutor = Interlocutor().apply {
                    id = it.sender.id
                    email = it.sender.email
                    fullname = it.sender.fullname
                    image = it.sender.image
                    username = it.sender.username
                }

                val conversation = Conversation().apply {
                    interlocutorId = it.sender.id
                    conversationId = it.idConversation
                    conversation = it.conversation
                    replyTo = it.replyTo
                    conversationStatus = Conversation.STATUS_SENT
                    whoIsSender = Conversation.THEM
                    sendAt = it.receivedByTargetAt
                    receivedByServerAt = it.receivedByTargetAt
                    sentAt = it.receivedByTargetAt
                }

                if(interlocutorDao.isInterlocutorExists(interlocutor.id)){
                    interlocutorDao.updateInterlocutor(interlocutor)
                }else {
                    interlocutorDao.insertInterlocutor(interlocutor)
                }

                conversationDao.insertConversation(conversation)
            }
        }
    }

    suspend fun truncateAll(){
        withContext(Dispatchers.IO){
            interlocutorDao.clearInterlocutor()
            conversationDao.clearConversation()
        }
    }
}