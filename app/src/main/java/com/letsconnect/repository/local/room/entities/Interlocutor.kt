package com.letsconnect.repository.local.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.letsconnect.repository.local.room.LocalDatabase
import java.io.Serializable

@Entity(tableName = LocalDatabase.INTERLOCUTOR_TABLE)
class Interlocutor: Serializable {
    companion object {
        const val COLUMN_ID = "id"
        const val COLUMN_EMAIL = "email"
        const val COLUMN_FULLNAME = "fullname"
        const val COLUMN_IMAGE = "image"
        const val COLUMN_USERNAME = "username"
    }

    @PrimaryKey@ColumnInfo(name = COLUMN_ID)
    var id: Int = -1

    @ColumnInfo(name = COLUMN_EMAIL)
    var email: String? = null

    @ColumnInfo(name = COLUMN_FULLNAME)
    var fullname: String? = null

    @ColumnInfo(name = COLUMN_IMAGE)
    var image: String? = null

    @ColumnInfo(name = COLUMN_USERNAME)
    var username: String? = null

}