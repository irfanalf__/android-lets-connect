package com.letsconnect.repository.network.response

import com.google.gson.annotations.SerializedName
import com.letsconnect.ancestors.BaseResponse

data class DiscoverByRoleResponse(
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val data: List<DiscoverPersonResponse.Data.Person>
): BaseResponse {
    override fun status(): Boolean = status
    override fun message(): String = message
}