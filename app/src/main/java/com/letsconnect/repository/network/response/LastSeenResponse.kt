package com.letsconnect.repository.network.response

import com.google.gson.annotations.SerializedName
import com.letsconnect.ancestors.BaseResponse

data class LastSeenResponse(
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("message")
    val message: String,
    @SerializedName("last_online")
    val lastOnline: Double,
    @SerializedName("status_online")
    val statusOnline: Int
) : BaseResponse {

    override fun status(): Boolean = status
    override fun message(): String = message

}