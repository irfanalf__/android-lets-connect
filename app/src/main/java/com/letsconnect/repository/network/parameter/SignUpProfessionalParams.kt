package com.letsconnect.repository.network.parameter

import com.letsconnect.repository.network.response.RegistrationDataResponse
import com.mcnmr.utilities.extension.doIfTrue
import com.mcnmr.utilities.extension.isNotEmptyOrNotNull
import com.mcnmr.utilities.extension.notNullAndTrue
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import org.json.JSONArray
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject


class SignUpProfessionalParams(private val role: Int?,
                               private val introductionVideo: String?,
                               private val fullName: String?,
                               private val skypeId: String?,
                               private val gender: String?,
                               private val locationCode: String?,
                               private val aboutMe: String?,
                               private val licensing: String?,
                               private val certification: String?,
                               private val password: String?,
                               private val specialize: List<RegistrationDataResponse.Specialize>?,
                               private val isLiveChat: Boolean?,
                               private val isVoiceCall: Boolean?,
                               private val isVideoCall: Boolean?,
                               private val speakFluent: String?,
                               private val charge20: String?,
                               private val charge50: String?,
                               private val email: String?,
                               private val isWithAdults: Boolean?,
                               private val isWithAdolescents: Boolean?,
                               private val isWithCouples: Boolean?) {

    fun asMultipart(): MutableMap<String, RequestBody> = mutableMapOf<String, RequestBody>().apply {
        var providesSessions = ""
        var workWith = ""

        role?.let { this["role"] = it.toString().toRequestBody("text/plain".toMediaTypeOrNull()) }
        introductionVideo?.let { this["introduction_video"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        fullName?.let { this["full_name"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        skypeId?.let { this["skype_id"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        gender?.let { this["gender"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        locationCode?.let { this["location_code"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        aboutMe?.let { this["about_me"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        licensing?.let { this["licensing"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        certification?.let { this["certification"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        password?.let { this["password"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        specialize?.let {
            if(it.isNotEmpty()){
                val json = JSONArray()
                it.forEach { spec ->
                    json.put(JSONObject().apply { put("id", spec.id) }.toString())
                }
                this["specialize"] = json.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            }
        }
        isLiveChat.notNullAndTrue().doIfTrue { providesSessions += "Live Chat"}
        isVoiceCall.notNullAndTrue().doIfTrue {
            if(providesSessions != "") providesSessions += ", Voice Call"
            else providesSessions = "Voice Call"
        }
        isVideoCall.notNullAndTrue().doIfTrue {
            if(providesSessions != "") providesSessions += ", Video Call"
            else providesSessions = "Video Call"
        }
        providesSessions.isNotEmptyOrNotNull().doIfTrue {
            this["provides_sessions"] = providesSessions.toRequestBody("text/plain".toMediaTypeOrNull())
        }
        speakFluent?.let { this["speak_fluent"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        charge20?.let { this["charge_20"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        charge50?.let { this["charge_50"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        email?.let { this["email"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        isWithAdults.notNullAndTrue().doIfTrue { workWith += "Adults" }
        isWithAdolescents.notNullAndTrue().doIfTrue {
            if(workWith != "") workWith += ", Adolescents"
            else workWith = "Adolescents"
        }
        isWithCouples.notNullAndTrue().doIfTrue {
            if(workWith != "") workWith += ", Couples"
            else workWith = "Couples"
        }
        workWith.isNotEmptyOrNotNull().doIfTrue {
            this["work_with"] = workWith.toRequestBody("text/plain".toMediaTypeOrNull())
        }
    }

    fun asMap(): MutableMap<String, String> = mutableMapOf<String, String>().apply {
        var providesSessions = ""
        var workWith = ""

        role?.let { this["role"] = it.toString() }
        introductionVideo?.let { this["introduction_video"] = it }
        fullName?.let { this["full_name"] = it }
        skypeId?.let { this["skype_id"] = it }
        gender?.let { this["gender"] = it }
        locationCode?.let { this["location_code"] = it }
        aboutMe?.let { this["about_me"] = it }
        licensing?.let { this["licensing"] = it }
        certification?.let { this["certification"] = it }
        password?.let { this["password"] = it }
        specialize?.let {
            if(it.isNotEmpty()){
                val listSpecializations = mutableListOf<String>()
                it.forEach { spec ->
                    listSpecializations.add(spec.id)
                }
                this["specialize"] = JSONArray(it).toString()
            }
        }
        isLiveChat.notNullAndTrue().doIfTrue { providesSessions += "Live Chat"}
        isVoiceCall.notNullAndTrue().doIfTrue {
            if(providesSessions != "") providesSessions += ", Voice Call"
            else providesSessions = "Voice Call"
        }
        isVideoCall.notNullAndTrue().doIfTrue {
            if(providesSessions != "") providesSessions += ", Video Call"
            else providesSessions = "Video Call"
        }
        providesSessions.isNotEmptyOrNotNull().doIfTrue {
            this["provides_sessions"] = providesSessions
        }
        speakFluent?.let { this["speak_fluent"] = it }
        charge20?.let { this["charge_20"] = it }
        charge50?.let { this["charge_50"] = it }
        email?.let { this["email"] = it }
        isWithAdults.notNullAndTrue().doIfTrue { workWith += "Adults" }
        isWithAdolescents.notNullAndTrue().doIfTrue {
            if(workWith != "") workWith += ", Adolescents"
            else workWith = "Adolescents"
        }
        isWithCouples.notNullAndTrue().doIfTrue {
            if(workWith != "") workWith += ", Couples"
            else workWith = "Couples"
        }
        workWith.isNotEmptyOrNotNull().doIfTrue {
            this["work_with"] = workWith
        }
    }
}