package com.letsconnect.repository.network.request

import com.letsconnect.ancestors.BaseResponse
import com.letsconnect.repository.network.parameter.ErrorReportParams
import com.letsconnect.repository.network.parameter.LoginParams
import com.letsconnect.repository.network.parameter.SignUpClientParams
import com.letsconnect.repository.network.response.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface RemoteRepository {

    //Registration
    @GET("registration/registration_data")
    suspend fun registrationData(): RegistrationDataResponse

    @POST("registration/client")
    suspend fun registerAsClient(@Body params: SignUpClientParams): NoDataResponse

    @Multipart
    @POST("registration/volunteer")
    suspend fun registerAsVolunteer(@PartMap params: MutableMap<String, RequestBody>,
                                    @Part file: MultipartBody.Part): NoDataResponse

    @Multipart
    @POST("registration/professional")
    suspend fun registerAsProfessional(@PartMap params: MutableMap<String, RequestBody>,
                                       @Part file: MultipartBody.Part): NoDataResponse

    @Multipart
    @POST("registration/professional")
    suspend fun registerAsProfessional(@PartMap params: MutableMap<String, RequestBody>,
                                       @Part file: MultipartBody.Part,
                                       @Part cert: MultipartBody.Part): NoDataResponse

    @Multipart
    @POST("registration/supporter")
    suspend fun registerAsSupporter(@PartMap params: MutableMap<String, RequestBody>,
                                       @Part file: MultipartBody.Part): NoDataResponse

    @Multipart
    @POST("registration/supporter")
    suspend fun registerAsSupporter(@PartMap params: MutableMap<String, RequestBody>,
                                       @Part file: MultipartBody.Part,
                                       @Part cert: MultipartBody.Part): NoDataResponse

    //Auth
    @POST("login")
    suspend fun login(@Body params: LoginParams): UserResponse

    @POST("logout")
    @FormUrlEncoded
    suspend fun logout(@Field("id") id: Int): NoDataResponse

    //Dashboard
    @POST("discover/all")
    @FormUrlEncoded
    suspend fun discoverPerson(@Field("query") query: String): DiscoverPersonResponse

    @POST("discover/role")
    @FormUrlEncoded
    suspend fun discoverPersonByRole(@Field("query") query: String,
                                     @Field("role") role: String): DiscoverByRoleResponse

    @POST("discover/subrole")
    @FormUrlEncoded
    suspend fun discoverPersonBySubrole(@Field("query") query: String,
                                     @Field("role") role: String,
                                     @Field("subrole") subrole: String): DiscoverByRoleResponse

    //Chat
    @POST("chat/lastseen/{id_user}")
    suspend fun getLastSeen(@Path("id_user") idUser: String): LastSeenResponse

    //Crash error reporting
    @POST("error_reporting/android/send")
    suspend fun reportError(@Body params: ErrorReportParams): NoDataResponse

    //Check version
    @POST("version/android")
    @FormUrlEncoded
    suspend fun checkVersion(@Field("current_version_code") currentVersionCode: Int): CheckVersionResponse
}