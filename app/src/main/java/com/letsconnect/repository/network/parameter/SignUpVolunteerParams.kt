package com.letsconnect.repository.network.parameter

import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody

class SignUpVolunteerParams(private val organization: String?,
                            private val fullName: String?,
                            private val username: String?,
                            private val gender: String?,
                            private val locationCode: String?,
                            private val email: String?,
                            private val password: String?,
                            private val aboutMe: String?) {

    fun asMultipart(): MutableMap<String, RequestBody> = mutableMapOf<String, RequestBody>().apply {
        organization?.let { this["organization"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        fullName?.let { this["full_name"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        username?.let { this["username"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        gender?.let { this["gender"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        locationCode?.let { this["location_code"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        email?.let { this["email"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        password?.let { this["password"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
        aboutMe?.let { this["about_me"] = it.toRequestBody("text/plain".toMediaTypeOrNull()) }
    }
}