package com.letsconnect.repository.network.response

import com.google.gson.annotations.SerializedName
import com.letsconnect.ancestors.BaseResponse
import com.letsconnect.model.Role
import java.io.Serializable

data class DiscoverPersonResponse(
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val data: List<Data>
): BaseResponse, Serializable {

    override fun status(): Boolean = status
    override fun message(): String = message

    data class Data(
        @SerializedName("message")
        val message: String,
        @SerializedName("role")
        val role: Int,
        @SerializedName("persons")
        val persons: List<Person>
    ): Serializable {

        data class Person(
            @SerializedName("detail")
            val detail: Detail,
            @SerializedName("specializations")
            val specializations: List<Specialization>,
            @SerializedName("reviews")
            val reviews: List<Review>
        ): Serializable {

            data class Detail(
                @SerializedName("about_me")
                val aboutMe: String?,
                @SerializedName("birthday")
                val birthday: String?,
                @SerializedName("certificate")
                val certificate: String?,
                @SerializedName("communication_id")
                val communicationId: String?,
                @SerializedName("email")
                val email: String?,
                @SerializedName("fullname")
                val fullname: String?,
                @SerializedName("gender")
                val gender: String?,
                @SerializedName("id")
                val id: Int,
                @SerializedName("image")
                val image: String?,
                @SerializedName("licensing")
                val licensing: String?,
                @SerializedName("linkvideo")
                val linkvideo: String?,
                @SerializedName("location_code")
                val locationCode: String,
                @SerializedName("location_name")
                val locationName: String,
                @SerializedName("our_sessions")
                val ourSessions: String?,
                @SerializedName("trial_per")
                val trialPer: String?,
                @SerializedName("rate_per")
                val ratePer: String?,
                @SerializedName("role_id")
                val roleId: String,
                @SerializedName("role_name")
                val roleName: String?,
                @SerializedName("speak_fluent")
                val speakFluent: String?,
                @SerializedName("type_namerole")
                val typeNamerole: String,
                @SerializedName("username")
                val username: String?,
                @SerializedName("work_with")
                val workWith: String?,
                @SerializedName("years_exp")
                val yearsExp: String?,
                @SerializedName("status_available_color")
                val statusAvailableColor: String = "#00000000",
                @SerializedName("rating")
                val rating: Double?,
                @SerializedName("subrole_name")
                val subroleName: String,
                @SerializedName("organization")
                val organization: String
            ): Serializable
            data class Specialization(
                @SerializedName("id")
                val id: String,
                @SerializedName("id_specialize")
                val idSpecialize: String,
                @SerializedName("specialize_name")
                val specializeName: String?
            ): Serializable
            data class Review(
                @SerializedName("comment")
                val comment: String,
                @SerializedName("date")
                val date: String,
                @SerializedName("id_review")
                val idReview: String,
                @SerializedName("rating")
                val rating: String
            ): Serializable

            fun isProfessional(): Boolean = detail.roleId == Role.ROLE_PROFESSIONAL
            fun isSupporter(): Boolean = detail.roleId == Role.ROLE_SUPPORTER
            fun isVolunteer(): Boolean = detail.roleId == Role.ROLE_VOLUNTEER
            fun isClient(): Boolean = detail.roleId == Role.ROLE_CLIENT
        }

    }

}