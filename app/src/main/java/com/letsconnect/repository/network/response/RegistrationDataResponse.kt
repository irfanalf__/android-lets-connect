package com.letsconnect.repository.network.response


import com.google.gson.annotations.SerializedName
import com.letsconnect.ancestors.BaseResponse
import java.io.Serializable

data class RegistrationDataResponse(@SerializedName("status")
                                    val status: Boolean,
                                    @SerializedName("message")
                                    val message: String,
                                    @SerializedName("location")
                                    val location: ArrayList<Location>,
                                    @SerializedName("primary_location")
                                    val primaryLocation: ArrayList<Location> = arrayListOf(),
                                    @SerializedName("specialize")
                                    val specialize: ArrayList<Specialize>): BaseResponse, Serializable {

    override fun status(): Boolean = status
    override fun message(): String = message

    data class Location(
        @SerializedName("location_code")
        val locationCode: String,
        @SerializedName("location_name")
        val locationName: String
    ): Serializable {
        companion object {
            val DIVIDER = Location("DIVIDER", "- - - - - - - - - - - - - - - - - - - - - - - - - ")
        }

        override fun toString(): String = locationName
    }

    data class Specialize(
        @SerializedName("id")
        val id: String,
        @SerializedName("specialize_name")
        val specializeName: String
    ): Serializable {
        var isSelected = false
        override fun toString(): String = specializeName
    }
}