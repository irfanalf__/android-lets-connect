package com.letsconnect.repository.network.parameter

import com.google.gson.annotations.SerializedName

data class SignUpClientParams(@SerializedName("username") val username: String?,
                              @SerializedName("email") val email: String?,
                              @SerializedName("password") val password: String?)