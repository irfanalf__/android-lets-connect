package com.letsconnect.repository.network.response

import com.google.gson.annotations.SerializedName
import com.letsconnect.ancestors.BaseResponse
import java.io.Serializable

data class UserResponse(@SerializedName("status")
                        val status: Boolean,
                        @SerializedName("message")
                        val message: String,
                        @SerializedName("user")
                        val user: User,
                        @SerializedName("specializations")
                        val specializations: List<Specialization>,
                        @SerializedName("history_chat")
                        val historyChat: List<HistoryChat>) : BaseResponse, Serializable{

    override fun status(): Boolean = status
    override fun message(): String = message

    data class Specialization(
        @SerializedName("id")
        val id: String,
        @SerializedName("id_specialize")
        val idSpecialize: String,
        @SerializedName("specialize_name")
        val specializeName: String?
    ): Serializable
    data class User(
        @SerializedName("about_me")
        val aboutMe: String?,
        @SerializedName("birthday")
        val birthday: String?,
        @SerializedName("certificate")
        val certificate: String?,
        @SerializedName("communication_id")
        val communicationId: String?,
        @SerializedName("email")
        val email: String?,
        @SerializedName("fullname")
        val fullname: String?,
        @SerializedName("gender")
        val gender: String?,
        @SerializedName("id")
        val id: Int,
        @SerializedName("image")
        val image: String?,
        @SerializedName("licensing")
        val licensing: String?,
        @SerializedName("linkvideo")
        val linkvideo: String?,
        @SerializedName("location_code")
        val locationCode: String,
        @SerializedName("location_name")
        val locationName: String,
        @SerializedName("our_sessions")
        val ourSessions: String?,
        @SerializedName("trial_per")
        val trialPer: String?,
        @SerializedName("rate_per")
        val ratePer: String?,
        @SerializedName("role_id")
        val roleId: String,
        @SerializedName("role_name")
        val roleName: String?,
        @SerializedName("speak_fluent")
        val speakFluent: String?,
        @SerializedName("type_namerole")
        val typeNamerole: String,
        @SerializedName("username")
        val username: String?,
        @SerializedName("work_with")
        val workWith: String?,
        @SerializedName("years_exp")
        val yearsExp: String?
    ): Serializable
    data class HistoryChat(
        @SerializedName("id")
        val id: Int,
        @SerializedName("fullname")
        val fullname: String,
        @SerializedName("email")
        val email: String,
        @SerializedName("image")
        val image: String,
        @SerializedName("username")
        val username: String,
        @SerializedName("conversations")
        val conversations: List<Conversation>,
        @SerializedName("last_conversation")
        val lastConversation: Conversation
    ): Serializable {

        data class Conversation(
            @SerializedName("id_conversation")
            val idConversation: String,
            @SerializedName("chat_sender")
            val chatSender: Int,
            @SerializedName("conversation")
            val conversation: String,
            @SerializedName("send_at")
            val sendAt: Double,
            @SerializedName("received_by_server_at")
            val receivedByServerAt: Double,
            @SerializedName("sent_at")
            val sentAt: Double,
            @SerializedName("read_at")
            val readAt: Double,
            @SerializedName("deleted_at")
            val deletedAt: Double,
            @SerializedName("status")
            val status: Int,
            @SerializedName("reply_to")
            val replyTo: String?
        ): Serializable
    }
}