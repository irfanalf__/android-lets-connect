package com.letsconnect.repository.network.response

import com.google.gson.annotations.SerializedName
import com.letsconnect.ancestors.BaseResponse
import java.io.Serializable

data class CheckVersionResponse(
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val data: List<Data>,
    @SerializedName("update_required")
    val updateRequired: Boolean
): BaseResponse, Serializable {

    override fun status(): Boolean = status
    override fun message(): String = message

    data class Data(
        @SerializedName("version_code")
        val versionCode: String,
        @SerializedName("version_name")
        val versionName: String
    ): Serializable

}