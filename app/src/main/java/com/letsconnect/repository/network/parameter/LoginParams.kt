package com.letsconnect.repository.network.parameter

import com.google.gson.annotations.SerializedName

data class LoginParams(@SerializedName("email") val email: String?,
                       @SerializedName("password") val password: String?,
                       @SerializedName("firebase_token") val firebaseToken: String?)