package com.letsconnect.repository.network.response

class ErrorResponseException(message: String?) : Exception(message)