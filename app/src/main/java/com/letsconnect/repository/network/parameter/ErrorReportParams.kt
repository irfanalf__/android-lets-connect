package com.letsconnect.repository.network.parameter

import com.google.gson.annotations.SerializedName

data class ErrorReportParams(
    @SerializedName("model")
    val model: String,
    @SerializedName("device_id")
    val deviceId: String,
    @SerializedName("manufacturer")
    val manufacturer: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("device_user")
    val user: String,
    @SerializedName("base")
    val base: Int,
    @SerializedName("incremental")
    val incremental: String,
    @SerializedName("sdk")
    val sdk: Int,
    @SerializedName("board")
    val board: String,
    @SerializedName("brand")
    val brand: String,
    @SerializedName("host")
    val host: String,
    @SerializedName("fingerprint")
    val fingerprint: String,
    @SerializedName("release")
    val release: String,
    @SerializedName("version_code")
    val versionCode: Int,
    @SerializedName("version_name")
    val version_name: String,
    @SerializedName("build_type")
    val buildType: String,
    @SerializedName("from_activity")
    val fromActivity: String,
    @SerializedName("error_message")
    val errorMessage: String,
    @SerializedName("id_user")
    val id_user: Int,
    @SerializedName("unix_error_happen")
    val unix: Long)