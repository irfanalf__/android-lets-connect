package com.letsconnect.preference

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.util.Log
import com.google.gson.Gson
import com.letsconnect.repository.network.response.UserResponse


class AppPreference(private val context: Context) {
    companion object {
        const val PREFERENCE = "PREFERENCE"
        const val USER = "USER"
    }

    private val gson = Gson()
    private val preference by lazy { context.getSharedPreferences(PREFERENCE, MODE_PRIVATE) }

    fun saveUser(user: UserResponse){
        preference.edit().putString(USER, gson.toJson(user)).apply()
    }
    fun getUser(): UserResponse?{
        if(!preference.contains(USER)){
            return null
        }

        return gson.fromJson(preference.getString(USER, ""), UserResponse::class.java)
    }
    fun removeUser(){
        preference.edit().remove(USER).apply()
    }
}