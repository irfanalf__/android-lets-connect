package com.letsconnect

import android.app.Activity
import android.app.Application
import android.os.Bundle
import android.util.Log
import androidx.core.app.NotificationManagerCompat
import com.letsconnect.injection.component.DaggerInjectConsumer
import com.letsconnect.injection.component.InjectConsumer
import com.letsconnect.injection.module.ContextModule
import com.letsconnect.repository.socket.SocketRepository
import javax.inject.Inject

class MainApplication : Application(), Application.ActivityLifecycleCallbacks {
    private var activityReferences = 0
    private val notificationManager by lazy { NotificationManagerCompat.from(this) }

    lateinit var consumer: InjectConsumer

    @Inject
    lateinit var socketRepository: SocketRepository

    override fun onCreate() {
        super.onCreate()
        registerActivityLifecycleCallbacks(this)

        consumer = DaggerInjectConsumer.builder().contextModule(ContextModule(this))
            .build().apply { inject(this@MainApplication) }
    }

    override fun onActivityPaused(activity: Activity) {

    }

    override fun onActivityStarted(activity: Activity) {
        if (++activityReferences == 1) {
            socketRepository.gotoForeground()
            notificationManager.cancelAll()
        }
    }

    override fun onActivityDestroyed(activity: Activity) {

    }

    override fun onActivitySaveInstanceState(activity: Activity, p1: Bundle) {

    }

    override fun onActivityStopped(activity: Activity) {
        if (--activityReferences == 0) {
            socketRepository.gotoBackground()
        }
    }

    override fun onActivityCreated(activity: Activity, p1: Bundle?) {

    }

    override fun onActivityResumed(activity: Activity) {

    }
}