package com.letsconnect.ui.chat

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jakewharton.rxbinding4.widget.textChanges
import com.letsconnect.BuildConfig
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.glideCallback
import com.letsconnect.extension.instantiateViewModel
import com.letsconnect.model.Role
import com.letsconnect.repository.local.room.entities.Interlocutor
import com.letsconnect.ui.chat.adapter.ChatAdapter
import com.letsconnect.ui.chat.touch_helper.MessageSwipeHelper
import com.letsconnect.ui.dashboard.client.DashboardClientActivity
import com.letsconnect.ui.dashboard.professional.DashboardProfessionalActivity
import com.letsconnect.ui.dashboard.volunteer.DashboardVolunteerActivity
import com.letsconnect.ui.notification_click.NotificationClickActivity
import com.letsconnect.ui.splash_screen.SplashScreenActivity
import com.mcnmr.utilities.extension.*
import com.mcnmr.utilities.internal_plugin.SerializableIntent
import kotlinx.android.synthetic.main.activity_chat.*
import org.jetbrains.anko.startActivity

class ChatActivity: BaseActivity() {
    companion object{
        const val INTERLOCUTOR_ARGUMENT = "interlocutor_argument"
    }

    private val vm by lazy { instantiateViewModel<ChatVM>(person.id) }

    @SerializableIntent(INTERLOCUTOR_ARGUMENT)
    lateinit var person: Interlocutor

    private var replyId: String? = null

    override fun shouldShowLoading(tag: Any) {}

    override fun shouldHideLoading(tag: Any) {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        setSupportActionBar(toolbar)

        Glide.with(this).load(person.image)
            .listener(glideCallback(onFinish = {
                shimmerCivProfile.stopShimmer()
                shimmerCivProfile.gone()
            }))
            .error(R.drawable.default_image_profile)
            .into(civProfile)
        tvName.text = person.fullname

        rvChat.layoutManager = LinearLayoutManager(this)
        rvChat.adapter = ChatAdapter(this, mutableListOf())
        ItemTouchHelper(MessageSwipeHelper(this){
            (rvChat.adapter as ChatAdapter).getMessage(it).apply {
                replyId = this.conversationId
                tvReply.text = this.conversation?.hexDecrypt(
                    method = BuildConfig.ENCRYPTION_METHOD,
                    algorithm = BuildConfig.ENCRYPTION_ALGORITHM,
                    ivSeparator = BuildConfig.ENCRYPTION_IV_DELIMITER,
                    key = BuildConfig.ENCRYPTION_KEY
                )
                cvReply.visible()
            }
        }).attachToRecyclerView(rvChat)

        vm.typingData.observe(this, Observer {
            if(it) { tvStatusOnline.text = getString(R.string.chat_typing) }
            else {
                tvStatusOnline.visible()
                tvStatusOnline.text = vm.onlineStatusData.value
            }
        })
        vm.onlineStatusData.observe(this, Observer {
            tvStatusOnline.visible()
            tvStatusOnline.text = vm.onlineStatusData.value
        })
        vm.conversationRows.observe(this, Observer {
            (rvChat.adapter as ChatAdapter).newMessage(it)
            rvChat.scrollToPosition((rvChat.adapter?.itemCount ?: 2) -2)
        })
        vm.scrollToBottomData.observe(this, Observer {
            if(it){
                cvScrollToBottom.circularReveal()
            }else {
                cvScrollToBottom.circularHide()
            }
        })

        etChatInput.textChanges().subscribe {
            if(it.toString().trim().isNotEmpty()){
                vm.executeTyping()
            }
        }

        rvChat.addOnScrollListener(object: RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                vm.calculateScroll(recyclerView, dy)
            }
        })

        cvSend.setOnClickListener {
            if(etChatInput.text.toString().trim().isNotEmpty()){
                vm.executeSendMessage(person, etChatInput.text.toString().trim(), replyId)
                etChatInput.setText("")

                replyId = null
                tvReply.text = ""
                cvReply.gone()
            }
        }
        cvScrollToBottom.setOnClickListener {
            vm.scrollToBottomData.value = false
            rvChat.scrollToPosition((rvChat.adapter?.itemCount ?: 2) -2)
        }
        ivCloseReply.setOnClickListener {
            replyId = null
            tvReply.text = ""
            cvReply.gone()
        }
        llBack.setOnClickListener { onBackPressed() }

        vm.observeTyping(person.id.toString())
        vm.observeOnline(person.id.toString())
    }

    override fun onBackPressed() = when(previousActivity()){
        NotificationClickActivity::class.java.name -> {
            when(preference.getUser()?.user?.roleId){
                Role.ROLE_CLIENT -> startActivity<DashboardClientActivity>()
                Role.ROLE_PROFESSIONAL -> startActivity<DashboardProfessionalActivity>()
                Role.ROLE_SUPPORTER -> startActivity<DashboardProfessionalActivity>()
                Role.ROLE_VOLUNTEER -> startActivity<DashboardVolunteerActivity>()
                else -> startActivity<SplashScreenActivity>()
            }
            finishAffinity()
        }
        else -> super.onBackPressed()
    }

    override fun onResume() {
        vm.joinPrivateChatRoom()
        shimmerCivProfile.startShimmer()
        super.onResume()
    }

    override fun onPause() {
        vm.exitPrivateChatRoom()
        shimmerCivProfile.stopShimmer()
        super.onPause()
    }
}