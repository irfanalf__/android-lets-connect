package com.letsconnect.ui.chat.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.letsconnect.BuildConfig
import com.letsconnect.R
import com.mcnmr.utilities.extension.hexDecrypt
import com.letsconnect.repository.local.room.custom_row.CompleteConversation
import com.mcnmr.utilities.extension.toDate
import com.letsconnect.repository.local.room.entities.Conversation
import com.mcnmr.utilities.extension.visible

class ChatAdapter(private val context: Context,
                  private val conversations: MutableList<CompleteConversation>): RecyclerView.Adapter<ChatAdapter.ChatVH>() {

    override fun getItemViewType(position: Int): Int = position

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ChatVH {
        return if(position == 0){
            ChatVH(LayoutInflater.from(context).inflate(R.layout.row_chat_encryption, parent, false))
        }else if(position == 1){
            ChatVH(LayoutInflater.from(context).inflate(R.layout.row_chat_suicide_description, parent, false))
        }else {
            if(conversations[position - 2].whoIsSender == Conversation.ME){
                ChatVH(LayoutInflater.from(context).inflate(R.layout.row_chat_me, parent, false))
            }else {
                ChatVH(LayoutInflater.from(context).inflate(R.layout.row_chat_them, parent, false))
            }
        }
    }

    override fun getItemCount(): Int = if(conversations.size > 0) conversations.size + 2 else 2

    override fun onBindViewHolder(holder: ChatVH, position: Int) {
        if(position == 0){
            holder.tvMessage?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lock_12, 0, 0, 0)
        }else if(position == 1){
            //do nothing
        }else {
            holder.swipeable = conversations[position - 2].conversationStatus ?: -1 >= Conversation.STATUS_RECEIVED_BY_SERVER

            holder.tvMessage?.text = conversations[position - 2].conversation?.hexDecrypt(
                method = BuildConfig.ENCRYPTION_METHOD,
                algorithm = BuildConfig.ENCRYPTION_ALGORITHM,
                ivSeparator = BuildConfig.ENCRYPTION_IV_DELIMITER,
                key = BuildConfig.ENCRYPTION_KEY
            )
            holder.tvDate?.text = conversations[position - 2].sendAt?.toLong()
                .toDate("dd MMM yyyy, HH:mm")

            conversations[position - 2].replyMessage?.let {
                holder.tvReplyMessage?.text = it.hexDecrypt(
                    method = BuildConfig.ENCRYPTION_METHOD,
                    algorithm = BuildConfig.ENCRYPTION_ALGORITHM,
                    ivSeparator = BuildConfig.ENCRYPTION_IV_DELIMITER,
                    key = BuildConfig.ENCRYPTION_KEY
                )
                holder.llReply?.visible()
            }

            when(conversations[position - 2].conversationStatus){
                Conversation.STATUS_SEND -> holder.ivChatStatus?.setImageResource(R.drawable.ic_chat_pending)
                Conversation.STATUS_RECEIVED_BY_SERVER -> holder.ivChatStatus
                    ?.setImageResource(R.drawable.ic_chat_received_by_server)
                Conversation.STATUS_SENT -> holder.ivChatStatus?.setImageResource(R.drawable.ic_chat_sent)
                Conversation.STATUS_READ -> holder.ivChatStatus?.setImageResource(R.drawable.ic_chat_read)
            }
        }
    }

    fun newMessage(conversation: List<CompleteConversation>){
        conversations.clear()
        conversations.addAll(conversation)
        notifyDataSetChanged()
    }

    fun getMessage(position: Int) = conversations[position - 2]

    inner class ChatVH(v: View): RecyclerView.ViewHolder(v){
        val tvMessage: TextView? = v.findViewById(R.id.tvMessage)
        val tvDate: TextView? = v.findViewById(R.id.tvDate)
        val ivChatStatus: AppCompatImageView? = v.findViewById(R.id.ivChatStatus)
        val llReply: LinearLayout? = v.findViewById(R.id.llReply)
        val tvReplyMessage: TextView? = v.findViewById(R.id.tvReplyMessage)

        var swipeable = false
    }
}