package com.letsconnect.ui.chat

import android.os.CountDownTimer
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.letsconnect.BuildConfig
import com.letsconnect.MainApplication
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.ancestors.BaseViewModel
import com.mcnmr.utilities.extension.hexEncrypt
import com.mcnmr.utilities.extension.interval
import com.mcnmr.utilities.extension.toDate
import com.letsconnect.repository.local.LocalRepository
import com.letsconnect.repository.local.room.custom_row.CompleteConversation
import com.letsconnect.repository.network.request.RemoteRepository
import com.letsconnect.repository.socket.SocketRepository
import com.letsconnect.repository.socket.parameter.SendMessageParams
import com.letsconnect.repository.socket.parameter.TypingParams
import com.letsconnect.repository.local.room.entities.Conversation
import com.letsconnect.repository.local.room.entities.Interlocutor
import com.mcnmr.utilities.extension.doIf
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class ChatVM(activity: BaseActivity, private val interlocutorId: Int): BaseViewModel(activity) {
    companion object {
        const val GET_LAST_SEEN = "GET_REGISTRATION_DATA"

        const val SCROLL_ITEM_THRESHOLD = 3
    }

    @Inject
    lateinit var remoteRepository: RemoteRepository

    @Inject
    lateinit var socketRepository: SocketRepository

    @Inject
    lateinit var localRepository: LocalRepository

    val typingData = MutableLiveData<Boolean>()
    val onlineStatusData = MutableLiveData<String>()
    val scrollToBottomData = MutableLiveData(false)

    val conversationRows: LiveData<List<CompleteConversation>>

    private var typingCountdown: CountDownTimer? = null

    init {
        (activity.application as MainApplication).consumer.inject(this)

        conversationRows = localRepository.watchConversation(interlocutorId)
    }

    fun joinPrivateChatRoom(){
        viewModelScope.launch {
            localRepository.iReadMessageOfThisInterlocutor(interlocutorId)
            socketRepository.joinPrivateChatRoom(interlocutorId)
        }
    }
    fun exitPrivateChatRoom(){ socketRepository.exitPrivateChatRoom() }

    fun observeTyping(idUser: String){
        socketRepository.onSomeoneTyping.observe(activity, Observer {
            it[idUser].doIf(
                ifNull = {
                    typingData.postValue(false)
                },
                ifNotNull = { typingTime ->
                    if(System.currentTimeMillis().minus(typingTime) < 10000){
                        typingData.postValue(true)

                        typingCountdown?.cancel()
                        typingCountdown = interval(activity, future = 10000, onFinish = {
                            typingData.postValue(false)
                        })
                    }else {
                        typingData.postValue(false)
                    }
                }
            )
        })
    }
    fun observeOnline(idUser: String){
        if(!socketRepository.onSomeoneOnline.value.contains(idUser) &&
            !socketRepository.onSomeoneOffline.value.containsKey(idUser)){
            viewModelScope.launch {
                activity.shouldShowLoading(GET_LAST_SEEN)
                val result = safeApiCall(GET_LAST_SEEN) { remoteRepository.getLastSeen(idUser) }
                activity.shouldHideLoading(GET_LAST_SEEN)
                result.consume(
                    onErrorResponse = activity::onErrorResponse,
                    onHttpError = activity::onHttpError,
                    onNetworkError = activity::onNetworkError,
                    onTimeoutError = activity::onTimeoutError,
                    onUnknownError = activity::onUnknownError,
                    onSuccess = {
                        if(it.statusOnline == 1){
                            onlineStatusData.value = "Online"
                        }else {
                            onlineStatusData.value = if(it.lastOnline.toInt() == 0) "Never online"
                            else "Last seen ${it.lastOnline.toLong().toDate("dd MMM yyyy, HH:mm")}"
                        }
                    }
                )
            }
        }

        socketRepository.onSomeoneOnline.observe(activity, Observer {
            if(it.contains(idUser)) { onlineStatusData.value = "Online" }
        })

        socketRepository.onSomeoneOffline.observe(activity, Observer {
            it[idUser]?.let { lastSeen ->
                onlineStatusData.value = "Last seen ${lastSeen.toLong().toDate("dd MMM yyyy, HH:mm")}"
            }
        })
    }

    fun executeTyping(){ socketRepository.typing(TypingParams(interlocutorId)) }

    fun executeSendMessage(interlocutor: Interlocutor, message: String, replyTo: String?){
        viewModelScope.launch {
            val id = UUID.randomUUID().toString()
            val nowUtc = System.currentTimeMillis().toDouble()

            val conversation = Conversation()
            conversation.conversationId = id
            conversation.interlocutorId = interlocutorId
            conversation.conversation = message
            conversation.replyTo = replyTo
            conversation.conversationStatus = Conversation.STATUS_SEND
            conversation.whoIsSender = Conversation.ME
            conversation.sendAt = nowUtc
            conversation.receivedByServerAt = null
            conversation.sentAt = null
            conversation.readAt = null

            localRepository.sendMessage(interlocutor, conversation)
            socketRepository.sendMessage(
                params = SendMessageParams(
                    conversationId = id,
                    targetId = interlocutor.id,
                    conversation = message.hexEncrypt(
                        method = BuildConfig.ENCRYPTION_METHOD,
                        key = BuildConfig.ENCRYPTION_KEY,
                        ivSeparator = BuildConfig.ENCRYPTION_IV_DELIMITER,
                        ivLength = BuildConfig.ENCRYPTION_IV_LENGTH,
                        algorithm = BuildConfig.ENCRYPTION_ALGORITHM
                    ),
                    replyTo = replyTo,
                    sendAt = nowUtc
                ),
                callback = { viewModelScope.launch { localRepository.serverReceivingMessage(it) }}
            )
        }
    }

    fun calculateScroll(recyclerView: RecyclerView, dy: Int){
        val lastVisiblePosition = (recyclerView.layoutManager as LinearLayoutManager)
            .findLastVisibleItemPosition()
        val itemCount = recyclerView.adapter?.itemCount ?: 0
        if(dy > 0){
            //scroll to bottom
            if(itemCount - lastVisiblePosition < SCROLL_ITEM_THRESHOLD){
                if(scrollToBottomData.value == true){
                    scrollToBottomData.value = false
                }
            }
        }else if(dy < 0){
            //scroll to top
            if(itemCount - lastVisiblePosition >= SCROLL_ITEM_THRESHOLD){
                if(scrollToBottomData.value == false){
                    scrollToBottomData.value = true
                }
            }
        }
    }
}