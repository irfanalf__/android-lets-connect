package com.letsconnect.ui.discover_subrole

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding4.widget.textChanges
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.instantiateViewModel
import com.letsconnect.model.Role
import com.letsconnect.ui.chat.ChatActivity
import com.letsconnect.ui.detail_person.DetailPersonActivity
import com.letsconnect.ui.discover_role.DiscoverRoleVM
import com.letsconnect.ui.discover_subrole.adapter.DiscoverSubrolePersonAdapter
import com.letsconnect.ui.login.LoginActivity
import com.mcnmr.utilities.extension.doIf
import com.mcnmr.utilities.extension.visibleOrGoneIf
import com.mcnmr.utilities.internal_plugin.IntIntent
import com.mcnmr.utilities.internal_plugin.StringIntent
import kotlinx.android.synthetic.main.activity_discover_subrole.*
import org.jetbrains.anko.startActivity

class DiscoverSubroleActivity: BaseActivity() {

    companion object{
        const val ROLE_ARGUMENT = "ROLE_ARGUMENT"
        const val SUBROLE_ARGUMENT = "SUBROLE_ARGUMENT"
    }

    private val vm by lazy { instantiateViewModel<DiscoverSubroleVM>() }

    @StringIntent(value = ROLE_ARGUMENT)
    lateinit var role: String

    @StringIntent(value = SUBROLE_ARGUMENT)
    lateinit var subrole: String

    override fun shouldShowLoading(tag: Any) { vm.showLoadingDiscoverPersonEvent.value = true }
    override fun shouldHideLoading(tag: Any) { vm.showLoadingDiscoverPersonEvent.value = false }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_discover_subrole)

        tvTitle.text = Role.get(role, subrole).toString()

        vm.showLoadingDiscoverPersonEvent.observe(this, Observer {
            if(it){
                shimmerDiscover.startShimmer()
            }else {
                shimmerDiscover.stopShimmer()
            }
            rvDiscoverPerson.visibleOrGoneIf(!it)
            shimmerDiscover.visibleOrGoneIf(it)
        })
        vm.discoverPersonResponse.observe(this, Observer {
            (rvDiscoverPerson.adapter as DiscoverSubrolePersonAdapter).setNewData(it.data)
        })

        rvDiscoverPerson.layoutManager = LinearLayoutManager(this)
        rvDiscoverPerson.adapter = DiscoverSubrolePersonAdapter(this, mutableListOf(),
            onPersonSelected = { person, isAutoPlay ->
                startActivity<DetailPersonActivity>(
                    DetailPersonActivity.PERSON_ARGUMENT to person,
                    DetailPersonActivity.AUTO_PLAY_ARGUMENT to isAutoPlay
                )
            },
            onChatClicked = {
                preference.getUser().doIf(
                    ifNull = {
                        startActivity<LoginActivity>()
                    },
                    ifNotNull = {
                        startActivity<ChatActivity>(ChatActivity.INTERLOCUTOR_ARGUMENT to it)
                    }
                )
            }
        )

        etSearch.textChanges().subscribe{ vm.executeDiscover(role, subrole, it.toString()) }

        llBack.setOnClickListener { onBackPressed() }
    }
}