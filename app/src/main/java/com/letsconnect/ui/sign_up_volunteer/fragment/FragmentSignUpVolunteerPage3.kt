package com.letsconnect.ui.sign_up_volunteer.fragment

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.jakewharton.rxbinding4.widget.checkedChanges
import com.jakewharton.rxbinding4.widget.textChanges
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.instantiateViewModel
import com.letsconnect.ui.sign_up_volunteer.SignUpVolunteerVM
import com.mcnmr.utilities.extension.error
import com.mcnmr.utilities.extension.getCompatDrawable
import com.mcnmr.utilities.extension.isNull
import kotlinx.android.synthetic.main.fragment_sign_up_volunteer_page3.*

class FragmentSignUpVolunteerPage3: Fragment() {
    private val vm by lazy { (requireActivity() as BaseActivity).instantiateViewModel<SignUpVolunteerVM>() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_sign_up_volunteer_page3, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        vm.visiblePasswordData.observe(viewLifecycleOwner, Observer {
            if(it){
                etPassword.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                etPassword.setSelection(etPassword.text.length)
                ivPasswordVisibility.setImageResource(R.drawable.ic_visibility_off)
            }else {
                etPassword.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                etPassword.setSelection(etPassword.text.length)
                ivPasswordVisibility.setImageResource(R.drawable.ic_visibility)
            }
        })
        vm.visibleRepeatPasswordData.observe(viewLifecycleOwner, Observer {
            if(it){
                etRepeatPassword.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                etRepeatPassword.setSelection(etRepeatPassword.text.length)
                ivRepeatPasswordVisibility.setImageResource(R.drawable.ic_visibility_off)
            }else {
                etRepeatPassword.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                etRepeatPassword.setSelection(etRepeatPassword.text.length)
                ivRepeatPasswordVisibility.setImageResource(R.drawable.ic_visibility)
            }
        })
        vm.emailValidation.observe(viewLifecycleOwner, Observer { tvErrorEmail.error(it) })
        vm.passwordValidation.observe(viewLifecycleOwner, Observer { tvErrorPassword.error(it) })
        vm.repeatPasswordValidation.observe(viewLifecycleOwner, Observer { tvErrorRepeatPassword.error(it) })
        vm.step3Validation.observe(viewLifecycleOwner, Observer {
            btnSignUp.isEnabled = it
            btnSignUp.background = getCompatDrawable(if(it) R.drawable.bg_radius_button_black else
                R.drawable.bg_radius_button_grey)
        })

        etEmail.textChanges().subscribe { vm.emailData.value = it.toString() }
        etPassword.textChanges().subscribe { vm.passwordData.value = it.toString() }
        etRepeatPassword.textChanges().subscribe { vm.repeatPasswordData.value = it.toString() }
        cbTermsAgreement.checkedChanges().subscribe { vm.termsOfAgreementData.value = it }
        cbOver13Age.checkedChanges().subscribe { vm.above13YearsData.value = it }

        ivPasswordVisibility.setOnClickListener {
            vm.visiblePasswordData.value = if(vm.visiblePasswordData.value.isNull()) true else
                !vm.visiblePasswordData.value!!
        }
        ivRepeatPasswordVisibility.setOnClickListener {
            vm.visibleRepeatPasswordData.value = if(vm.visibleRepeatPasswordData.value.isNull()) true else
                !vm.visibleRepeatPasswordData.value!!
        }
        btnSignUp.setOnClickListener { vm.executeRegister() }
    }
}