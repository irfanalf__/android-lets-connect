package com.letsconnect.ui.sign_up_volunteer.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.letsconnect.repository.network.response.RegistrationDataResponse
import com.letsconnect.ui.sign_up_volunteer.fragment.FragmentSignUpVolunteerPage1
import com.letsconnect.ui.sign_up_volunteer.fragment.FragmentSignUpVolunteerPage2
import com.letsconnect.ui.sign_up_volunteer.fragment.FragmentSignUpVolunteerPage3

class SignUpVolunteerPagerAdapter(fm: FragmentManager, response: RegistrationDataResponse):
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private val page1 = FragmentSignUpVolunteerPage1()
    private val page2 = FragmentSignUpVolunteerPage2.newInstance(response.location, response.primaryLocation)
    private val page3 = FragmentSignUpVolunteerPage3()

    override fun getItem(position: Int): Fragment = when(position){
        0 -> page1
        1 -> page2
        else -> page3
    }

    override fun getCount(): Int = 3
}