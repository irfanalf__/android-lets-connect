package com.letsconnect.ui.sign_up_volunteer.fragment

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.jakewharton.rxbinding4.widget.itemSelections
import com.jakewharton.rxbinding4.widget.textChanges
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.*
import com.letsconnect.model.Role
import com.letsconnect.ui.sign_up_volunteer.SignUpVolunteerVM
import com.mcnmr.utilities.extension.error
import com.mcnmr.utilities.extension.getCompatColor
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import droidninja.filepicker.utils.ContentUriUtils
import kotlinx.android.synthetic.main.fragment_sign_up_volunteer_page1.*
import java.io.File

class FragmentSignUpVolunteerPage1: Fragment() {
    companion object {
        const val IMAGE_PICKER_REQUEST_CODE = 0
    }

    private val vm by lazy { (requireActivity() as BaseActivity).instantiateViewModel<SignUpVolunteerVM>() }
    private val organizations by lazy { arrayOf(getString(R.string.signup_organization_lets_connect_volunteer)) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_sign_up_volunteer_page1, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        spOrganization.adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, organizations)

        vm.readStoragePermissionResult.observe(viewLifecycleOwner, Observer { granted ->
            if(granted){
                FilePickerBuilder.instance
                    .setMaxCount(1)
                    .pickPhoto(this, IMAGE_PICKER_REQUEST_CODE)
            }
        })

        vm.photoData.observe(viewLifecycleOwner, Observer {
            it?.path?.let{ path -> etPhoto.setText(path) }
        })

        vm.screenNameValidation.observe(viewLifecycleOwner, Observer { tvErrorScreenName.error(it) })
        vm.fullNameValidation.observe(viewLifecycleOwner, Observer { tvErrorFullName.error(it) })
        vm.photoValidation.observe(viewLifecycleOwner, Observer { tvErrorPhoto.error(it) })
        vm.step1Validation.observe(viewLifecycleOwner, Observer {
            cvForward.isEnabled = it
            cvForward.setCardBackgroundColor(getCompatColor(if(it) android.R.color.black else
                android.R.color.darker_gray))
        })

        spOrganization.itemSelections().subscribe { vm.organizationData.value = organizations[it] }
        etScreenName.textChanges().subscribe { vm.screenNameData.value = it.toString() }
        etFullName.textChanges().subscribe { vm.fullNameData.value = it.toString() }
        btnBrowse.setOnClickListener { vm.readStoragePermissionEvent.trigger() }
        cvForward.setOnClickListener { vm.gotoStep2Event.trigger() }
        tvLogin.setOnClickListener { vm.gotoLoginPageEvent.trigger() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            IMAGE_PICKER_REQUEST_CODE -> {
                if(resultCode == RESULT_OK){
                    data?.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_MEDIA)?.let {
                        ContentUriUtils.getFilePath(requireContext(), it[0])?.let { path ->
                            vm.photoData.value = File(path)
                        }
                    }
                }
            }
        }
    }
}