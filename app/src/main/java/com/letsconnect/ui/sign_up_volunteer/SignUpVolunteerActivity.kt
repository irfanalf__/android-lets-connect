package com.letsconnect.ui.sign_up_volunteer

import android.Manifest
import android.os.Build
import android.os.Bundle
import androidx.lifecycle.Observer
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.instantiateViewModel
import com.letsconnect.extension.isPermissionGranted
import com.mcnmr.utilities.extension.openPermissionSettings
import com.letsconnect.repository.network.response.RegistrationDataResponse
import com.letsconnect.ui.login.LoginActivity
import com.letsconnect.ui.sign_up_volunteer.adapter.SignUpVolunteerPagerAdapter
import com.mcnmr.utilities.internal_plugin.SerializableIntent
import kotlinx.android.synthetic.main.activity_sign_up_volunteer.*
import org.jetbrains.anko.startActivity

class SignUpVolunteerActivity : BaseActivity() {
    companion object {
        const val REGISTRATION_RESPONSE = "REGISTRATION_RESPONSE"
        const val LOCATION_DATA = "LOCATION_DATA"
        const val PRIMARY_LOCATION_DATA = "PRIMARY_LOCATION_DATA"

        const val REQ_READ_EXTERNAL_STORAGE = 1
    }

    private val vm by lazy { instantiateViewModel<SignUpVolunteerVM>() }

    @SerializableIntent(REGISTRATION_RESPONSE)
    lateinit var registrationDataResponse: RegistrationDataResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_volunteer)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        viewPager.adapter = SignUpVolunteerPagerAdapter(supportFragmentManager, registrationDataResponse)

        vm.readStoragePermissionEvent.observe(this, Observer {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE), REQ_READ_EXTERNAL_STORAGE)
            }else {
                vm.readStoragePermissionResult.value = true
            }
        })
        vm.registerResponse.observe(this, Observer {
            alertDialog(title = getString(R.string.alert_dialog_message_title),
                message = getString(R.string.signup_success_register),
                positiveTitle = getString(R.string.alert_dialog_close), onDismiss = { vm.gotoLoginPageEvent.trigger() })
        })

        vm.gotoStep1Event.observe(this, Observer { viewPager.currentItem = 0 })
        vm.gotoStep2Event.observe(this, Observer { viewPager.currentItem = 1 })
        vm.gotoStep3Event.observe(this, Observer { viewPager.currentItem = 2 })
        vm.gotoLoginPageEvent.observe(this, Observer { startActivity<LoginActivity>()
            finishAffinity()
        })
    }

    override fun onBackPressed() {
        when(viewPager.currentItem){
            0 -> {
                startActivity<LoginActivity>()
                finishAffinity()
            }
            1 -> vm.gotoStep1Event.trigger()
            2 -> vm.gotoStep2Event.trigger()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(isPermissionGranted(permissions, grantResults)){
            vm.readStoragePermissionResult.value = true
        }else {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                (!shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        !shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE))){
                vm.readStoragePermissionResult.value = false

                alertDialog(title = getString(R.string.permission_denied), message = getString(R.string.permission_desc_read_storage),
                    negativeTitle = getString(R.string.alert_dialog_close), positiveTitle = getString(R.string.permission_goto_setting),
                    positiveAction = { openPermissionSettings() }
                )
            }
        }
    }
}