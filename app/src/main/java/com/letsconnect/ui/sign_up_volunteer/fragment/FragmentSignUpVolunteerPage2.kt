package com.letsconnect.ui.sign_up_volunteer.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.jakewharton.rxbinding4.widget.itemSelections
import com.jakewharton.rxbinding4.widget.textChanges
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.*
import com.letsconnect.repository.network.response.RegistrationDataResponse
import com.letsconnect.ui.sign_up_volunteer.SignUpVolunteerActivity
import com.letsconnect.ui.sign_up_volunteer.SignUpVolunteerVM
import com.mcnmr.utilities.extension.error
import com.mcnmr.utilities.extension.getCompatColor
import kotlinx.android.synthetic.main.fragment_sign_up_volunteer_page2.*

@Suppress("UNCHECKED_CAST")
class FragmentSignUpVolunteerPage2 private constructor(): Fragment() {
    companion object {
        fun newInstance(location: ArrayList<RegistrationDataResponse.Location>,
                        primaryLocation: ArrayList<RegistrationDataResponse.Location>): FragmentSignUpVolunteerPage2 = FragmentSignUpVolunteerPage2().apply {
            arguments = Bundle().apply {
                putSerializable(SignUpVolunteerActivity.LOCATION_DATA, location)
                putSerializable(SignUpVolunteerActivity.PRIMARY_LOCATION_DATA, primaryLocation)
            }
        }
    }

    private val vm by lazy { (requireActivity() as BaseActivity).instantiateViewModel<SignUpVolunteerVM>() }

    private val genders by lazy { arrayOf(getString(R.string.signup_male), getString(R.string.signup_female), getString(R.string.signup_non_binary)) }
    private lateinit var location: ArrayList<RegistrationDataResponse.Location>
    private lateinit var primaryLocation: ArrayList<RegistrationDataResponse.Location>

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.let {
            location = it.getSerializable(SignUpVolunteerActivity.LOCATION_DATA) as ArrayList<RegistrationDataResponse.Location>
            primaryLocation = it.getSerializable(SignUpVolunteerActivity.PRIMARY_LOCATION_DATA) as ArrayList<RegistrationDataResponse.Location>
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_sign_up_volunteer_page2, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val locations = mutableListOf<RegistrationDataResponse.Location>()
        locations.addAll(primaryLocation)
        locations.add(RegistrationDataResponse.Location.DIVIDER)
        locations.addAll(location)

        spGender.adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, genders)
        spLocation.adapter = object : ArrayAdapter<RegistrationDataResponse.Location>(requireContext(),
            android.R.layout.simple_spinner_dropdown_item, locations){
            override fun areAllItemsEnabled(): Boolean = false
            override fun isEnabled(position: Int): Boolean =
                locations[position] != RegistrationDataResponse.Location.DIVIDER
        }

        vm.aboutMeValidation.observe(viewLifecycleOwner, Observer { tvErrorAboutMe.error(it) })
        vm.step2Validation.observe(viewLifecycleOwner, Observer {
            cvForward.isEnabled = it
            cvForward.setCardBackgroundColor(getCompatColor(if(it) android.R.color.black else
                android.R.color.darker_gray))
        })

        spGender.itemSelections().subscribe { vm.genderData.value = genders[it] }
        spLocation.itemSelections().subscribe { vm.locationData.value = location[it] }
        etAboutMe.textChanges().subscribe { vm.aboutMeData.value = it.toString() }

        cvForward.setOnClickListener { vm.gotoStep3Event.trigger() }
    }
}