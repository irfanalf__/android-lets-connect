package com.letsconnect.ui.sign_up_volunteer

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.ancestors.BaseResponse
import com.letsconnect.ancestors.BaseViewModel
import com.letsconnect.repository.network.parameter.SignUpVolunteerParams
import com.letsconnect.repository.network.request.RemoteRepository
import com.letsconnect.repository.network.response.RegistrationDataResponse
import com.mcnmr.utilities.extension.*
import com.mcnmr.utilities.internal_plugin.combineLiveData
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import javax.inject.Inject

class SignUpVolunteerVM(activity: BaseActivity): BaseViewModel(activity) {
    companion object{
        const val POST_REGISTER_AS_VOLUNTEER = "POST_REGISTER_AS_VOLUNTEER"
    }

    @Inject
    lateinit var remoteRepository: RemoteRepository

    val readStoragePermissionEvent = SingleEventWrapper<Any?>()
    val readStoragePermissionResult = SingleEventWrapper<Boolean>()
    val gotoStep1Event = SingleEventWrapper<Any?>()
    val gotoStep2Event = SingleEventWrapper<Any?>()
    val gotoStep3Event = SingleEventWrapper<Any?>()
    val gotoLoginPageEvent = SingleEventWrapper<Any?>()
    val registerResponse = SingleEventWrapper<BaseResponse>()

    val organizationData = MutableLiveData<String>()
    val photoData = MutableLiveData<File?>(null)
    val screenNameData = MutableLiveData<String>()
    val screenNameValidation = combineLiveData(screenNameData){
        if(!it.isNotEmptyOrNotNull() || (it ?: " ").split(" ").size != 1){
            activity.getString(R.string.error_username_empty)
        }else {
            ""
        }
    }
    val fullNameData = MutableLiveData<String>()
    val fullNameValidation = combineLiveData(fullNameData){
        if(it.isNull()){
            activity.getString(R.string.error_fullname_empty)
        }else {
            ""
        }
    }
    val photoValidation = combineLiveData(photoData){
        if(it.isNull()){
            activity.getString(R.string.error_photo_profile_empty)
        }else {
            ""
        }
    }

    val genderData = MutableLiveData<String>()
    val locationData = MutableLiveData<RegistrationDataResponse.Location>()
    val aboutMeData = MutableLiveData<String>()
    val aboutMeValidation = combineLiveData(aboutMeData){
        if(!it.isNotEmptyOrNotNull()){
            activity.getString(R.string.error_about_me_empty)
        }else {
            ""
        }
    }

    val emailData = MutableLiveData<String>()
    val passwordData = MutableLiveData<String>()
    val repeatPasswordData = MutableLiveData<String>()
    val visiblePasswordData = MutableLiveData<Boolean>()
    val visibleRepeatPasswordData = MutableLiveData<Boolean>()
    val termsOfAgreementData = MutableLiveData<Boolean>()
    val above13YearsData = MutableLiveData<Boolean>()
    val emailValidation = combineLiveData(emailData){ a ->
        if(!a.isNotEmptyOrNotNull()){
            activity.getString(R.string.error_email_empty)
        }else if(!a.isEmailAddress()){
            activity.getString(R.string.error_invalid_email)
        }else {
            ""
        }
    }
    val passwordValidation = combineLiveData(passwordData){ a ->
        if(!a.isNotEmptyOrNotNull()){
            activity.getString(R.string.error_password_empty)
        }else if(!a.isAlphanumeric() || (a?.length ?: 0) < 8 ){
            activity.getString(R.string.error_password_pattern_not_valid)
        }else {
            ""
        }
    }
    val repeatPasswordValidation = combineLiveData(passwordData, repeatPasswordData){ a, b ->
        if(!a.equals(b)){
            activity.getString(R.string.error_invalid_repeat_password)
        }else {
            ""
        }
    }

    val step1Validation = combineLiveData(organizationData, photoValidation, screenNameValidation,
        fullNameValidation)
    { a, b, c, d -> a.isNotNull() && b.isNullOrEmpty() && c.isNullOrEmpty() && d.isNullOrEmpty()}
    val step2Validation = combineLiveData(genderData, locationData, aboutMeValidation)
    { a, b, c -> a.isNotNull() && b.isNotNull() && c.isNullOrEmpty()}
    val step3Validation = combineLiveData(emailValidation, passwordValidation, repeatPasswordValidation, termsOfAgreementData, above13YearsData)
    { a, b, c, d, e -> a.isNullOrEmpty() && b.isNullOrEmpty() && c.isNullOrEmpty()
            && d.notNullAndTrue() && e.notNullAndTrue()}

    init { app.consumer.inject(this) }

    fun executeRegister(){
        val params = SignUpVolunteerParams(organizationData.value, fullNameData.value, screenNameData.value,
            genderData.value, locationData.value?.locationCode,
            emailData.value, passwordData.value, aboutMeData.value).asMultipart()
        val file = MultipartBody.Part.createFormData("profile_image",
            "${System.currentTimeMillis()}.png",
            photoData.value!!.asRequestBody("image/png".toMediaTypeOrNull()))

        viewModelScope.launch {
            activity.shouldShowLoading(POST_REGISTER_AS_VOLUNTEER)
            val result = safeApiCall(POST_REGISTER_AS_VOLUNTEER){ remoteRepository.registerAsVolunteer(params, file) }
            activity.shouldHideLoading(POST_REGISTER_AS_VOLUNTEER)
            result.consume(
                onErrorResponse = activity::onErrorResponse,
                onHttpError = activity::onHttpError,
                onNetworkError = activity::onNetworkError,
                onTimeoutError = activity::onTimeoutError,
                onUnknownError = activity::onUnknownError,
                onSuccess = { response -> registerResponse.value = response }
            )
        }
    }
}