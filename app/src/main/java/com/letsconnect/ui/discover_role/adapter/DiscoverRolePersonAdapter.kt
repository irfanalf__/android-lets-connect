package com.letsconnect.ui.discover_role.adapter

import android.content.Context
import android.graphics.Color
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.cardview.widget.CardView
import androidx.core.text.color
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.facebook.shimmer.ShimmerFrameLayout
import com.idlestar.ratingstar.RatingStarView
import com.letsconnect.R
import com.mcnmr.utilities.extension.asColor
import com.letsconnect.extension.glideCallback
import com.letsconnect.repository.local.room.entities.Interlocutor
import com.letsconnect.repository.network.response.DiscoverPersonResponse
import com.mcnmr.countryflagkit.Flags
import com.mcnmr.utilities.extension.*
import kotlinx.android.synthetic.main.row_discover_person_by_role.view.*
import java.util.*

class DiscoverRolePersonAdapter(private val context: Context,
                                private var list: List<DiscoverPersonResponse.Data.Person>,
                                private val onPersonSelected: (person: DiscoverPersonResponse.Data.Person, isAutoPlayIntroVideo: Boolean) -> Unit,
                                private val onChatClicked: (Interlocutor) -> Unit,
                                private val onSubroleClicked: (DiscoverPersonResponse.Data.Person) -> Unit):
    RecyclerView.Adapter<DiscoverRolePersonAdapter.DiscoverRolePersonVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiscoverRolePersonVH =
        DiscoverRolePersonVH(LayoutInflater.from(context).inflate(R.layout.row_discover_person_by_role,
            parent, false))

    override fun getItemCount(): Int = list.size
    override fun getItemViewType(position: Int): Int = position

    fun setNewData(list: List<DiscoverPersonResponse.Data.Person>){
        this.list = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: DiscoverRolePersonVH, position: Int) {
        holder.shimmerCivProfile.startShimmer()

        Glide.with(context).load(list[position].detail.image)
            .listener(glideCallback(onFinish = {
                holder.shimmerCivProfile.gone()
                holder.shimmerCivProfile.stopShimmer()
            }))
            .error(R.drawable.default_image_profile)
            .into(holder.ivProfile)

        holder.tvName.text = list[position].detail.fullname
        holder.tvRole.text = list[position].detail.subroleName.toUpperCase(Locale.getDefault())
        list[position].detail.linkvideo?.let {
            holder.tvIntroductionVideo.visible()
        }

        var specializations = context.getString(R.string.dashboard_specialize_in)
        list[position].specializations.forEach {
            specializations += "\n"
            specializations += context.getString(R.string.dashboard_specialities).format(it.specializeName)
        }
        holder.tvSpecialization.text = specializations
        holder.tvSpecialization.visibleOrInvisibleIf(list[position].isProfessional() or
                list[position].isSupporter())

        holder.tvOrganization.text = list[position].detail.organization
        holder.tvOrganization.visibleOrGoneIf(list[position].isVolunteer())

        holder.ivOnlineStatus.setBackgroundColor(list[position].detail.statusAvailableColor.asColor())

        if(list[position].detail.aboutMe?.length ?: 0 > 140){
            holder.tvAboutMe.text = SpannableStringBuilder()
                .append(list[position].detail.aboutMe?.substring(0, 130))
                .color(context.getCompatColor(R.color.red5757)){
                    append(context.getString(R.string.dashboard_read_more))
                }
        }else {
            holder.tvAboutMe.text = list[position].detail.aboutMe
        }

        val flag = Flags.get(list[position].detail.locationCode)
        if(flag != Flags.UNKNOWN_COUNTRY_CODE){
            holder.ivFlag.setImageResource(flag)
        }
        holder.rating.rating = list[position].detail.rating?.toFloat() ?: 0f

        if(list[position].isVolunteer()){
            holder.btnChat.text = context.getString(R.string.dashboard_connect_volunteer)
                .format((list[position].detail.fullname ?: "").split(" ")[0])
        }else {
            holder.btnChat.text = context.getString(R.string.dashboard_connect)
                .format((list[position].detail.fullname ?: "").split(" ")[0])
        }

        holder.btnChat.setOnClickListener { onChatClicked(Interlocutor().apply {
            id = list[position].detail.id
            email = list[position].detail.email
            fullname = list[position].detail.fullname
            image = list[position].detail.image
            username = list[position].detail.username
        }) }
        holder.tvIntroductionVideo.setOnClickListener { onPersonSelected.invoke(list[position], true) }
        holder.cvContainer.setOnClickListener { onPersonSelected.invoke(list[position], false) }
        holder.tvRole.setOnClickListener { onSubroleClicked.invoke(list[position]) }
    }

    inner class DiscoverRolePersonVH(v: View): RecyclerView.ViewHolder(v) {
        val cvContainer: CardView = v.cvContainer
        val ivProfile: AppCompatImageView = v.ivProfile
        val tvName: TextView = v.tvName
        val tvRole: TextView = v.tvRole
        val tvSpecialization: TextView = v.tvSpecialization
        val tvAboutMe: TextView = v.tvAboutMe
        val ivFlag: AppCompatImageView = v.ivFlag
        val ivOnlineStatus: AppCompatImageView = v.ivOnlineStatus
        val rating: RatingStarView = v.rating
        val tvIntroductionVideo: TextView = v.tvIntrouctionVideo
        val tvOrganization: TextView = v.tvOrganization
        val btnChat: AppCompatButton = v.btnChat
        val shimmerCivProfile: ShimmerFrameLayout = v.shimmerCivProfile
    }
}