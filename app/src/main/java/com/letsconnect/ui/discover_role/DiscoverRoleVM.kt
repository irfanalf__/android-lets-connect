package com.letsconnect.ui.discover_role

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.letsconnect.MainApplication
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.ancestors.BaseViewModel
import com.letsconnect.repository.network.request.RemoteRepository
import com.letsconnect.repository.network.response.DiscoverByRoleResponse
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

class DiscoverRoleVM(activity: BaseActivity): BaseViewModel(activity) {
    companion object {
        const val GET_DISCOVER_PERSON = "GET_DISCOVER_PERSON"
    }

    @Inject
    lateinit var remoteRepository: RemoteRepository

    val showLoadingDiscoverPersonEvent = MutableLiveData<Boolean>()
    val discoverPersonResponse = MutableLiveData<DiscoverByRoleResponse>()
    private var discoverJob: Job? = null

    init { (activity.application as MainApplication).consumer.inject(this) }

    fun executeDiscover(role: String, query: String = ""){
        discoverJob?.cancel()
        discoverJob = viewModelScope.launch {
            activity.shouldShowLoading(GET_DISCOVER_PERSON)
            val result = safeApiCall(GET_DISCOVER_PERSON) {
                remoteRepository.discoverPersonByRole(query, role)
            }
            activity.shouldHideLoading(GET_DISCOVER_PERSON)
            result.consume(
                onErrorResponse = activity::onErrorResponse,
                onHttpError = activity::onHttpError,
                onNetworkError = activity::onNetworkError,
                onTimeoutError = activity::onTimeoutError,
                onUnknownError = activity::onUnknownError,
                onSuccess = { discoverPersonResponse.value = it }
            )
        }
    }
}