package com.letsconnect.ui.detail_person

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.ancestors.BaseViewModel
import com.letsconnect.repository.network.request.RemoteRepository
import javax.inject.Inject

class DetailPersonVM(activity: BaseActivity): BaseViewModel(activity) {
    companion object{
        const val GET_REVIEW_USER = "GET_REVIEW_USER"
    }

    @Inject
    lateinit var remoteRepository: RemoteRepository

    init { app.consumer.inject(this) }

}