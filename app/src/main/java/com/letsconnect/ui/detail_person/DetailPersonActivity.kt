package com.letsconnect.ui.detail_person

import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.glideCallback
import com.letsconnect.model.Role
import com.letsconnect.repository.local.room.entities.Interlocutor
import com.letsconnect.repository.network.response.DiscoverPersonResponse
import com.letsconnect.ui.chat.ChatActivity
import com.letsconnect.ui.detail_person.adapter.DetailPersonSpecializationAdapter
import com.letsconnect.ui.detail_person.adapter.ReviewAdapter
import com.letsconnect.ui.login.LoginActivity
import com.mcnmr.countryflagkit.Flags
import com.mcnmr.utilities.extension.*
import com.mcnmr.utilities.internal_plugin.BooleanIntent
import com.mcnmr.utilities.internal_plugin.SerializableIntent
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener
import kotlinx.android.synthetic.main.activity_detail_person.*
import org.jetbrains.anko.startActivity

class DetailPersonActivity : BaseActivity() {
    companion object{
        const val PERSON_ARGUMENT = "person_argument"
        const val AUTO_PLAY_ARGUMENT = "auto_play_argument"
    }

    @SerializableIntent(PERSON_ARGUMENT)
    lateinit var person: DiscoverPersonResponse.Data.Person

    @BooleanIntent(AUTO_PLAY_ARGUMENT)
    @JvmField
    var isAutoPlayIntroVideo: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_person)

        lifecycle.addObserver(ytIntroduction)

        tvTitle.text = person.detail.fullname
        tvName.text = person.detail.fullname
        tvRole.text = Role.get(person.detail.roleId, person.detail.typeNamerole).toString()
        rating.rating = person.detail.rating?.toFloat() ?: 0f
        Glide.with(this).load(person.detail.image)
            .listener(glideCallback(onFinish = {
                shimmerCivProfile.stopShimmer()
                shimmerCivProfile.gone()
            }))
            .error(R.drawable.default_image_profile)
            .into(civProfile)
        person.detail.linkvideo?.let {
            ytIntroduction.visible()
            ytIntroduction.addYouTubePlayerListener(object: AbstractYouTubePlayerListener(){
                override fun onReady(youTubePlayer: YouTubePlayer) {
                    youTubePlayer.loadVideo(it, 0f)
                    if(isAutoPlayIntroVideo){
                        youTubePlayer.play()
                    }else {
                        youTubePlayer.pause()
                    }
                }
            })
        }

        when(person.detail.roleId){
            Role.ROLE_VOLUNTEER -> {
                tvAboutMeValue.text = person.detail.aboutMe

                val flag = Flags.get(person.detail.locationCode)
                if(flag != Flags.UNKNOWN_COUNTRY_CODE){
                    ivFlag.setImageResource(flag)
                }
            }
            Role.ROLE_SUPPORTER, Role.ROLE_PROFESSIONAL -> {
                val flag = Flags.get(person.detail.locationCode)
                if(flag != Flags.UNKNOWN_COUNTRY_CODE){
                    ivFlag.setImageResource(flag)
                }

                cvOverview.visible()
                cvReviews.visible()
                clRates.visible()

                tvAboutMeValue.text = person.detail.aboutMe
                tvWorkWithValue.text = person.detail.workWith
                tvProvidesSessionsValue.text = person.detail.ourSessions
                tvSpeakFluentValue.text = person.detail.speakFluent
                tvTrialSessionValue.text = getString(R.string.detail_person_usd).format(person.detail.trialPer)
                tvFullSessionValue.text = getString(R.string.detail_person_usd).format(person.detail.ratePer)

                person.specializations.isNotEmpty().doIfTrue {
                    person.specializations.forEach {
                        Log.e("Specializations", it.specializeName ?: "Empty String")
                    }
                    cvSpecialities.visible()
                    rvSpecialization.layoutManager = LinearLayoutManager(this)
                    rvSpecialization.adapter = DetailPersonSpecializationAdapter(this, person.specializations)
                }
                person.detail.licensing.isNotEmptyOrNotNull().doIfTrue {
                    cvLicensing.visible()
                    tvLicensing.text = person.detail.licensing
                }

                rvReviews.layoutManager = LinearLayoutManager(this)
                rvReviews.adapter = ReviewAdapter(this, person.reviews)
            }
        }

        if(person.isVolunteer()){
            btnChat.text = getString(R.string.detail_person_lets_connect)
        }else {
            btnChat.text = getString(R.string.detail_person_connect)
                .format((person.detail.fullname ?: "").split(" ")[0])
        }
        
        btnChat.setOnClickListener {
            preference.getUser().doIf(
                ifNull = {
                    startActivity<LoginActivity>()
                },
                ifNotNull = {
                    startActivity<ChatActivity>(ChatActivity.INTERLOCUTOR_ARGUMENT to Interlocutor().apply {
                        id = person.detail.id
                        email = person.detail.email
                        fullname = person.detail.fullname
                        image = person.detail.image
                        username = person.detail.username
                    })
                }
            )
        }
        llBack.setOnClickListener { onBackPressed() }
    }

}