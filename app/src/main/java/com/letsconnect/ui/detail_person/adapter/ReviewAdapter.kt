package com.letsconnect.ui.detail_person.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.letsconnect.R
import com.letsconnect.repository.network.response.DiscoverPersonResponse
import kotlinx.android.synthetic.main.row_review.view.*

class ReviewAdapter(private val context: Context,
                    private val list: List<DiscoverPersonResponse.Data.Person.Review>): RecyclerView.Adapter<ReviewAdapter.ReviewVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewVH =
        ReviewVH(LayoutInflater.from(context).inflate(R.layout.row_review, parent, false))

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ReviewVH, position: Int) {
        holder.ratingBar.progress = list[position].rating.toInt()
        holder.tvReview.text = list[position].rating
        holder.tvDate.text = list[position].date
    }

    inner class ReviewVH(v: View): RecyclerView.ViewHolder(v) {
        val ratingBar: RatingBar = v.ratingBar
        val tvReview: TextView = v.tvReview
        val tvDate: TextView = v.tvDate
    }
}