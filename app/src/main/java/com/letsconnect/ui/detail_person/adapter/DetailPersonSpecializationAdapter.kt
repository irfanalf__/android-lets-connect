package com.letsconnect.ui.detail_person.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.letsconnect.R
import com.letsconnect.repository.network.response.DiscoverPersonResponse
import kotlinx.android.synthetic.main.row_person.view.*

class DetailPersonSpecializationAdapter(private val context: Context,
                                        private val list: List<DiscoverPersonResponse.Data.Person.Specialization>):
    RecyclerView.Adapter<DetailPersonSpecializationAdapter.DetailPersonSpecializationVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailPersonSpecializationVH =
        DetailPersonSpecializationVH(LayoutInflater.from(context)
            .inflate(R.layout.row_detail_person_specialization, parent, false))

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: DetailPersonSpecializationVH, position: Int) {
        holder.tvSpecialization.text = list[position].specializeName
    }

    inner class DetailPersonSpecializationVH(v: View): RecyclerView.ViewHolder(v){
        val tvSpecialization: TextView = v.tvSpecialization
    }
}