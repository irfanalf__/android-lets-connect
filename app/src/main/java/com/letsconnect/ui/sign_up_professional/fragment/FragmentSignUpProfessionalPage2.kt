package com.letsconnect.ui.sign_up_professional.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.jakewharton.rxbinding4.widget.checkedChanges
import com.jakewharton.rxbinding4.widget.itemSelections
import com.jakewharton.rxbinding4.widget.textChanges
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.instantiateViewModel
import com.letsconnect.repository.network.response.RegistrationDataResponse
import com.letsconnect.ui.sign_up_professional.SignUpProfessionalActivity
import com.letsconnect.ui.sign_up_professional.SignUpProfessionalVM
import com.mcnmr.utilities.extension.getCompatColor
import kotlinx.android.synthetic.main.fragment_sign_up_professional_page2.*

@Suppress("UNCHECKED_CAST")
class FragmentSignUpProfessionalPage2 private constructor(): Fragment() {
    companion object {
        fun newInstance(data: ArrayList<RegistrationDataResponse.Location>): FragmentSignUpProfessionalPage2 =
            FragmentSignUpProfessionalPage2().apply {
                arguments = Bundle().apply { putSerializable(SignUpProfessionalActivity.REGISTRATION_DATA, data) }
            }
    }

    private val vm by lazy { (requireActivity() as BaseActivity).instantiateViewModel<SignUpProfessionalVM>() }
    private val genders by lazy { arrayOf(getString(R.string.signup_male), getString(R.string.signup_female), getString(R.string.signup_non_binary)) }
    private lateinit var location: List<RegistrationDataResponse.Location>

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.let {
            location = it.getSerializable(SignUpProfessionalActivity.REGISTRATION_DATA) as ArrayList<RegistrationDataResponse.Location>
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_sign_up_professional_page2, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        spGender.adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, genders)
        spLocation.adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, location)

        vm.step2Validation.observe(viewLifecycleOwner, Observer {
            cvForward.isEnabled = it
            cvForward.setCardBackgroundColor(getCompatColor(if(it) android.R.color.black else
                android.R.color.darker_gray))
        })

        etSkype.textChanges().subscribe { vm.skypeData.value = it.toString() }
        spGender.itemSelections().subscribe { vm.genderData.value = genders[it] }
        spLocation.itemSelections().subscribe { vm.locationData.value = location[it] }
        cbAdults.checkedChanges().subscribe { vm.workWithAdultsData.value = it }
        cbAdolescents.checkedChanges().subscribe { vm.workWithAdolescentsData.value = it }
        cbCouples.checkedChanges().subscribe { vm.workWithCouplesData.value = it }
        cbLiveChat.checkedChanges().subscribe { vm.isLiveChatData.value = it }
        cbVideoCall.checkedChanges().subscribe { vm.isVideoCallData.value = it }
        cbVoiceCall.checkedChanges().subscribe { vm.isVoiceCallData.value = it }

        cvForward.setOnClickListener { vm.gotoStep3Event.trigger() }
    }
}