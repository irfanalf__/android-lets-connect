package com.letsconnect.ui.sign_up_professional.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.flexbox.FlexboxLayoutManager
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.instantiateViewModel
import com.letsconnect.repository.network.response.RegistrationDataResponse
import com.letsconnect.ui.sign_up_professional.SignUpProfessionalActivity
import com.letsconnect.ui.sign_up_professional.SignUpProfessionalVM
import com.letsconnect.ui.sign_up_professional.adapter.SpecializationAdapter
import com.mcnmr.utilities.extension.getCompatColor
import kotlinx.android.synthetic.main.fragment_sign_up_professional_page4.*

@Suppress("UNCHECKED_CAST")
class FragmentSignUpProfessionalPage4 private constructor(): Fragment() {
    companion object {
        fun newInstance(data: ArrayList<RegistrationDataResponse.Specialize>): FragmentSignUpProfessionalPage4 =
            FragmentSignUpProfessionalPage4().apply {
                arguments = Bundle().apply { putSerializable(SignUpProfessionalActivity.REGISTRATION_DATA, data) }
            }
    }

    private val vm by lazy { (requireActivity() as BaseActivity).instantiateViewModel<SignUpProfessionalVM>() }
    private lateinit var specialize: ArrayList<RegistrationDataResponse.Specialize>

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.let {
            specialize = it.getSerializable(SignUpProfessionalActivity.REGISTRATION_DATA) as ArrayList<RegistrationDataResponse.Specialize>
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_sign_up_professional_page4, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rvSpecialization.layoutManager = object : FlexboxLayoutManager(requireContext()){
            override fun canScrollVertically(): Boolean = false
        }
        rvSpecialization.adapter = SpecializationAdapter(requireContext(), specialize, vm.listSpecializationData)

        vm.step4Validation.observe(viewLifecycleOwner, Observer {
            cvForward.isEnabled = it
            cvForward.setCardBackgroundColor(getCompatColor(if(it) android.R.color.black else
                android.R.color.darker_gray))
        })

        cvForward.setOnClickListener { vm.gotoStep5Event.trigger() }
    }
}