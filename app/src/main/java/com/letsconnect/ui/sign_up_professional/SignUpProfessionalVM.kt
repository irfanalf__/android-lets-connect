package com.letsconnect.ui.sign_up_professional

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.letsconnect.MainApplication
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.ancestors.BaseResponse
import com.letsconnect.ancestors.BaseViewModel
import com.letsconnect.model.Role
import com.letsconnect.repository.network.parameter.SignUpProfessionalParams
import com.letsconnect.repository.network.request.RemoteRepository
import com.letsconnect.repository.network.response.RegistrationDataResponse
import com.mcnmr.utilities.extension.doIf
import com.mcnmr.utilities.extension.isNotEmptyOrNotNull
import com.mcnmr.utilities.extension.isNotNull
import com.mcnmr.utilities.internal_plugin.combineLiveData
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import com.mcnmr.utilities.extension.gt
import com.mcnmr.utilities.extension.isMultipliedBy
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import javax.inject.Inject

class SignUpProfessionalVM(activity: BaseActivity): BaseViewModel(activity) {
    companion object{
        const val POST_REGISTER_PROFESSIONAL = "POST_REGISTER_PROFESSIONAL"
    }

    @Inject
    lateinit var remoteRepository: RemoteRepository

    val readStoragePermissionPhotoEvent = SingleEventWrapper<Any?>()
    val readStoragePermissionPhotoResult = SingleEventWrapper<Boolean>()
    val readStoragePermissionCopiesCertEvent = SingleEventWrapper<Any?>()
    val readStoragePermissionCopiesCertResult = SingleEventWrapper<Boolean>()
    val gotoStep1Event = SingleEventWrapper<Any?>()
    val gotoStep2Event = SingleEventWrapper<Any?>()
    val gotoStep3Event = SingleEventWrapper<Any?>()
    val gotoStep4Event = SingleEventWrapper<Any?>()
    val gotoStep5Event = SingleEventWrapper<Any?>()
    val gotoLoginPageEvent = SingleEventWrapper<Any?>()
    val registerResponse = SingleEventWrapper<BaseResponse>()

    val roleData = MutableLiveData<Role>()
    val photoData = MutableLiveData<File>()
    val introductionVideoData = MutableLiveData<String>()
    val fullNameData = MutableLiveData<String>()

    val skypeData = MutableLiveData<String>()
    val genderData = MutableLiveData<String>()
    val locationData = MutableLiveData<RegistrationDataResponse.Location>()
    val workWithAdultsData = MutableLiveData<Boolean>()
    val workWithAdolescentsData = MutableLiveData<Boolean>()
    val workWithCouplesData = MutableLiveData<Boolean>()
    val isLiveChatData = MutableLiveData<Boolean>()
    val isVoiceCallData = MutableLiveData<Boolean>()
    val isVideoCallData = MutableLiveData<Boolean>()

    val aboutMeData = MutableLiveData<String>()
    val licensingData = MutableLiveData<String>()
    val certificationData = MutableLiveData<String>()
    val copiesCertificationData = MutableLiveData<File>()

    val listSpecializationData = MutableLiveData<MutableList<RegistrationDataResponse.Specialize>>()

    val speakFluentData = MutableLiveData<String>()
    val charge20Data = MutableLiveData<String>()
    val charge50Data = MutableLiveData<String>()
    val emailData = MutableLiveData<String>()
    val passwordData = MutableLiveData<String>()
    val repeatPasswordData = MutableLiveData<String>()
    val visiblePasswordData = MutableLiveData<Boolean>()
    val visibleRepeatPasswordData = MutableLiveData<Boolean>()

    val step1Validation = combineLiveData(roleData, photoData, fullNameData, introductionVideoData)
    { a, b, c, d -> a.isNotNull() && b.isNotNull() && c.isNotEmptyOrNotNull() && d.isNotEmptyOrNotNull()}
    val step2Validation = combineLiveData(skypeData, genderData, locationData)
    { a, b, c -> a.isNotEmptyOrNotNull() && b.isNotEmptyOrNotNull() && c.isNotNull()}
    val step3Validation = combineLiveData(aboutMeData){ a -> a.isNotEmptyOrNotNull() }
    val step4Validation = combineLiveData(listSpecializationData){ a -> a != null && a.isNotEmpty() }
    val step5Validation = combineLiveData(speakFluentData, charge20Data, charge50Data, emailData, passwordData, repeatPasswordData)
    { a, b, c, d, e, f -> a.isNotEmptyOrNotNull() && b.isNotEmptyOrNotNull() &&
            (b?.toInt().isMultipliedBy(5) && b?.toInt().gt(0)) &&
            (c?.toInt().isMultipliedBy(5) && c?.toInt().gt(0)) &&
            c.isNotEmptyOrNotNull() && d.isNotEmptyOrNotNull() && e.isNotEmptyOrNotNull() &&
            f.isNotEmptyOrNotNull() && e.equals(f)}

    init {
        (activity.application as MainApplication).consumer.inject(this)
    }

    fun executeRegister(){
        val params = SignUpProfessionalParams(roleData.value?.id, introductionVideoData.value,
            fullNameData.value, skypeData.value, genderData.value, locationData.value?.locationCode,
            aboutMeData.value, licensingData.value, certificationData.value,
            passwordData.value, listSpecializationData.value, isLiveChatData.value,
            isVoiceCallData.value, isVideoCallData.value, speakFluentData.value, charge20Data.value,
            charge50Data.value, emailData.value, workWithAdultsData.value, workWithAdolescentsData.value,
            workWithCouplesData.value).asMultipart()
        val file = MultipartBody.Part.createFormData("profile_image",
            "${System.currentTimeMillis()}.png",
            photoData.value!!.asRequestBody("image/png".toMediaTypeOrNull()))

        copiesCertificationData.value.doIf(ifNotNull = {
            val certFile = MultipartBody.Part.createFormData("certification_image",
                "${System.currentTimeMillis()}.png",
                it.asRequestBody("image/png".toMediaTypeOrNull()))

            viewModelScope.launch {
                activity.shouldShowLoading(POST_REGISTER_PROFESSIONAL)
                val result = safeApiCall(POST_REGISTER_PROFESSIONAL){ remoteRepository.registerAsProfessional(params, file, certFile) }
                activity.shouldHideLoading(POST_REGISTER_PROFESSIONAL)
                result.consume(
                    onErrorResponse = activity::onErrorResponse,
                    onHttpError = activity::onHttpError,
                    onNetworkError = activity::onNetworkError,
                    onTimeoutError = activity::onTimeoutError,
                    onUnknownError = activity::onUnknownError,
                    onSuccess = { response -> registerResponse.value = response }
                )
            }
        }, ifNull = {
            viewModelScope.launch {
                activity.shouldShowLoading(POST_REGISTER_PROFESSIONAL)
                val result = safeApiCall(POST_REGISTER_PROFESSIONAL){ remoteRepository.registerAsProfessional(params, file) }
                activity.shouldHideLoading(POST_REGISTER_PROFESSIONAL)
                result.consume(
                    onErrorResponse = activity::onErrorResponse,
                    onHttpError = activity::onHttpError,
                    onNetworkError = activity::onNetworkError,
                    onTimeoutError = activity::onTimeoutError,
                    onUnknownError = activity::onUnknownError,
                    onSuccess = { response -> registerResponse.value = response }
                )
            }
        })
    }
}