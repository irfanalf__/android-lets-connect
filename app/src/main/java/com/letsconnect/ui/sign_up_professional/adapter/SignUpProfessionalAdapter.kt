package com.letsconnect.ui.sign_up_professional.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.letsconnect.repository.network.response.RegistrationDataResponse
import com.letsconnect.ui.sign_up_professional.fragment.*

class SignUpProfessionalAdapter(fm: FragmentManager, response: RegistrationDataResponse):
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val page1 = FragmentSignUpProfessionalPage1()
    private val page2 = FragmentSignUpProfessionalPage2.newInstance(response.location)
    private val page3 = FragmentSignUpProfessionalPage3()
    private val page4 = FragmentSignUpProfessionalPage4.newInstance(response.specialize)
    private val page5 = FragmentSignUpProfessionalPage5()

    override fun getItem(position: Int): Fragment = when(position){
        0 -> page1
        1 -> page2
        2 -> page3
        3 -> page4
        else -> page5
    }

    override fun getCount(): Int = 5
}