package com.letsconnect.ui.sign_up_professional

import android.Manifest
import android.os.Build
import android.os.Bundle
import androidx.lifecycle.Observer
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.instantiateViewModel
import com.letsconnect.extension.isPermissionGranted
import com.mcnmr.utilities.extension.openPermissionSettings
import com.letsconnect.repository.network.response.RegistrationDataResponse
import com.letsconnect.ui.login.LoginActivity
import com.letsconnect.ui.sign_up_professional.adapter.SignUpProfessionalAdapter
import com.mcnmr.utilities.internal_plugin.SerializableIntent
import kotlinx.android.synthetic.main.activity_sign_up_professional.*
import org.jetbrains.anko.startActivity

class SignUpProfessionalActivity : BaseActivity() {
    companion object {
        const val REGISTRATION_RESPONSE = "REGISTRATION_RESPONSE"
        const val REGISTRATION_DATA = "REGISTRATION_DATA"

        const val REQ_PHOTO_READ_EXTERNAL_STORAGE = 1
        const val REQ_CERT_READ_EXTERNAL_STORAGE = 2
    }

    private val vm by lazy { instantiateViewModel<SignUpProfessionalVM>() }

    @SerializableIntent(REGISTRATION_RESPONSE)
    lateinit var registrationDataResponse: RegistrationDataResponse

    override fun showBackButton(): Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_professional)

        viewPager.adapter = SignUpProfessionalAdapter(supportFragmentManager, registrationDataResponse)

        vm.readStoragePermissionPhotoEvent.observe(this, Observer {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE), REQ_PHOTO_READ_EXTERNAL_STORAGE)
            }else {
                vm.readStoragePermissionPhotoResult.value = true
            }
        })
        vm.readStoragePermissionCopiesCertEvent.observe(this, Observer {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE), REQ_CERT_READ_EXTERNAL_STORAGE)
            }else {
                vm.readStoragePermissionCopiesCertResult.value = true
            }
        })
        vm.registerResponse.observe(this, Observer {
            alertDialog(title = getString(R.string.alert_dialog_message_title),
                message = getString(R.string.signup_success_register),
                positiveTitle = getString(R.string.alert_dialog_close), onDismiss = { vm.gotoLoginPageEvent.trigger() })
        })
        vm.gotoStep1Event.observe(this, Observer { viewPager.currentItem = 0 })
        vm.gotoStep2Event.observe(this, Observer { viewPager.currentItem = 1 })
        vm.gotoStep3Event.observe(this, Observer { viewPager.currentItem = 2 })
        vm.gotoStep4Event.observe(this, Observer { viewPager.currentItem = 3 })
        vm.gotoStep5Event.observe(this, Observer { viewPager.currentItem = 4 })
        vm.gotoLoginPageEvent.observe(this, Observer { startActivity<LoginActivity>()
            finishAffinity()
        })
    }

    override fun onBackPressed() {
        when(viewPager.currentItem){
            0 -> {
                startActivity<LoginActivity>()
                finishAffinity()
            }
            1 -> vm.gotoStep1Event.trigger()
            2 -> vm.gotoStep2Event.trigger()
            3 -> vm.gotoStep3Event.trigger()
            4 -> vm.gotoStep4Event.trigger()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(isPermissionGranted(permissions, grantResults)){
            when(requestCode){
                REQ_PHOTO_READ_EXTERNAL_STORAGE -> vm.readStoragePermissionPhotoResult.value = true
                REQ_CERT_READ_EXTERNAL_STORAGE -> vm.readStoragePermissionCopiesCertResult.value = true
            }
        }else {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                (!shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        !shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE))){
                when(requestCode){
                    REQ_PHOTO_READ_EXTERNAL_STORAGE -> vm.readStoragePermissionPhotoResult.value = false
                    REQ_CERT_READ_EXTERNAL_STORAGE -> vm.readStoragePermissionCopiesCertResult.value = false
                }

                alertDialog(title = getString(R.string.permission_denied),
                    message = getString(R.string.permission_desc_read_storage),
                    negativeTitle = getString(R.string.alert_dialog_close),
                    positiveTitle = getString(R.string.permission_goto_setting),
                    positiveAction = { openPermissionSettings() }
                )
            }
        }
    }
}