package com.letsconnect.ui.notification_click

import android.os.Bundle
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.repository.local.room.entities.Interlocutor
import com.letsconnect.service.fcm.fcm_response.FCMReceiveMessage
import com.letsconnect.ui.chat.ChatActivity
import com.letsconnect.wrapper.NotificationWrapper
import com.mcnmr.utilities.internal_plugin.SerializableIntent
import org.jetbrains.anko.startActivity
import java.io.Serializable

class NotificationClickActivity: BaseActivity() {
    companion object {
        const val EXTRA_DATA = "extra_data"
    }

    @SerializableIntent(EXTRA_DATA)
    lateinit var extraData: NotificationWrapper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_click)

        when(extraData){
            is NotificationWrapper.ReceiveMessage -> {
                val data = (extraData as NotificationWrapper.ReceiveMessage).value
                val interlocutor = Interlocutor().apply {
                    id = data.senderId
                    email = data.senderEmail
                    fullname = data.senderFullname
                    image = data.senderImage
                    username = data.senderUsername
                }
                startActivity<ChatActivity>(
                    ChatActivity.INTERLOCUTOR_ARGUMENT to interlocutor,
                    FROM to this::class.java.name
                )
                finishAffinity()
            }
        }
    }
}