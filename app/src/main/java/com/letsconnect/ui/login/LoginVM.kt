package com.letsconnect.ui.login

import android.R.attr.key
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.ancestors.BaseViewModel
import com.mcnmr.utilities.extension.hexStringToByteArray
import com.mcnmr.utilities.extension.toHexString
import com.letsconnect.repository.local.LocalRepository
import com.letsconnect.repository.network.parameter.LoginParams
import com.letsconnect.repository.network.request.RemoteRepository
import com.letsconnect.repository.network.response.RegistrationDataResponse
import com.letsconnect.repository.network.response.UserResponse
import com.mcnmr.utilities.extension.isNotEmptyOrNotNull
import com.mcnmr.utilities.internal_plugin.combineLiveData
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.nio.charset.Charset
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec
import javax.inject.Inject
import kotlin.random.Random


class LoginVM(activity: BaseActivity): BaseViewModel(activity) {
    companion object {
        const val GET_REGISTRATION_DATA = "GET_REGISTRATION_DATA"
        const val POST_LOGIN = "POST_LOGIN"
    }

    @Inject
    lateinit var remoteRepository: RemoteRepository

    @Inject
    lateinit var localRepository: LocalRepository

    val registrationDataResponse = SingleEventWrapper<RegistrationDataResponse>()
    val loginResponse = SingleEventWrapper<UserResponse>()

    val emailData = MutableLiveData<String>()
    val passwordData = MutableLiveData<String>()
    val firebaseTokenData = MutableLiveData<String>()
    val visiblePasswordData = MutableLiveData<Boolean>()
    val validation = combineLiveData(emailData, passwordData)
    { a, b -> a.isNotEmptyOrNotNull() && b.isNotEmptyOrNotNull() }

    init { app.consumer.inject(this) }

    fun executeGetRegistrationData(){
        viewModelScope.launch {
            activity.shouldShowLoading(tag = GET_REGISTRATION_DATA)
            val result = safeApiCall(tag = GET_REGISTRATION_DATA) { remoteRepository.registrationData() }
            activity.shouldHideLoading(tag = GET_REGISTRATION_DATA)
            result.consume(
                onErrorResponse = activity::onErrorResponse,
                onHttpError = activity::onHttpError,
                onNetworkError = activity::onNetworkError,
                onTimeoutError = activity::onTimeoutError,
                onUnknownError = activity::onUnknownError,
                onSuccess = { registrationDataResponse.value = it}
            )
        }
    }

    fun executeLogin(){
        viewModelScope.launch {
            val params = LoginParams(
                email = emailData.value,
                password = passwordData.value,
                firebaseToken = firebaseTokenData.value
            )

            activity.shouldShowLoading(tag = POST_LOGIN)
            val result = safeApiCall(tag = POST_LOGIN) { remoteRepository.login(params) }
            result.consume(
                onErrorResponse = { tag, error ->
                    activity.shouldHideLoading(POST_LOGIN)
                    activity.onErrorResponse(tag, error)
                },
                onHttpError = { tag, code, message ->
                    activity.shouldHideLoading(POST_LOGIN)
                    activity.onHttpError(tag, code, message)
                },
                onNetworkError = { tag, error ->
                    activity.shouldHideLoading(POST_LOGIN)
                    activity.onNetworkError(tag, error)
                },
                onTimeoutError = { tag ->
                    activity.shouldHideLoading(POST_LOGIN)
                    activity.onTimeoutError(tag)
                },
                onUnknownError = { tag, message ->
                    activity.shouldHideLoading(POST_LOGIN)
                    activity.onUnknownError(tag, message)
                },
                onSuccess = {
                    viewModelScope.launch {
                        withContext(Dispatchers.IO) { localRepository.insertHistoryChat(it) }
                        activity.shouldHideLoading(POST_LOGIN)
                        loginResponse.value = it
                    }
                }
            )
        }
    }
}