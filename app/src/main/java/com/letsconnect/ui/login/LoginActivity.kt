package com.letsconnect.ui.login

import android.os.Bundle
import android.text.InputType
import androidx.lifecycle.Observer
import com.google.firebase.iid.FirebaseInstanceId
import com.jakewharton.rxbinding4.widget.textChanges
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.custom_dialog.SignUpDialog
import com.mcnmr.utilities.extension.getCompatDrawable
import com.letsconnect.extension.instantiateViewModel
import com.mcnmr.utilities.extension.launchUrl
import com.letsconnect.model.Role
import com.letsconnect.ui.dashboard.professional.DashboardProfessionalActivity
import com.letsconnect.ui.dashboard.client.DashboardClientActivity
import com.letsconnect.ui.dashboard.supporter.DashboardSupporterActivity
import com.letsconnect.ui.dashboard.volunteer.DashboardVolunteerActivity
import com.letsconnect.ui.sign_up_professional.SignUpProfessionalActivity
import com.letsconnect.ui.sign_up_client.SignUpClientActivity
import com.letsconnect.ui.sign_up_supporter.SignUpSupporterActivity
import com.letsconnect.ui.sign_up_volunteer.SignUpVolunteerActivity
import com.mcnmr.utilities.extension.isNull
import com.mcnmr.utilities.extension.showIfNotShowing
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity

class LoginActivity : BaseActivity() {
    private val signUpDialog by lazy { SignUpDialog(this, ::onSignUpSelected) }
    private val vm by lazy { instantiateViewModel<LoginVM>() }

    private lateinit var action: SignUpDialog.SignUpDialogAction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {
            if(!it.isSuccessful){
                return@addOnCompleteListener
            }

            vm.firebaseTokenData.value = it.result?.token
        }
        vm.visiblePasswordData.observe(this, Observer {
            if(it){
                etPassword.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                etPassword.setSelection(etPassword.text.length)
                ivPasswordVisibility.setImageResource(R.drawable.ic_visibility_off)
            }else {
                etPassword.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                etPassword.setSelection(etPassword.text.length)
                ivPasswordVisibility.setImageResource(R.drawable.ic_visibility)
            }
        })
        vm.validation.observe(this, Observer {
            btnLogin.isEnabled = it
            btnLogin.background = getCompatDrawable(if(it) R.drawable.bg_radius_button_black else
                R.drawable.bg_radius_button_grey)
        })
        vm.registrationDataResponse.observe(this, Observer {
            when (action) {
                SignUpDialog.SignUpDialogAction.VOLUNTEER -> {
                    startActivity<SignUpVolunteerActivity>(SignUpVolunteerActivity.REGISTRATION_RESPONSE to it)
                }
                SignUpDialog.SignUpDialogAction.SUPPORTER -> {
                    startActivity<SignUpSupporterActivity>(SignUpProfessionalActivity.REGISTRATION_RESPONSE to it)
                }
                else -> {
                    startActivity<SignUpProfessionalActivity>(SignUpProfessionalActivity.REGISTRATION_RESPONSE to it)
                }
            }
        })
        vm.loginResponse.observe(this, Observer {
            preference.saveUser(it)

            when(it.user.roleId){
                Role.ROLE_VOLUNTEER -> startActivity<DashboardVolunteerActivity>()
                Role.ROLE_CLIENT -> startActivity<DashboardClientActivity>()
                Role.ROLE_PROFESSIONAL -> startActivity<DashboardProfessionalActivity>()
                Role.ROLE_SUPPORTER -> startActivity<DashboardSupporterActivity>()
            }

            finishAffinity()
        })

        etEmail.textChanges().subscribe { vm.emailData.value = it.toString() }
        etPassword.textChanges().subscribe { vm.passwordData.value = it.toString() }
        btnLogin.setOnClickListener { vm.executeLogin() }
        tvSignup.setOnClickListener { signUpDialog.showIfNotShowing() }
        tvForgotPassword.setOnClickListener { launchUrl("https://letsconnectproject.com/auth/forgot_password") }
        ivPasswordVisibility.setOnClickListener { vm.visiblePasswordData.value = if(vm.visiblePasswordData.value.isNull()) true else !vm.visiblePasswordData.value!! }
        llBack.setOnClickListener { onBackPressed() }
    }

    private fun onSignUpSelected(action: SignUpDialog.SignUpDialogAction){
        this.action = action
        when(action){
            SignUpDialog.SignUpDialogAction.CLIENT -> startActivity<SignUpClientActivity>()
            SignUpDialog.SignUpDialogAction.SUPPORTER -> launchUrl("https://letsconnectproject.com/auth/register/supporter")
            SignUpDialog.SignUpDialogAction.PROFESSIONAL -> launchUrl("https://letsconnectproject.com/auth/register/mentalhealthprofessional")
            SignUpDialog.SignUpDialogAction.VOLUNTEER -> vm.executeGetRegistrationData()
        }
    }
}