package com.letsconnect.ui.sign_up_client

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.ancestors.BaseResponse
import com.letsconnect.ancestors.BaseViewModel
import com.mcnmr.utilities.extension.isAlphanumeric
import com.mcnmr.utilities.extension.isEmailAddress
import com.mcnmr.utilities.extension.isEmpty
import com.letsconnect.repository.network.parameter.SignUpClientParams
import com.letsconnect.repository.network.request.RemoteRepository
import com.mcnmr.utilities.extension.isNotEmptyOrNotNull
import com.mcnmr.utilities.extension.notNullAndTrue
import com.mcnmr.utilities.internal_plugin.combineLiveData
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class SignUpClientVM(activity: BaseActivity): BaseViewModel(activity) {
    companion object {
        const val POST_REGISTER_AS_CLIENT = "REQUEST_REGISTER_AS_CLIENT"
    }

    @Inject
    lateinit var remoteRepository: RemoteRepository

    val gotoLoginPageEvent = SingleEventWrapper<Any?>()
    val registerResponse = SingleEventWrapper<BaseResponse>()

    val screenNameData = MutableLiveData<String>()
    val emailData = MutableLiveData<String>()
    val passwordData = MutableLiveData<String>()
    val repeatPasswordData = MutableLiveData<String>()
    val termConditionsData = MutableLiveData<Boolean>()
    val over13YearsData = MutableLiveData<Boolean>()
    val showPasswordData = MutableLiveData<Boolean>()
    val showRepeatPasswordData = MutableLiveData<Boolean>()

    val screenNameValidation = combineLiveData(screenNameData){
        if(!it.isNotEmptyOrNotNull() || (it ?: " ").split(" ").size != 1){
            activity.getString(R.string.error_username_empty)
        }else {
            ""
        }
    }
    val emailValidation = combineLiveData(emailData){ a ->
        if(!a.isNotEmptyOrNotNull()){
            activity.getString(R.string.error_email_empty)
        }else if(!a.isEmailAddress()){
            activity.getString(R.string.error_invalid_email)
        }else {
            ""
        }
    }
    val passwordValidation = combineLiveData(passwordData){ a ->
        if(!a.isNotEmptyOrNotNull()){
            activity.getString(R.string.error_password_empty)
        }else if(!a.isAlphanumeric() || (a?.length ?: 0) < 8 ){
            activity.getString(R.string.error_password_pattern_not_valid)
        }else {
            ""
        }
    }
    val repeatPasswordValidation = combineLiveData(passwordData, repeatPasswordData){ a, b ->
        if(!a.equals(b)){
            activity.getString(R.string.error_invalid_repeat_password)
        }else {
            ""
        }
    }

    val validation = combineLiveData(screenNameValidation, emailValidation,
        passwordValidation, repeatPasswordValidation, termConditionsData, over13YearsData)
    { a, b, c, d, e, f -> a.isEmpty() && b.isEmpty() && c.isEmpty() && d.isEmpty()
            && e.notNullAndTrue() && f.notNullAndTrue() }

    init { app.consumer.inject(this) }

    fun executeRegister(){
        val params = SignUpClientParams(screenNameData.value,
            emailData.value?.toLowerCase(Locale.getDefault()), passwordData.value)

        viewModelScope.launch {
            activity.shouldShowLoading(POST_REGISTER_AS_CLIENT)
            val result = safeApiCall(POST_REGISTER_AS_CLIENT){ remoteRepository.registerAsClient(params) }
            activity.shouldHideLoading(POST_REGISTER_AS_CLIENT)
            result.consume(
                onErrorResponse = activity::onErrorResponse,
                onHttpError = activity::onHttpError,
                onNetworkError = activity::onNetworkError,
                onTimeoutError = activity::onTimeoutError,
                onUnknownError = activity::onUnknownError,
                onSuccess = { registerResponse.value = it }
            )
        }
    }
}