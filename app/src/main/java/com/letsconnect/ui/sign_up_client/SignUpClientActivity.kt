package com.letsconnect.ui.sign_up_client

import android.os.Bundle
import android.text.InputType
import androidx.lifecycle.Observer
import com.jakewharton.rxbinding4.widget.checkedChanges
import com.jakewharton.rxbinding4.widget.textChanges
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.mcnmr.utilities.extension.error
import com.mcnmr.utilities.extension.getCompatDrawable
import com.letsconnect.extension.instantiateViewModel
import com.letsconnect.ui.login.LoginActivity
import com.mcnmr.utilities.extension.isNull
import kotlinx.android.synthetic.main.activity_sign_up_client.*
import org.jetbrains.anko.startActivity

class SignUpClientActivity : BaseActivity() {

    private val vm by lazy { instantiateViewModel<SignUpClientVM>() }

    override fun showBackButton(): Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_client)

        vm.showPasswordData.observe(this, Observer {
            if(it){
                etPassword.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                etPassword.setSelection(etPassword.text?.length ?: 0)
                ivPasswordVisibility.setImageResource(R.drawable.ic_visibility_off)
            }else {
                etPassword.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                etPassword.setSelection(etPassword.text?.length ?: 0)
                ivPasswordVisibility.setImageResource(R.drawable.ic_visibility)
            }
        })
        vm.showRepeatPasswordData.observe(this, Observer {
            if(it){
                etRepeatPassword.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                etRepeatPassword.setSelection(etRepeatPassword.text?.length ?: 0)
                ivRepeatPasswordVisibility.setImageResource(R.drawable.ic_visibility_off)
            }else {
                etRepeatPassword.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                etRepeatPassword.setSelection(etRepeatPassword.text?.length ?: 0)
                ivRepeatPasswordVisibility.setImageResource(R.drawable.ic_visibility)
            }
        })
        vm.screenNameValidation.observe(this, Observer { tvErrorScreenName.error(it) })
        vm.emailValidation.observe(this, Observer { tvErrorEmail.error(it) })
        vm.passwordValidation.observe(this, Observer { tvErrorPassword.error(it) })
        vm.repeatPasswordValidation.observe(this, Observer { tvErrorRepeatPassword.error(it) })
        vm.validation.observe(this, Observer {
            btnSignUp.isEnabled = it
            btnSignUp.background = getCompatDrawable(if(it) R.drawable.bg_radius_button_black else
                R.drawable.bg_radius_button_grey)
        })
        vm.registerResponse.observe(this, Observer {
            alertDialog(title = getString(R.string.alert_dialog_message_title),
                message = getString(R.string.signup_success_register),
                positiveTitle = getString(R.string.alert_dialog_close),
                onDismiss = { vm.gotoLoginPageEvent.trigger() })
        })
        vm.gotoLoginPageEvent.observe(this, Observer { startActivity<LoginActivity>()
            finishAffinity()
        })

        etScreenName.textChanges().subscribe { vm.screenNameData.value = it.toString() }
        etEmail.textChanges().subscribe { vm.emailData.value = it.toString() }
        etPassword.textChanges().subscribe { vm.passwordData.value = it.toString() }
        etRepeatPassword.textChanges().subscribe { vm.repeatPasswordData.value = it.toString() }
        cbTermConditions.checkedChanges().subscribe { vm.termConditionsData.value = it }
        cbOver13Age.checkedChanges().subscribe { vm.over13YearsData.value = it }

        ivPasswordVisibility.setOnClickListener {
            vm.showPasswordData.value = if(vm.showPasswordData.value.isNull()) true else !vm.showPasswordData.value!!
        }
        ivRepeatPasswordVisibility.setOnClickListener {
            vm.showRepeatPasswordData.value = if(vm.showRepeatPasswordData.value.isNull()) true else !vm.showRepeatPasswordData.value!!
        }
        btnSignUp.setOnClickListener { vm.executeRegister() }
        tvLogin.setOnClickListener { vm.gotoLoginPageEvent.trigger() }
    }

    override fun onBackPressed() {
        startActivity<LoginActivity>()
        finishAffinity()
    }
}