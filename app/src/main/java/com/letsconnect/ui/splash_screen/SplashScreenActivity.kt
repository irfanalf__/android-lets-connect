package com.letsconnect.ui.splash_screen

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.core.os.postDelayed
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.*
import com.letsconnect.model.Role
import com.letsconnect.repository.network.response.ErrorResponseException
import com.letsconnect.ui.dashboard.client.DashboardClientActivity
import com.letsconnect.ui.dashboard.professional.DashboardProfessionalActivity
import com.letsconnect.ui.dashboard.supporter.DashboardSupporterActivity
import com.letsconnect.ui.dashboard.volunteer.DashboardVolunteerActivity
import com.letsconnect.ui.onboarding.OnboardingActivity
import com.mcnmr.utilities.extension.doIf
import org.jetbrains.anko.startActivity
import java.io.IOException

class SplashScreenActivity : BaseActivity() {

    private val vm by lazy { instantiateViewModel<SplashScreenVM>() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        vm.needUpdateResponse.observe(this, Observer {
            if(it){
                alertDialog(title = getString(R.string.alert_dialog_new_version_title),
                    message = getString(R.string.alert_dialog_new_version_message),
                    positiveTitle = getString(R.string.alert_dialog_update_now),
                    positiveAction = {
                        try {
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")))
                        } catch (anfe: ActivityNotFoundException) {
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$packageName")))
                        }
                    },
                    cancelable = false)
            }else {
                alreadyUpToDate()
            }
        })

        Handler().postDelayed(1000){
            vm.checkVersion()
        }
    }

    override fun onNetworkError(tag: Any, exception: IOException) {
        alertDialog(title = getString(R.string.alert_dialog_error_title),
            message = getString(R.string.alert_dialog_no_internet_message).format(exception.message),
            positiveTitle = getString(R.string.alert_dialog_try_again),
            positiveAction = { vm.checkVersion() },
            cancelable = false)
    }

    override fun onTimeoutError(tag: Any) {
        alertDialog(title = getString(R.string.alert_dialog_error_title),
            message = getString(R.string.alert_dialog_rto_message),
            positiveTitle = getString(R.string.alert_dialog_close),
            positiveAction = { vm.checkVersion() },
            cancelable = false)
    }

    override fun onErrorResponse(tag: Any, exception: ErrorResponseException) {
        alreadyUpToDate()
    }

    private fun alreadyUpToDate(){
        preference.getUser().doIf(
            ifNull = {
                startActivity<OnboardingActivity>()
            },
            ifNotNull = {
                Log.e("USER", Gson().toJson(it).toString())
                when(it.user.roleId){
                    Role.ROLE_VOLUNTEER -> startActivity<DashboardVolunteerActivity>()
                    Role.ROLE_CLIENT -> startActivity<DashboardClientActivity>()
                    Role.ROLE_PROFESSIONAL -> startActivity<DashboardProfessionalActivity>()
                    Role.ROLE_SUPPORTER -> startActivity<DashboardSupporterActivity>()
                }
            }
        )

        finishAffinity()
    }
}