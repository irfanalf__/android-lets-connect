package com.letsconnect.ui.splash_screen

import androidx.lifecycle.viewModelScope
import com.letsconnect.BuildConfig
import com.letsconnect.MainApplication
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.ancestors.BaseViewModel
import com.letsconnect.repository.network.request.RemoteRepository
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import kotlinx.coroutines.launch
import javax.inject.Inject

class SplashScreenVM(activity: BaseActivity): BaseViewModel(activity) {
    companion object {
        const val REQUEST_CHECK_VERSION = "REQUEST_CHECK_VERSION"
    }

    @Inject
    lateinit var remoteRepository: RemoteRepository

    val needUpdateResponse = SingleEventWrapper<Boolean>()

    init { (activity.application as MainApplication).consumer.inject(this) }

    fun checkVersion(){
        viewModelScope.launch {
            activity.shouldShowLoading(REQUEST_CHECK_VERSION)
            val result = safeApiCall(REQUEST_CHECK_VERSION){ remoteRepository.checkVersion(
                BuildConfig.VERSION_CODE) }
            activity.shouldHideLoading(REQUEST_CHECK_VERSION)
            result.consume(
                onErrorResponse = activity::onErrorResponse,
                onHttpError = activity::onHttpError,
                onNetworkError = activity::onNetworkError,
                onTimeoutError = activity::onTimeoutError,
                onUnknownError = activity::onUnknownError,
                onSuccess = { needUpdateResponse.value = it.updateRequired }
            )
        }
    }
}