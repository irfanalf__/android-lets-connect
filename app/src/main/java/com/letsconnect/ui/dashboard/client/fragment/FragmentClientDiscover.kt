package com.letsconnect.ui.dashboard.client.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding4.widget.textChanges
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.instantiateViewModel
import com.mcnmr.utilities.extension.visibleOrGoneIf
import com.letsconnect.ui.chat.ChatActivity
import com.letsconnect.ui.dashboard.DashboardVM
import com.letsconnect.ui.dashboard.adapter.DiscoverPersonAdapter
import com.letsconnect.ui.detail_person.DetailPersonActivity
import com.letsconnect.ui.discover_role.DiscoverRoleActivity
import com.letsconnect.ui.discover_subrole.DiscoverSubroleActivity
import kotlinx.android.synthetic.main.fragment_client_discover.*
import org.jetbrains.anko.startActivity

class FragmentClientDiscover : Fragment() {
    private val vm by lazy { (requireActivity() as BaseActivity)
        .instantiateViewModel<DashboardVM>() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_client_discover, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        vm.showLoadingDiscoverPersonEvent.observe(viewLifecycleOwner, Observer {
            if(it){
                shimmerDiscover.startShimmer()
            }else {
                shimmerDiscover.stopShimmer()
            }
            rvDiscoverPerson.visibleOrGoneIf(!it)
            shimmerDiscover.visibleOrGoneIf(it)
        })
        vm.discoverPersonResponse.observe(viewLifecycleOwner, Observer {
            (rvDiscoverPerson.adapter as DiscoverPersonAdapter).setNewData(it.data)
        })

        rvDiscoverPerson.layoutManager = LinearLayoutManager(requireContext())
        rvDiscoverPerson.adapter = DiscoverPersonAdapter(requireContext(), mutableListOf(),
            onPersonSelected = { person, isAutoPlay ->
                requireContext().startActivity<DetailPersonActivity>(
                    DetailPersonActivity.PERSON_ARGUMENT to person,
                    DetailPersonActivity.AUTO_PLAY_ARGUMENT to isAutoPlay
                )
            },
            onChatClicked = {
                requireContext().startActivity<ChatActivity>(
                    ChatActivity.INTERLOCUTOR_ARGUMENT to it
                )
            },
            onNextClicked = {
                requireContext().startActivity<DiscoverRoleActivity>(
                    DiscoverRoleActivity.ROLE_ARGUMENT to it.role.toString()
                )
            },
            onSubroleClicked = {
                requireContext().startActivity<DiscoverSubroleActivity>(
                    DiscoverSubroleActivity.ROLE_ARGUMENT to it.detail.roleId,
                    DiscoverSubroleActivity.SUBROLE_ARGUMENT to it.detail.typeNamerole
                )
            }
        )

        etSearch.textChanges().subscribe{ vm.executeDiscover(it.toString()) }
    }
}