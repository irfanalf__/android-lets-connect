package com.letsconnect.ui.dashboard.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.facebook.shimmer.ShimmerFrameLayout
import com.letsconnect.BuildConfig
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.custom_view.ProfilePicture
import com.letsconnect.extension.*
import com.letsconnect.repository.local.room.custom_row.InterlocutorMessagesRow
import com.letsconnect.repository.local.room.entities.Interlocutor
import com.mcnmr.utilities.extension.*
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.row_chat.view.*

class InterlocutorMessagesAdapter(private val context: Context,
                                  private var list: List<InterlocutorMessagesRow>,
                                  private val isClient: Boolean,
                                  private val onTyping: LiveData<HashMap<String, Long>>,
                                  private val onSelected: (Interlocutor) -> Unit):
    RecyclerView.Adapter<InterlocutorMessagesAdapter.InterlocutorMessagesVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InterlocutorMessagesVH =
        InterlocutorMessagesVH(LayoutInflater.from(context).inflate(R.layout.row_chat, parent, false))

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: InterlocutorMessagesVH, position: Int) {
        fun displayLastChat(isTyping: Boolean){
            if(isTyping){
                holder.tvLastChat.text = context.getString(R.string.chat_typing)
                holder.tvLastChat.setTextColor(context.getCompatColor(R.color.greenae60))
            }else {
                holder.tvLastChat.text = list[position].conversation?.hexDecrypt(
                    method = BuildConfig.ENCRYPTION_METHOD,
                    algorithm = BuildConfig.ENCRYPTION_ALGORITHM,
                    ivSeparator = BuildConfig.ENCRYPTION_IV_DELIMITER,
                    key = BuildConfig.ENCRYPTION_KEY
                )
                if(list[position].isFromThemAndRead()){
                    holder.tvLastChat.setTextColor(context.getCompatColor(R.color.grey9595))
                }else {
                    holder.tvLastChat.setTextColor(context.getCompatColor(android.R.color.black))
                }
            }
        }

        holder.profilePicture.setImage(isClient, list[position])

        holder.tvName.text = list[position].fullname
        holder.tvLastChatTime.text = list[position].sendAt?.toLong()
            .toDate("dd MMM yyyy, HH:mm")
        holder.tvUnreadCount.text = list[position].unreadCount.toString()
        holder.llUnreadCount.visibleOrInvisibleIf(list[position].unreadCount > 0)
        displayLastChat(false)

        onTyping.observe((context as BaseActivity), Observer {
            it[list[position].id.toString()]?.doIf(
                ifNotNull = { time ->
                    if(System.currentTimeMillis().minus(time) < 10000){
                        interval(context, future = 10000, onFinish = {
                            displayLastChat(false)
                        })
                        displayLastChat(true)
                    }else {
                        displayLastChat(false)
                    }
                },
                ifNull = { displayLastChat(false) }
            )
        })

        holder.clContainer.setOnClickListener { onSelected(Interlocutor().apply {
            id = list[position].id
            email = list[position].email
            fullname = list[position].fullname
            image = list[position].image
            username = list[position].username
        }) }
    }

    fun newData(list: List<InterlocutorMessagesRow>){
        this.list = list
        notifyDataSetChanged()
    }

    inner class InterlocutorMessagesVH(v: View): RecyclerView.ViewHolder(v){
        val clContainer: ConstraintLayout = v.clContainer
        val profilePicture: ProfilePicture = v.profilePicture
        val tvName: TextView = v.tvName
        val tvLastChatTime: TextView = v.tvLastChatTime
        val tvLastChat: TextView = v.tvLastChat
        val llUnreadCount: LinearLayout = v.llUnreadCount
        val tvUnreadCount: TextView = v.tvUnreadCount
    }
}