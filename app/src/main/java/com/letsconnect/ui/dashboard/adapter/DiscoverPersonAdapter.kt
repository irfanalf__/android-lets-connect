package com.letsconnect.ui.dashboard.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.letsconnect.R
import com.letsconnect.repository.network.response.DiscoverPersonResponse
import com.letsconnect.repository.local.room.entities.Interlocutor
import kotlinx.android.synthetic.main.row_discover_person.view.*

class DiscoverPersonAdapter(private val context: Context,
                            private var list: List<DiscoverPersonResponse.Data>, 
                            private val onPersonSelected: (person: DiscoverPersonResponse.Data.Person,
                                                           isAutoPlayIntroVideo: Boolean) -> Unit,
                            private val onChatClicked: (Interlocutor) -> Unit,
                            private val onNextClicked: (DiscoverPersonResponse.Data) -> Unit,
                            private val onSubroleClicked: (DiscoverPersonResponse.Data.Person) -> Unit):
    RecyclerView.Adapter<DiscoverPersonAdapter.DiscoverPersonVH>(){

    fun setNewData(list: List<DiscoverPersonResponse.Data>){
        this.list = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiscoverPersonVH =
        DiscoverPersonVH(LayoutInflater.from(context)
            .inflate(R.layout.row_discover_person, parent, false))

    override fun getItemCount(): Int = list.size
    override fun getItemViewType(position: Int): Int = position

    override fun onBindViewHolder(holder: DiscoverPersonVH, position: Int) {
        holder.tvMessage.text = list[position].message

        holder.rvPerson.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        holder.rvPerson.adapter = PersonAdapter(context, list[position].persons, 
            onPersonSelected, onChatClicked, onSubroleClicked)

        holder.llNext.setOnClickListener { onNextClicked.invoke(list[position]) }
    }

    inner class DiscoverPersonVH(v: View): RecyclerView.ViewHolder(v){
        val tvMessage: TextView = v.tvMessage
        val rvPerson: RecyclerView = v.rvPerson
        val llNext: LinearLayout = v.llNext
    }
}