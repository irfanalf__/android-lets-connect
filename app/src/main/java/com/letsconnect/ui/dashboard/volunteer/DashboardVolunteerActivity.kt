package com.letsconnect.ui.dashboard.volunteer

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.bumptech.glide.Glide
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.glideCallback
import com.mcnmr.utilities.extension.gone
import com.letsconnect.extension.instantiateViewModel
import com.letsconnect.ui.dashboard.DashboardVM
import com.letsconnect.ui.profile.ProfileActivity
import kotlinx.android.synthetic.main.activity_dashboard_client.*
import kotlinx.android.synthetic.main.activity_dashboard_volunteer.*
import kotlinx.android.synthetic.main.activity_dashboard_volunteer.bottomNavView
import kotlinx.android.synthetic.main.activity_dashboard_volunteer.toolbar
import kotlinx.android.synthetic.main.activity_dashboard_volunteer.tvTitle
import org.jetbrains.anko.startActivity

class DashboardVolunteerActivity: BaseActivity(), NavController.OnDestinationChangedListener {
    private val vm by lazy { instantiateViewModel<DashboardVM>() }

    override fun shouldShowLoading(tag: Any) = when(tag){
        DashboardVM.GET_DISCOVER_PERSON -> vm.showLoadingDiscoverPersonEvent.value = true
        else -> super.shouldShowLoading(tag)
    }

    override fun shouldHideLoading(tag: Any) = when(tag){
        DashboardVM.GET_DISCOVER_PERSON -> vm.showLoadingDiscoverPersonEvent.value = false
        else -> super.shouldHideLoading(tag)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard_volunteer)
        setSupportActionBar(toolbar)

        bottomNavView.setupWithNavController(findNavController(R.id.navHostFragment).apply {
            addOnDestinationChangedListener(this@DashboardVolunteerActivity)
        })

        preference.getUser()?.let {
            vm.connectSocket(it.user.id)
            Glide.with(this).load(it.user.image)
                .listener(glideCallback(onFinish = {
                    shimmerCivProfile.stopShimmer()
                    shimmerCivProfile.gone()
                }))
                .error(R.drawable.default_image_profile)
                .into(civProfile)
        }

        civProfile.setOnClickListener { startActivity<ProfileActivity>() }
    }

    override fun onDestinationChanged(controller: NavController, destination: NavDestination,
                                      arguments: Bundle?) {
        when(destination.id){
            R.id.fragmentDashboardMessages -> tvTitle.text = getString(R.string.bottom_menu_messages)
            R.id.fragmentDashboardHelp -> tvTitle.text = getString(R.string.bottom_menu_help)
        }
    }
}