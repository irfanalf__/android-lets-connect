package com.letsconnect.ui.dashboard.fragment

import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.buildSpannedString
import androidx.core.text.color
import androidx.fragment.app.Fragment
import com.letsconnect.R
import com.mcnmr.utilities.extension.getCompatColor
import kotlinx.android.synthetic.main.fragment_dashboard_help.*
import org.jetbrains.anko.clickable

class FragmentDashboardHelp: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_dashboard_help, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val span = SpannableString(getString(R.string.dashboard_help))
        span.setSpan(object: ClickableSpan(){
            override fun onClick(p0: View) {
                val to = getString(R.string.dashboard_help_email)
                val subject = getString(R.string.dashboard_default_subject)
                val message = getString(R.string.dashboard_default_message)

                val intent = Intent(Intent.ACTION_SEND)
                val addressees = arrayOf(to)
                intent.putExtra(Intent.EXTRA_EMAIL, addressees)
                intent.putExtra(Intent.EXTRA_SUBJECT, subject)
                intent.putExtra(Intent.EXTRA_TEXT, message)
                intent.type = "message/rfc822"
                startActivity(Intent.createChooser(intent, getString(R.string.dashboard_send_email_chooser)))
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
                ds.color = getCompatColor(R.color.red5757)
            }
        }, 248, 278, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        /**val span = buildSpannedString {
            append(getString(R.string.dashboard_help1))
            color(getCompatColor(R.color.red5757)){
                append(getString(R.string.dashboard_help2))
            }.clickable {

            }
            append(getString(R.string.dashboard_help3))
        }*/

        tvMessage.text = span
        tvMessage.movementMethod = LinkMovementMethod.getInstance()
    }
}