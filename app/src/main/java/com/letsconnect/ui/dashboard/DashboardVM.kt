package com.letsconnect.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.viewModelScope
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.ancestors.BaseViewModel
import com.letsconnect.repository.local.LocalRepository
import com.letsconnect.repository.network.request.RemoteRepository
import com.letsconnect.repository.network.response.DiscoverPersonResponse
import com.letsconnect.repository.socket.SocketRepository
import com.letsconnect.repository.socket.parameter.AskLatestMessageStatusParams
import com.letsconnect.repository.socket.parameter.ImReadMessageParams
import com.letsconnect.repository.socket.parameter.ImReceivingMessageParams
import com.letsconnect.repository.socket.parameter.SendMessageParams
import com.letsconnect.repository.socket.response.SomeoneSendMessageResponse
import com.letsconnect.repository.socket.response.TargetReadYourMessageResponse
import com.letsconnect.repository.socket.response.TargetReceivingYourMessageResponse
import com.letsconnect.repository.local.room.custom_row.InterlocutorMessagesRow
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

class DashboardVM(activity: BaseActivity): BaseViewModel(activity) {
    companion object {
        const val GET_DISCOVER_PERSON = "GET_DISCOVER_PERSON"
    }

    @Inject
    lateinit var remoteRepository: RemoteRepository

    @Inject
    lateinit var socketRepository: SocketRepository

    @Inject
    lateinit var localRepository: LocalRepository

    val showLoadingDiscoverPersonEvent = MutableLiveData<Boolean>()
    val discoverPersonResponse = MutableLiveData<DiscoverPersonResponse>()

    private val onFirstConnectObs: Observer<Void>
    private val onConnectObs: Observer<Void>
    private val onSomeoneSendMessageObs: Observer<SomeoneSendMessageResponse>
    private val onTargetReceivingYourMessagObs: Observer<TargetReceivingYourMessageResponse>
    private val onTargetReadYourMessageObs: Observer<TargetReadYourMessageResponse>

    val interlocutorMessagesRows: LiveData<List<InterlocutorMessagesRow>>

    private var discoverJob: Job? = null

    init {
        app.consumer.inject(this)

        interlocutorMessagesRows = localRepository.watchInterlocutorMessages()

        onFirstConnectObs = Observer { viewModelScope.launch {
            localRepository.getMyUndeliveredMessage().forEach {
                socketRepository.sendMessage(
                    params = SendMessageParams(
                        conversationId = it.conversationId,
                        targetId = it.interlocutorId,
                        conversation = it.conversation,
                        replyTo = it.replyTo,
                        sendAt = it.sendAt
                    ),
                    callback = {
                        viewModelScope.launch { localRepository.serverReceivingMessage(it) }
                    }
                )
            }
        }}
        onConnectObs = Observer { viewModelScope.launch {
            val data = mutableListOf<AskLatestMessageStatusParams.Message>()
            localRepository.getMyUnreadUnsentMessage().forEach {
                data.add(AskLatestMessageStatusParams.Message(it.conversationId))
            }

            if(data.isNotEmpty()){
                socketRepository.askLatestMessageStatus(AskLatestMessageStatusParams(data)){
                    viewModelScope.launch { localRepository.updateMessageWithLatestStatus(it) }
                }
            }

            socketRepository.askForDelayedMessage { viewModelScope.launch {
                localRepository.receiveDelayedMessage(it)
                kotlin.run stopIfInSameRoom@{
                    it.data.forEach {
                        if(socketRepository.isActiveInRoom(it.sender.id)){
                            localRepository.iReadMessageOfThisInterlocutor(it.sender.id)
                            socketRepository.imReadMessage(ImReadMessageParams(it.sender.id))
                            return@stopIfInSameRoom
                        }
                    }
                }
            }}
        }}
        onSomeoneSendMessageObs = Observer { viewModelScope.launch {
            localRepository.receivingMessage(it)
            socketRepository.imReceivingMessage(
                ImReceivingMessageParams(it.conversationId,
                it.sender.id)
            )
            if(socketRepository.isActiveInRoom(it.sender.id)){
                localRepository.iReadMessageOfThisInterlocutor(it.sender.id)
                socketRepository.imReadMessage(ImReadMessageParams(it.sender.id))
            }
        }}
        onTargetReceivingYourMessagObs = Observer { viewModelScope.launch {
            localRepository.targetReceivingMyMessage(it)
        }}
        onTargetReadYourMessageObs = Observer { viewModelScope.launch {
            localRepository.targetReadMyMessage(it)
        }}

        socketRepository.onFirstConnect.observeForever(onFirstConnectObs)
        socketRepository.onConnect.observeForever(onConnectObs)
        socketRepository.onSomeoneSendMessage.observeForever(onSomeoneSendMessageObs)
        socketRepository.onTargetReceivingYourMessage.observeForever(onTargetReceivingYourMessagObs)
        socketRepository.onTargetReadYourMessage.observeForever(onTargetReadYourMessageObs)
    }

    fun connectSocket(idUser: Int){
        socketRepository.connect(idUser)
    }

    fun executeDiscover(query: String = ""){
        discoverJob?.cancel()
        discoverJob = viewModelScope.launch {
            activity.shouldShowLoading(GET_DISCOVER_PERSON)
            val result = safeApiCall(GET_DISCOVER_PERSON) { remoteRepository.discoverPerson(query) }
            activity.shouldHideLoading(GET_DISCOVER_PERSON)
            result.consume(
                onErrorResponse = activity::onErrorResponse,
                onHttpError = activity::onHttpError,
                onNetworkError = activity::onNetworkError,
                onTimeoutError = activity::onTimeoutError,
                onUnknownError = activity::onUnknownError,
                onSuccess = { discoverPersonResponse.value = it }
            )
        }
    }

    override fun onCleared() {
        socketRepository.onFirstConnect.removeObserver(onFirstConnectObs)
        socketRepository.onConnect.removeObserver(onConnectObs)
        socketRepository.onSomeoneSendMessage.removeObserver(onSomeoneSendMessageObs)
        socketRepository.onTargetReceivingYourMessage.removeObserver(onTargetReceivingYourMessagObs)
        socketRepository.onTargetReadYourMessage.removeObserver(onTargetReadYourMessageObs)
        super.onCleared()
    }
}