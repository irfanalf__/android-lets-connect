package com.letsconnect.ui.dashboard.client

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.bumptech.glide.Glide
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.glideCallback
import com.mcnmr.utilities.extension.gone
import com.letsconnect.extension.instantiateViewModel
import com.letsconnect.ui.dashboard.DashboardVM
import com.letsconnect.ui.profile.ProfileActivity
import kotlinx.android.synthetic.main.activity_dashboard_client.*
import org.jetbrains.anko.startActivity

class DashboardClientActivity : BaseActivity(), NavController.OnDestinationChangedListener {
    private val vm by lazy { instantiateViewModel<DashboardVM>() }

    override fun shouldShowLoading(tag: Any) = when(tag){
        DashboardVM.GET_DISCOVER_PERSON -> vm.showLoadingDiscoverPersonEvent.value = true
        else -> super.shouldShowLoading(tag)
    }

    override fun shouldHideLoading(tag: Any) = when(tag){
        DashboardVM.GET_DISCOVER_PERSON -> vm.showLoadingDiscoverPersonEvent.value = false
        else -> super.shouldHideLoading(tag)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard_client)
        setSupportActionBar(toolbar)

        bottomNavView.setupWithNavController(findNavController(R.id.navHostFragment).apply {
            addOnDestinationChangedListener(this@DashboardClientActivity)
        })

        preference.getUser()?.let {
            vm.connectSocket(it.user.id)
            profilePicture.setImage(it)
        }

        profilePicture.setOnClickListener { startActivity<ProfileActivity>() }
    }

    override fun onDestinationChanged(controller: NavController, destination: NavDestination,
                                      arguments: Bundle?) {
        when(destination.id){
            R.id.fragmentClientDiscover -> tvTitle.text = getString(R.string.bottom_menu_discover)
            R.id.fragmentDashboardMessages -> tvTitle.text = getString(R.string.bottom_menu_messages)
            R.id.fragmentDashboardHelp -> tvTitle.text = getString(R.string.bottom_menu_help)
        }
    }
}