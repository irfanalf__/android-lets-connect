package com.letsconnect.ui.dashboard.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.instantiateViewModel
import com.letsconnect.model.Role
import com.letsconnect.repository.local.room.entities.Interlocutor
import com.letsconnect.ui.chat.ChatActivity
import com.letsconnect.ui.dashboard.DashboardVM
import com.letsconnect.ui.dashboard.adapter.InterlocutorMessagesAdapter
import kotlinx.android.synthetic.main.fragment_dashboard_messages.*
import org.jetbrains.anko.startActivity

class FragmentDashboardMessages : Fragment() {
    private val vm by lazy { (requireActivity() as BaseActivity)
        .instantiateViewModel<DashboardVM>() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_dashboard_messages, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rvInterlocutorMessages.layoutManager = LinearLayoutManager(requireContext())
        rvInterlocutorMessages.adapter = InterlocutorMessagesAdapter(
            context = requireContext(),
            list = mutableListOf(),
            isClient = (requireActivity() as BaseActivity).preference.getUser()?.user?.roleId == Role.ROLE_CLIENT,
            onTyping = vm.socketRepository.onSomeoneTyping,
            onSelected = ::onChatSelected)

        vm.interlocutorMessagesRows.observe(viewLifecycleOwner, Observer{
            (rvInterlocutorMessages.adapter as InterlocutorMessagesAdapter).newData(it)
        })
    }

    private fun onChatSelected(interlocutor: Interlocutor){
        (requireActivity() as BaseActivity)
            .startActivity<ChatActivity>(ChatActivity.INTERLOCUTOR_ARGUMENT to interlocutor)
    }
}