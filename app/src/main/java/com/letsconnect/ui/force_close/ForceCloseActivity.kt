package com.letsconnect.ui.force_close

import android.os.Bundle
import androidx.lifecycle.Observer
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.instantiateViewModel
import com.mcnmr.utilities.extension.gone
import com.mcnmr.utilities.extension.visible
import com.mcnmr.utilities.internal_plugin.StringIntent
import kotlinx.android.synthetic.main.activity_force_close.*

class ForceCloseActivity: BaseActivity() {
    companion object{
        const val ERROR_MESSAGE_ARGUMENT = "ERROR_MESSAGE_ARGUMENT"
    }

    private val vm by lazy { instantiateViewModel<ForceCloseVM>() }

    @StringIntent(ERROR_MESSAGE_ARGUMENT)
    lateinit var errorMessageArgument: String

    override fun shouldCatchForceClose(): Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_force_close)

        vm.sendErrorReportResponse.observe(this, Observer {
            btnSendError.gone()
            tvErrorSent.visible()
        })

        btnSendError.setOnClickListener {
            vm.executeSendErrorReport(previousActivity(), errorMessageArgument)
        }
    }

}