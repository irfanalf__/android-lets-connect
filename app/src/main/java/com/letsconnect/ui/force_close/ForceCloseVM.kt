package com.letsconnect.ui.force_close

import android.os.Build
import androidx.lifecycle.viewModelScope
import com.letsconnect.BuildConfig
import com.letsconnect.MainApplication
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.ancestors.BaseResponse
import com.letsconnect.ancestors.BaseViewModel
import com.letsconnect.repository.network.parameter.ErrorReportParams
import com.letsconnect.repository.network.request.RemoteRepository
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import kotlinx.coroutines.launch
import javax.inject.Inject

class ForceCloseVM(activity: BaseActivity): BaseViewModel(activity) {
    companion object {
        const val REQUEST_SEND_ERROR_REPORT = "REQUEST_SEND_ERROR_REPORT"
    }

    @Inject
    lateinit var remoteRepository: RemoteRepository

    val sendErrorReportResponse = SingleEventWrapper<BaseResponse>()

    init { (activity.application as MainApplication).consumer.inject(this) }

    fun executeSendErrorReport(fromActivity: String, errorMessage: String){
        val params = ErrorReportParams(Build.MODEL, Build.ID, Build.MANUFACTURER, Build.TYPE,
            Build.USER, Build.VERSION_CODES.BASE, Build.VERSION.INCREMENTAL, Build.VERSION.SDK_INT,
            Build.BOARD, Build.BRAND, Build.HOST, Build.FINGERPRINT, Build.VERSION.RELEASE,
            BuildConfig.VERSION_CODE, BuildConfig.VERSION_NAME, BuildConfig.BUILD_TYPE,
            fromActivity, errorMessage, activity.preference.getUser()?.user?.id ?: -1,
            System.currentTimeMillis()
        )

        viewModelScope.launch {
            activity.shouldShowLoading(REQUEST_SEND_ERROR_REPORT)
            val result = safeApiCall(REQUEST_SEND_ERROR_REPORT){ remoteRepository.reportError(params) }
            activity.shouldHideLoading(REQUEST_SEND_ERROR_REPORT)
            result.consume(
                onErrorResponse = activity::onErrorResponse,
                onHttpError = activity::onHttpError,
                onNetworkError = activity::onNetworkError,
                onTimeoutError = activity::onTimeoutError,
                onUnknownError = activity::onUnknownError,
                onSuccess = { sendErrorReportResponse.value = it }
            )
        }
    }
}