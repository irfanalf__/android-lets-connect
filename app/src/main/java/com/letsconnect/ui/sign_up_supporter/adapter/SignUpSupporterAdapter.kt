package com.letsconnect.ui.sign_up_supporter.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.letsconnect.repository.network.response.RegistrationDataResponse
import com.letsconnect.ui.sign_up_supporter.fragment.FragmentSignUpSupporterPage1
import com.letsconnect.ui.sign_up_supporter.fragment.FragmentSignUpSupporterPage2
import com.letsconnect.ui.sign_up_supporter.fragment.FragmentSignUpSupporterPage3
import com.letsconnect.ui.sign_up_supporter.fragment.FragmentSignUpSupporterPage4
import com.letsconnect.ui.sign_up_supporter.fragment.FragmentSignUpSupporterPage5

class SignUpSupporterAdapter(fm: FragmentManager, response: RegistrationDataResponse):
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val page1 = FragmentSignUpSupporterPage1()
    private val page2 = FragmentSignUpSupporterPage2.newInstance(response.location, response.primaryLocation)
    private val page3 = FragmentSignUpSupporterPage3()
    private val page4 = FragmentSignUpSupporterPage4.newInstance(response.specialize)
    private val page5 = FragmentSignUpSupporterPage5()

    override fun getItem(position: Int): Fragment = when(position){
        0 -> page1
        1 -> page2
        2 -> page3
        3 -> page4
        else -> page5
    }

    override fun getCount(): Int = 5
}