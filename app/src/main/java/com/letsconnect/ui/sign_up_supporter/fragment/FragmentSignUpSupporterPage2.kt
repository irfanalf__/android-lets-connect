package com.letsconnect.ui.sign_up_supporter.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.jakewharton.rxbinding4.widget.checkedChanges
import com.jakewharton.rxbinding4.widget.itemSelections
import com.jakewharton.rxbinding4.widget.textChanges
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.instantiateViewModel
import com.letsconnect.repository.network.response.RegistrationDataResponse
import com.letsconnect.ui.sign_up_supporter.SignUpSupporterActivity
import com.letsconnect.ui.sign_up_supporter.SignUpSupporterVM
import com.mcnmr.utilities.extension.getCompatColor
import kotlinx.android.synthetic.main.fragment_sign_up_supporter_page2.*

@Suppress("UNCHECKED_CAST")
class FragmentSignUpSupporterPage2 private constructor(): Fragment() {
    companion object {
        fun newInstance(location: ArrayList<RegistrationDataResponse.Location>,
                        primaryLocation: ArrayList<RegistrationDataResponse.Location>): FragmentSignUpSupporterPage2 =
            FragmentSignUpSupporterPage2().apply {
                arguments = Bundle().apply {
                    putSerializable(SignUpSupporterActivity.LOCATION_DATA, location)
                    putSerializable(SignUpSupporterActivity.PRIMARY_LOCATION_DATA, primaryLocation)
                }
            }
    }

    private val vm by lazy { (requireActivity() as BaseActivity).instantiateViewModel<SignUpSupporterVM>() }
    private val genders by lazy { arrayOf(getString(R.string.signup_male), getString(R.string.signup_female), getString(R.string.signup_non_binary)) }
    private lateinit var primaryLocation: List<RegistrationDataResponse.Location>
    private lateinit var location: List<RegistrationDataResponse.Location>

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.let {
            location = it.getSerializable(SignUpSupporterActivity.LOCATION_DATA) as
                    ArrayList<RegistrationDataResponse.Location>
            primaryLocation = it.getSerializable(SignUpSupporterActivity.PRIMARY_LOCATION_DATA) as
                    ArrayList<RegistrationDataResponse.Location>
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_sign_up_supporter_page2, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val locations = mutableListOf<RegistrationDataResponse.Location>()
        locations.addAll(primaryLocation)
        locations.addAll(location)

        spGender.adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, genders)
        spLocation.adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, locations)

        vm.step2Validation.observe(viewLifecycleOwner, Observer {
            cvForward.isEnabled = it
            cvForward.setCardBackgroundColor(getCompatColor(if(it) android.R.color.black else
                android.R.color.darker_gray))
        })

        etSkype.textChanges().subscribe { vm.skypeData.value = it.toString() }
        spGender.itemSelections().subscribe { vm.genderData.value = genders[it] }
        spLocation.itemSelections().subscribe { vm.locationData.value = location[it] }
        cbAdults.checkedChanges().subscribe { vm.workWithAdultsData.value = it }
        cbAdolescents.checkedChanges().subscribe { vm.workWithAdolescentsData.value = it }
        cbCouples.checkedChanges().subscribe { vm.workWithCouplesData.value = it }
        cbLiveChat.checkedChanges().subscribe { vm.isLiveChatData.value = it }
        cbVideoCall.checkedChanges().subscribe { vm.isVideoCallData.value = it }
        cbVoiceCall.checkedChanges().subscribe { vm.isVoiceCallData.value = it }

        cvForward.setOnClickListener { vm.gotoStep3Event.trigger() }
    }
}