package com.letsconnect.ui.sign_up_supporter.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.letsconnect.R
import com.letsconnect.repository.network.response.RegistrationDataResponse
import kotlinx.android.synthetic.main.row_specialization.view.*

class SpecializationAdapter(private val context: Context,
                            private val specialization: List<RegistrationDataResponse.Specialize>,
                            private val specializeLiveData: MutableLiveData<MutableList<RegistrationDataResponse.Specialize>>): RecyclerView.Adapter<SpecializationAdapter.SpecializationVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpecializationVH =
        SpecializationVH(LayoutInflater.from(context).inflate(R.layout.row_specialization, parent, false))

    override fun getItemCount(): Int = specialization.size
    override fun getItemViewType(position: Int): Int = position

    override fun onBindViewHolder(holder: SpecializationVH, position: Int) {
        holder.cbSpecialization.text = specialization[position].specializeName
        holder.cbSpecialization.setOnCheckedChangeListener { _, it ->
            specialization[position].isSelected = it

            val list = specializeLiveData.value ?: mutableListOf()
            if(it){
                list.add(specialization[position])
            }else {
                list.remove(specialization[position])
            }
            specializeLiveData.value = list

            notifyDataSetChanged()
        }
    }

    inner class SpecializationVH(v: View): RecyclerView.ViewHolder(v){
        val cbSpecialization: AppCompatCheckBox = v.cbSpecialization
    }
}