package com.letsconnect.ui.sign_up_supporter.fragment

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.jakewharton.rxbinding4.widget.textChanges
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.instantiateViewModel
import com.letsconnect.ui.sign_up_supporter.SignUpSupporterVM
import com.mcnmr.utilities.extension.getCompatColor
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import droidninja.filepicker.utils.ContentUriUtils
import kotlinx.android.synthetic.main.fragment_sign_up_supporter_page3.*
import java.io.File

class FragmentSignUpSupporterPage3: Fragment() {
    companion object {
        const val IMAGE_PICKER_REQUEST_CODE = 0
    }
    private val vm by lazy { (requireActivity() as BaseActivity).instantiateViewModel<SignUpSupporterVM>() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_sign_up_supporter_page3, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        vm.readStoragePermissionCopiesCertResult.observe(viewLifecycleOwner, Observer { granted ->
            if(granted){
                FilePickerBuilder.instance
                    .setMaxCount(1)
                    .pickPhoto(this, IMAGE_PICKER_REQUEST_CODE)
            }
        })
        vm.copiesCertificationData.observe(viewLifecycleOwner, Observer {
            etCopiesCertification.setText(it.path)
        })
        vm.step3Validation.observe(viewLifecycleOwner, Observer {
            cvForward.isEnabled = it
            cvForward.setCardBackgroundColor(getCompatColor(if(it) android.R.color.black else
                android.R.color.darker_gray))
        })

        etAboutMe.textChanges().subscribe { vm.aboutMeData.value = it.toString() }
        etLicensing.textChanges().subscribe { vm.licensingData.value = it.toString() }
        etCertification.textChanges().subscribe { vm.certificationData.value = it.toString() }

        btnBrowse.setOnClickListener { vm.readStoragePermissionCopiesCertEvent.trigger() }
        cvForward.setOnClickListener { vm.gotoStep4Event.trigger() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            FragmentSignUpSupporterPage1.IMAGE_PICKER_REQUEST_CODE -> {
                if(resultCode == Activity.RESULT_OK){
                    data?.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_MEDIA)?.let {
                        ContentUriUtils.getFilePath(requireContext(), it[0])?.let { path ->
                            vm.copiesCertificationData.value = File(path)
                        }
                    }
                }
            }
        }
    }
}