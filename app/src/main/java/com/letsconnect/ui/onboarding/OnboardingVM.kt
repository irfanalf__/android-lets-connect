package com.letsconnect.ui.onboarding

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.ancestors.BaseViewModel
import com.letsconnect.extension.NonNullLiveData
import com.letsconnect.repository.network.request.RemoteRepository
import com.letsconnect.repository.network.response.DiscoverPersonResponse
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

class OnboardingVM(activity: BaseActivity): BaseViewModel(activity) {
    companion object {
        const val GET_DISCOVER_PERSON = "GET_DISCOVER_PERSON"
    }

    @Inject
    lateinit var remoteRepository: RemoteRepository

    val onDismissData = NonNullLiveData(false)

    val showLoadingDiscoverPersonEvent = MutableLiveData<Boolean>()
    val discoverPersonResponse = MutableLiveData<DiscoverPersonResponse>()

    private var discoverJob: Job? = null

    init { app.consumer.inject(this) }

    fun executeDiscover(query: String = ""){
        discoverJob?.cancel()
        discoverJob = viewModelScope.launch {
            activity.shouldShowLoading(GET_DISCOVER_PERSON)
            val result = safeApiCall(GET_DISCOVER_PERSON) { remoteRepository.discoverPerson(query) }
            activity.shouldHideLoading(GET_DISCOVER_PERSON)
            result.consume(
                onErrorResponse = activity::onErrorResponse,
                onHttpError = activity::onHttpError,
                onNetworkError = activity::onNetworkError,
                onTimeoutError = activity::onTimeoutError,
                onUnknownError = activity::onUnknownError,
                onSuccess = { discoverPersonResponse.value = it }
            )
        }
    }
}