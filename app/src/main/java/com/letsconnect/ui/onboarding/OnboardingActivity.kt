package com.letsconnect.ui.onboarding

import android.os.Bundle
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.behavior.SwipeDismissBehavior
import com.jakewharton.rxbinding4.widget.textChanges
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.custom_dialog.SignUpDialog
import com.letsconnect.extension.*
import com.letsconnect.ui.dashboard.DashboardVM
import com.letsconnect.ui.dashboard.adapter.DiscoverPersonAdapter
import com.letsconnect.ui.detail_person.DetailPersonActivity
import com.letsconnect.ui.discover_role.DiscoverRoleActivity
import com.letsconnect.ui.discover_subrole.DiscoverSubroleActivity
import com.letsconnect.ui.login.LoginActivity
import com.mcnmr.utilities.extension.*
import kotlinx.android.synthetic.main.activity_onboarding.*
import org.jetbrains.anko.startActivity

class OnboardingActivity : BaseActivity() {
    private val vm by lazy { instantiateViewModel<OnboardingVM>() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)

        vm.showLoadingDiscoverPersonEvent.observe(this, Observer {
            if(it){
                shimmerDiscover.startShimmer()
            }else {
                shimmerDiscover.stopShimmer()
            }
            rvDiscoverPerson.visibleOrGoneIf(!it)
            shimmerDiscover.visibleOrGoneIf(it)
        })
        vm.discoverPersonResponse.observe(this, Observer {
            (rvDiscoverPerson.adapter as DiscoverPersonAdapter).setNewData(it.data)
        })
        vm.onDismissData.observe(this, Observer { clOnboarding.visibleOrGoneIf(!it) })

        (clOnboarding.layoutParams as CoordinatorLayout.LayoutParams)
            .behavior = object: SwipeDismissBehavior<ConstraintLayout>(){
            override fun canSwipeDismissView(view: View): Boolean = view != clContent
        }.apply { listener = object: SwipeDismissBehavior.OnDismissListener{
            override fun onDismiss(view: View?) { vm.onDismissData.value = true }
            override fun onDragStateChanged(state: Int) {}
        } }

        rvDiscoverPerson.layoutManager = LinearLayoutManager(this)
        rvDiscoverPerson.adapter = DiscoverPersonAdapter(this, mutableListOf(),
            onPersonSelected = { person, isAutoPlay ->
                startActivity<DetailPersonActivity>(
                    DetailPersonActivity.PERSON_ARGUMENT to person,
                    DetailPersonActivity.AUTO_PLAY_ARGUMENT to isAutoPlay
                )
            },
            onChatClicked = {
                startActivity<LoginActivity>()
            },
            onNextClicked = {
                startActivity<DiscoverRoleActivity>(
                    DiscoverRoleActivity.ROLE_ARGUMENT to it.role.toString()
                )
            },
            onSubroleClicked = {
                startActivity<DiscoverSubroleActivity>(
                    DiscoverSubroleActivity.ROLE_ARGUMENT to it.detail.roleId,
                    DiscoverSubroleActivity.SUBROLE_ARGUMENT to it.detail.typeNamerole
                )
            }
        )

        etSearch.textChanges().subscribe{ vm.executeDiscover(it.toString()) }

        bottomNavView.setOnNavigationItemSelectedListener {
            if(it.itemId == R.id.messages){
                startActivity<LoginActivity>()
            }
            false
        }
        btnLogin.setOnClickListener { startActivity<LoginActivity>() }
        tvWebsite.setOnClickListener { launchUrl("https://letsconnectproject.com") }
    }

    override fun shouldShowLoading(tag: Any) = when(tag){
        OnboardingVM.GET_DISCOVER_PERSON -> vm.showLoadingDiscoverPersonEvent.value = true
        else -> super.shouldShowLoading(tag)
    }

    override fun shouldHideLoading(tag: Any) = when(tag){
        OnboardingVM.GET_DISCOVER_PERSON -> vm.showLoadingDiscoverPersonEvent.value = false
        else -> super.shouldHideLoading(tag)
    }

}
