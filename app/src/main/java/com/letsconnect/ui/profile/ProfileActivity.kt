package com.letsconnect.ui.profile

import android.os.Bundle
import androidx.lifecycle.Observer
import com.letsconnect.R
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.extension.instantiateViewModel
import com.letsconnect.ui.onboarding.OnboardingActivity
import com.mcnmr.utilities.extension.launchUrl
import kotlinx.android.synthetic.main.activity_profile.*
import org.jetbrains.anko.startActivity

class ProfileActivity: BaseActivity() {
    private val vm by lazy { instantiateViewModel<ProfileVM>() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        profilePicture.setImage(preference.getUser())
        tvName.text = preference.getUser()?.user?.fullname

        vm.onReadyLogout.observe(this, Observer {
            startActivity<OnboardingActivity>()
            finishAffinity()
        })

        btnEditProfile.setOnClickListener {
            preference.getUser()?.let {
                launchUrl("https://letsconnectproject.com/user/index/${it.user.id}")
            }
        }
        btnLogout.setOnClickListener { vm.logout() }
        llBack.setOnClickListener { onBackPressed() }
    }

}