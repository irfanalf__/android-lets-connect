package com.letsconnect.ui.profile

import androidx.lifecycle.viewModelScope
import com.letsconnect.MainApplication
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.ancestors.BaseViewModel
import com.letsconnect.repository.local.LocalRepository
import com.letsconnect.repository.network.request.RemoteRepository
import com.mcnmr.utilities.extension.doIf
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import kotlinx.coroutines.launch
import javax.inject.Inject

class ProfileVM(activity: BaseActivity): BaseViewModel(activity) {
    companion object{
        const val REQUEST_LOGOUT = "REQUEST_LOGOUT"
    }

    val onReadyLogout = SingleEventWrapper<Void>()

    @Inject
    lateinit var remoteRepository: RemoteRepository

    @Inject
    lateinit var localRepository: LocalRepository

    init { (activity.application as MainApplication).consumer.inject(this) }

    fun logout(){
        activity.preference.getUser().doIf(
            ifNull = {
                onReadyLogout.trigger()
            },
            ifNotNull = {
                viewModelScope.launch {
                    activity.shouldShowLoading(REQUEST_LOGOUT)
                    val result = safeApiCall(REQUEST_LOGOUT){ remoteRepository.logout(it.user.id) }
                    localRepository.truncateAll()
                    activity.preference.removeUser()
                    activity.shouldHideLoading(REQUEST_LOGOUT)
                    result.consume(
                        onErrorResponse = activity::onErrorResponse,
                        onHttpError = activity::onHttpError,
                        onNetworkError = activity::onNetworkError,
                        onTimeoutError = activity::onTimeoutError,
                        onUnknownError = activity::onUnknownError,
                        onSuccess = { onReadyLogout.trigger() }
                    )
                }
            }
        )
    }

}