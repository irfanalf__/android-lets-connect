package com.letsconnect.service.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.letsconnect.R
import com.letsconnect.ui.notification_click.NotificationClickActivity
import com.letsconnect.wrapper.NotificationWrapper

class FCMService: FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        /**if (/* Check if data needs to be processed by long running job */ true) {
            // For long-running tasks (10 seconds or more) use WorkManager.
            scheduleJob()
        } else {
            // Handle message within 10 seconds
            handleNow()
        }*/

        val channelId = getString(R.string.channel_notification_id)
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId, "Lets Connect Notifications",
                NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        NotificationWrapper.from(remoteMessage = remoteMessage).consume(
            onReceiveMessage = {
                val intent = Intent(this, NotificationClickActivity::class.java).apply {
                    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    putExtra(NotificationClickActivity.EXTRA_DATA, it)
                }

                val pendingIntent = PendingIntent.getActivity(this, it.value.senderId, intent,
                    PendingIntent.FLAG_ONE_SHOT)

                val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                val notificationBuilder = NotificationCompat.Builder(this, channelId)
                    .setSmallIcon(R.drawable.ic_logo_notification)
                    .setContentTitle(getString(R.string.notification_new_message_title))
                    .setContentText(getString(R.string.notification_new_message).format(
                        it.value.senderFullname, it.value.conversation
                    ))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_MAX)

                notificationManager.notify(it.value.senderId, notificationBuilder.build())
            }
        )
    }

    /**
     * Schedule async work using WorkManager.
     */
    /**private fun scheduleJob() {
        // [START dispatch_job]
        val work = OneTimeWorkRequest.Builder(MyWorker::class.java).build()
        WorkManager.getInstance().beginWith(work).enqueue()
        // [END dispatch_job]
    }*/

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
    }
}