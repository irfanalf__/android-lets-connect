package com.letsconnect.service.fcm.fcm_response

import com.google.gson.annotations.SerializedName
import com.letsconnect.ancestors.BaseResponse
import com.letsconnect.wrapper.NotificationWrapper
import java.io.Serializable

data class FCMReceiveMessage(
    @SerializedName("notification_type")
    val notificationType: String?,
    @SerializedName("conversation_id")
    val conversationId: String?,
    @SerializedName("receiver_id")
    val receiverId: String?,
    @SerializedName("conversation")
    val conversation: String?,
    @SerializedName("reply_to")
    val replyTo: String?,
    @SerializedName("received_by_server_at")
    val receivedByServerAt: Double?,
    @SerializedName("sender_id")
    val senderId: Int,
    @SerializedName("sender_email")
    val senderEmail: String?,
    @SerializedName("sender_fullname")
    val senderFullname: String?,
    @SerializedName("sender_image")
    val senderImage: String?,
    @SerializedName("sender_username")
    val senderUsername: String?): Serializable