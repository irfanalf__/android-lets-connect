package com.letsconnect.injection.module

import com.letsconnect.repository.local.LocalRepository
import com.letsconnect.repository.local.room.daos.ConversationDao
import com.letsconnect.repository.local.room.daos.InterlocutorDao
import com.letsconnect.repository.local.room.daos.UniversalDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class LocalRepositoryModule {
    @Singleton
    @Provides
    fun giveLocalRepository(interlocutorDao: InterlocutorDao, conversationDao: ConversationDao,
                            universalDao: UniversalDao): LocalRepository = LocalRepository(interlocutorDao,
        conversationDao, universalDao)
}