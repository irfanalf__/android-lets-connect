package com.letsconnect.injection.module

import android.content.Context

import com.google.gson.Gson
import com.letsconnect.BuildConfig
import com.letsconnect.model.Flavor

import java.util.concurrent.TimeUnit

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

@Module
open class ContextModule(
    @get:Provides
    val giveContext: Context
) {
    @get:Provides
    val giveClient: OkHttpClient
    @get:Provides
    val giveGson: Gson

    init {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = when(BuildConfig.BUILD_TYPE){
            Flavor.DEBUG -> HttpLoggingInterceptor.Level.BODY
            Flavor.RELEASE -> HttpLoggingInterceptor.Level.NONE
            Flavor.LOCAL -> HttpLoggingInterceptor.Level.BODY
            else -> HttpLoggingInterceptor.Level.BODY
        }

        this.giveClient = OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(loggingInterceptor).build()
        this.giveGson = Gson()
    }
}
