package com.letsconnect.injection.module

import com.google.gson.Gson
import com.letsconnect.BuildConfig
import com.letsconnect.model.Flavor
import com.letsconnect.repository.network.request.RemoteRepository

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
open class RetrofitModule {

    @Provides
    @Singleton
    open fun giveRetrofitBuilder(client: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .baseUrl(BuildConfig.API_URL)
            .build()
    }

    @Provides
    @Singleton
    open fun giveRemoteRepository(retrofit: Retrofit): RemoteRepository {
        return retrofit.create(RemoteRepository::class.java)
    }
}
