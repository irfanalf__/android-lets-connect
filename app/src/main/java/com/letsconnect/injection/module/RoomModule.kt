package com.letsconnect.injection.module

import android.content.Context
import androidx.room.Room
import com.letsconnect.repository.local.room.LocalDatabase
import com.letsconnect.repository.local.room.daos.ConversationDao
import com.letsconnect.repository.local.room.daos.InterlocutorDao
import com.letsconnect.repository.local.room.daos.UniversalDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {
    @Singleton
    @Provides
    fun giveLocalDatabase(context: Context): LocalDatabase {
        return synchronized(LocalDatabase::class) {
            Room.databaseBuilder(context, LocalDatabase::class.java, "localdatabase.db")
                .fallbackToDestructiveMigration().build()
        }
    }

    @Singleton
    @Provides
    fun giveInterlocutorDao(database: LocalDatabase): InterlocutorDao = database.interlocutorDao

    @Singleton
    @Provides
    fun giveConversationDao(database: LocalDatabase): ConversationDao = database.conversationDao

    @Singleton
    @Provides
    fun giveUniversalDao(database: LocalDatabase): UniversalDao = database.universalDao
}
