package com.letsconnect.injection.module

import com.letsconnect.BuildConfig
import com.letsconnect.repository.socket.SocketRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SocketRepositoryModule {

    @Singleton
    @Provides
    fun giveSocketRepository() = SocketRepository(BuildConfig.NODE_URL)

}