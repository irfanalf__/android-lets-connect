package com.letsconnect.injection.component

import com.letsconnect.MainApplication
import com.letsconnect.injection.module.*
import com.letsconnect.ui.chat.ChatVM
import com.letsconnect.ui.dashboard.DashboardVM
import com.letsconnect.ui.detail_person.DetailPersonVM
import com.letsconnect.ui.discover_role.DiscoverRoleVM
import com.letsconnect.ui.discover_subrole.DiscoverSubroleVM
import com.letsconnect.ui.force_close.ForceCloseVM
import com.letsconnect.ui.login.LoginVM
import com.letsconnect.ui.onboarding.OnboardingVM
import com.letsconnect.ui.profile.ProfileVM
import com.letsconnect.ui.sign_up_professional.SignUpProfessionalVM
import com.letsconnect.ui.sign_up_client.SignUpClientVM
import com.letsconnect.ui.sign_up_supporter.SignUpSupporterVM
import com.letsconnect.ui.sign_up_volunteer.SignUpVolunteerVM
import com.letsconnect.ui.splash_screen.SplashScreenVM
import javax.inject.Singleton

import dagger.Component

@Singleton
@Component(modules = [ ContextModule::class, RetrofitModule::class, RoomModule::class,
    LocalRepositoryModule::class, SocketRepositoryModule::class ])
interface InjectConsumer {
    fun inject(app: MainApplication)

    fun inject(vm: SplashScreenVM)
    fun inject(vm: OnboardingVM)
    fun inject(vm: SignUpClientVM)
    fun inject(vm: SignUpVolunteerVM)
    fun inject(vm: SignUpProfessionalVM)
    fun inject(vm: SignUpSupporterVM)
    fun inject(vm: LoginVM)
    fun inject(vm: DetailPersonVM)
    fun inject(vm: DashboardVM)
    fun inject(vm: ChatVM)
    fun inject(vm: DiscoverRoleVM)
    fun inject(vm: DiscoverSubroleVM)
    fun inject(vm: ProfileVM)
    fun inject(vm: ForceCloseVM)
}