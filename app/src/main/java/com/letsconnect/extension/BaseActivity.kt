package com.letsconnect.extension

import androidx.lifecycle.ViewModelProvider
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.ancestors.BaseViewModel
import com.letsconnect.factory.ChatRoomViewModelFactory
import com.letsconnect.factory.ViewModelFactory
import com.mcnmr.utilities.extension.isDenied

inline fun <reified T: BaseViewModel> BaseActivity.instantiateViewModel(): T =
    ViewModelProvider(this, ViewModelFactory(this)).get(T::class.java)
inline fun <reified T: BaseViewModel> BaseActivity.instantiateViewModel(interlocutorId: Int): T =
    ViewModelProvider(this, ChatRoomViewModelFactory(this, interlocutorId)).get(T::class.java)

fun isPermissionGranted(
    permissions: Array<out String>,
    grantResults: IntArray
) : Boolean{
    permissions.forEachIndexed { index, _ ->
        if(grantResults[index].isDenied()){
            return false
        }
    }
    return true
}