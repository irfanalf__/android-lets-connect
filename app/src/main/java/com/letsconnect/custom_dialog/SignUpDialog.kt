package com.letsconnect.custom_dialog

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.core.text.bold
import com.letsconnect.R
import com.mcnmr.utilities.extension.getString
import kotlinx.android.synthetic.main.dialog_sign_up.*

class SignUpDialog(context: Context, private val onPressed: (SignUpDialogAction) -> Unit):
    AlertDialog(context, R.style.Theme_AppCompat_Light_Dialog_Alert) {
    enum class SignUpDialogAction { CLIENT, VOLUNTEER, PROFESSIONAL, SUPPORTER }

    @SuppressLint("InflateParams")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setContentView(LayoutInflater.from(context).inflate(R.layout.dialog_sign_up, null))

        btnVolunteer.setOnClickListener {
            dismiss()
            onPressed(SignUpDialogAction.VOLUNTEER)
        }
        btnClient.setOnClickListener {
            dismiss()
            onPressed(SignUpDialogAction.CLIENT)
        }
        btnProfessional.setOnClickListener {
            dismiss()
            onPressed(SignUpDialogAction.PROFESSIONAL)
        }
        btnSupporter.setOnClickListener {
            dismiss()
            onPressed(SignUpDialogAction.SUPPORTER)
        }
    }
}