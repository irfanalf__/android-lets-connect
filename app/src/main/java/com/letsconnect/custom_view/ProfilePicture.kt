package com.letsconnect.custom_view

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.util.AttributeSet
import android.util.Log
import android.view.Gravity
import android.widget.FrameLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.facebook.shimmer.ShimmerFrameLayout
import com.letsconnect.R
import com.letsconnect.extension.glideCallback
import com.letsconnect.model.Role
import com.letsconnect.repository.local.room.custom_row.InterlocutorMessagesRow
import com.letsconnect.repository.network.response.UserResponse
import com.mcnmr.utilities.extension.*
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

class ProfilePicture: FrameLayout {

    private lateinit var civImageProfile: CircleImageView
    private lateinit var civImageText: CircleImageView
    private lateinit var tvImageText: TextView

    private lateinit var shimmerLayout: ShimmerFrameLayout
    private lateinit var civShimmer: CircleImageView

    constructor(context: Context): super(context) { init(null) }
    constructor(context: Context, attrs: AttributeSet?): super(context, attrs) { init(attrs) }
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int): super(context, attrs, defStyleAttr) { init(attrs) }
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int): super(context, attrs, defStyleAttr, defStyleRes) { init(attrs) }

    private fun init(attrs: AttributeSet?){
        civImageProfile = CircleImageView(context)
        civImageProfile.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)

        civImageText = CircleImageView(context)
        civImageText.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        civImageText.setImageDrawable(ColorDrawable(context.getCompatColor(R.color.greenae60)))

        tvImageText = TextView(context)
        tvImageText.textSize = 12.toDp(context).toFloat()
        tvImageText.setTextColor(context.getCompatColor(android.R.color.white))
        tvImageText.layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, Gravity.CENTER)

        shimmerLayout = ShimmerFrameLayout(context)
        shimmerLayout.startShimmer()
        shimmerLayout.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)

        civShimmer = CircleImageView(context)
        civShimmer.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        civShimmer.setBackgroundResource(R.drawable.bg_radius_button_shimmer_grey)

        shimmerLayout.addView(civShimmer)

        attrs?.let {
            val ta = context.obtainStyledAttributes(it, R.styleable.ProfilePicture)
            tvImageText.textSize = ta.getDimension(R.styleable.ProfilePicture_profileTextSize, 12.toDp(context).toFloat())
            ta.recycle()
        }

        addView(civImageProfile)
        addView(civImageText)
        addView(tvImageText)
        addView(shimmerLayout)
    }

    fun setImage(user: UserResponse?){
        val isClient = user?.user?.roleId == Role.ROLE_CLIENT

        user?.user?.doIf(
            ifNull = {
                civImageText.gone()
                tvImageText.gone()

                civImageProfile.visible()
                shimmerLayout.visible()

                Glide.with(this).load("empty string")
                    .listener(glideCallback(onFinish = {
                        shimmerLayout.startShimmer()
                        shimmerLayout.gone()
                    }))
                    .error(R.drawable.default_image_profile)
                    .into(civImageProfile)
            },
            ifNotNull = {
                if(isClient){
                    civImageProfile.gone()
                    shimmerLayout.gone()

                    civImageText.visible()
                    tvImageText.visible()

                    tvImageText.text = (if(it.fullname?.length ?: 0 > 1)
                        it.fullname?.substring(0, 2) else it.fullname?.substring(0, 1))
                        ?.toUpperCase(Locale.getDefault())
                }else {
                    civImageText.gone()
                    tvImageText.gone()

                    civImageProfile.visible()
                    shimmerLayout.visible()

                    Glide.with(this).load(it.image)
                        .listener(glideCallback(onFinish = {
                            shimmerLayout.stopShimmer()
                            shimmerLayout.gone()
                        }))
                        .error(R.drawable.default_image_profile)
                        .into(civImageProfile)
                }
            }
        )
    }

    fun setImage(showImage: Boolean, user: InterlocutorMessagesRow){
        if(showImage){
            civImageText.gone()
            tvImageText.gone()

            civImageProfile.visible()
            shimmerLayout.visible()

            Glide.with(this).load(user.image)
                .listener(glideCallback(onFinish = {
                    shimmerLayout.stopShimmer()
                    shimmerLayout.gone()
                }))
                .error(R.drawable.default_image_profile)
                .into(civImageProfile)
        }else {
            civImageProfile.gone()
            shimmerLayout.gone()

            civImageText.visible()
            tvImageText.visible()

            tvImageText.text = (if(user.fullname?.length ?: 0 > 1)
                user.fullname?.substring(0, 2) else user.fullname?.substring(0, 1))
                ?.toUpperCase(Locale.getDefault())
        }
    }
}