package com.letsconnect.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.ancestors.BaseViewModel
import com.letsconnect.ui.dashboard.DashboardVM
import com.letsconnect.ui.detail_person.DetailPersonVM
import com.letsconnect.ui.discover_role.DiscoverRoleVM
import com.letsconnect.ui.discover_subrole.DiscoverSubroleVM
import com.letsconnect.ui.force_close.ForceCloseVM
import com.letsconnect.ui.login.LoginVM
import com.letsconnect.ui.onboarding.OnboardingVM
import com.letsconnect.ui.profile.ProfileVM
import com.letsconnect.ui.sign_up_professional.SignUpProfessionalVM
import com.letsconnect.ui.sign_up_client.SignUpClientVM
import com.letsconnect.ui.sign_up_supporter.SignUpSupporterVM
import com.letsconnect.ui.sign_up_volunteer.SignUpVolunteerVM
import com.letsconnect.ui.splash_screen.SplashScreenVM

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(private val activity: BaseActivity): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T = when(modelClass){
        SplashScreenVM::class.java -> SplashScreenVM(activity) as T
        LoginVM::class.java -> LoginVM(activity) as T
        OnboardingVM::class.java -> OnboardingVM(activity) as T
        SignUpClientVM::class.java -> SignUpClientVM(activity) as T
        SignUpVolunteerVM::class.java -> SignUpVolunteerVM(activity) as T
        SignUpProfessionalVM::class.java -> SignUpProfessionalVM(activity) as T
        SignUpSupporterVM::class.java -> SignUpSupporterVM(activity) as T
        DashboardVM::class.java -> DashboardVM(activity) as T
        DetailPersonVM::class.java -> DetailPersonVM(activity) as T
        DiscoverRoleVM::class.java -> DiscoverRoleVM(activity) as T
        DiscoverSubroleVM::class.java -> DiscoverSubroleVM(activity) as T
        ProfileVM::class.java -> ProfileVM(activity) as T
        ForceCloseVM::class.java -> ForceCloseVM(activity) as T
        else -> BaseViewModel(activity) as T
    }
}