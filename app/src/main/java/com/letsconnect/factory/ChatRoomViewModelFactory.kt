package com.letsconnect.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.letsconnect.ancestors.BaseActivity
import com.letsconnect.ancestors.BaseViewModel
import com.letsconnect.ui.chat.ChatVM

@Suppress("UNCHECKED_CAST")
class ChatRoomViewModelFactory(private val activity: BaseActivity,
                               private val interlocutorId: Int): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T = when(modelClass){
        ChatVM::class.java -> ChatVM(activity, interlocutorId) as T
        else -> BaseViewModel(activity) as T
    }
}