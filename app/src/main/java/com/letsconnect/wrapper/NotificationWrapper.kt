package com.letsconnect.wrapper

import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.mcnmr.utilities.extension.JSONUtils
import com.mcnmr.utilities.extension.ifLet
import com.letsconnect.service.fcm.fcm_response.FCMReceiveMessage
import java.io.Serializable

sealed class NotificationWrapper: Serializable {
    companion object{
        private const val NOTIFICATION_TYPE = "notification_type"
        private const val TYPE_NEW_MESSAGE = "new_message"

        fun from(remoteMessage: RemoteMessage): NotificationWrapper{
            val gson = Gson()

            if(remoteMessage.data[NOTIFICATION_TYPE] == TYPE_NEW_MESSAGE){
                val fcmReceiveMessage = gson.fromJson(JSONUtils
                    .getJsonFromMap(remoteMessage.data as Map<String, Any>?)
                    .toString(), FCMReceiveMessage::class.java)
                ifLet(fcmReceiveMessage.conversation, fcmReceiveMessage.senderFullname){
                   return ReceiveMessage(fcmReceiveMessage)
                }

                return NoAction
            }else {
                return NoAction
            }
        }
    }

    object NoAction : NotificationWrapper()
    data class ReceiveMessage(val value: FCMReceiveMessage): NotificationWrapper()

    fun consume(onReceiveMessage: (ReceiveMessage) -> Unit = {},
                onNoAction: (NoAction) -> Unit = {}){
        when(this){
            is NoAction -> onNoAction.invoke(this)
            is ReceiveMessage -> onReceiveMessage.invoke(this)
        }
    }
}