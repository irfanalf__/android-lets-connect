package com.letsconnect.ancestors

interface BaseResponse{
    fun status(): Boolean
    fun message(): String
}