package com.letsconnect.model

class Flavor {
    companion object{
        const val DEBUG = "debug"
        const val RELEASE = "release"
        const val LOCAL = "local"
    }
}