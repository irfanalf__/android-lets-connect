package com.letsconnect.model

sealed class Role(val id: Int, private val roleName: String) {
    companion object{
        const val ROLE_VOLUNTEER = "1"
        const val ROLE_CLIENT = "2"
        const val ROLE_PROFESSIONAL = "3"
        const val ROLE_SUPPORTER = "4"

        fun get(role: String, roleName: String): Role{
            return when(role){
                ROLE_VOLUNTEER -> Volunteer()
                ROLE_PROFESSIONAL -> {
                    when(roleName){
                        "1" -> Counselor()
                        "2" -> Psychologist()
                        else -> Therapist()
                    }
                }
                ROLE_SUPPORTER -> {
                    when(roleName){
                        "1" -> PeerSupportSpecialist()
                        "2" -> LayCounselors()
                        else -> Coaches()
                    }
                }
                else -> Client()
            }
        }
    }

    class Volunteer: Role(0, "Volunteer")

    class Counselor: Role(1, "Counselor")
    class Psychologist: Role(2, "Psychologist")
    class Therapist: Role(3, "Therapist")

    class PeerSupportSpecialist: Role(1, "Peer Support Specialist")
    class LayCounselors: Role(2, "Lay Counselors")
    class Coaches: Role(3, "Coaches")

    class Client: Role(0, "Client")

    override fun toString(): String = roleName
}
